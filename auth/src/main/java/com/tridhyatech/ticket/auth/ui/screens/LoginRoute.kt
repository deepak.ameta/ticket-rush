package com.tridhyatech.ticket.auth.ui.screens

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.auth.common.BasicScreen
import com.tridhyatech.ticket.auth.navigation.LoginState

const val loginRoute = "login_route"

fun NavController.navigateToLogin(navOptions: NavOptions? = null) {
    this.navigate(loginRoute, navOptions)
}

fun NavGraphBuilder.loginScreen(
    appState: LoginState,
) {
    composable(route = loginRoute) {
        LoginRoute(appState = appState)
    }
}

@Composable
fun LoginRoute(
    appState: LoginState
) {
    BasicScreen {
        Text(text = "This is login screen")
        Spacer(modifier = Modifier.height(24.dp))
        Button(onClick = appState::navigateToVerifyOtp) {
            Text(text = "Login")
        }
        Spacer(modifier = Modifier.height(24.dp))
        Button(onClick = appState::navigateToRegister) {
            Text(text = "Visit Register")
        }
    }
}