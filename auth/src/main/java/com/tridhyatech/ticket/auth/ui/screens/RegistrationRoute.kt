package com.tridhyatech.ticket.auth.ui.screens

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.auth.common.BasicScreen
import com.tridhyatech.ticket.auth.navigation.LoginState

const val registrationRoute = "registration_route"

fun NavController.navigateToRegistration(navOptions: NavOptions? = null) {
    this.navigate(registrationRoute, navOptions)
}

fun NavGraphBuilder.registrationScreen(
    appState: LoginState,
) {
    composable(route = registrationRoute) {
        RegistrationRoute(appState = appState)
    }
}

@Composable
fun RegistrationRoute(
    appState: LoginState
) {
    BasicScreen {
        Text(text = "This is register screen")
        Spacer(modifier = Modifier.height(24.dp))
        Button(onClick = appState::navigateToLogin) {
            Text(text = "Visit Login")
        }
    }
}