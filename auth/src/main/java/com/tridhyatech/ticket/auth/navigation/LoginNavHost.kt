package com.tridhyatech.ticket.auth.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.tridhyatech.ticket.auth.ui.screens.loginRoute
import com.tridhyatech.ticket.auth.ui.screens.loginScreen
import com.tridhyatech.ticket.auth.ui.screens.registrationScreen
import com.tridhyatech.ticket.auth.ui.screens.setupAccountScreen
import com.tridhyatech.ticket.auth.ui.screens.setupPaymentMethodScreen
import com.tridhyatech.ticket.auth.ui.screens.verifyOtpScreen

@Composable
fun LoginNavHost(
    appState: LoginState,
    startDestination: String = loginRoute,
) {
    val navController = appState.navController

    NavHost(
        navController = navController as NavHostController,
        startDestination = startDestination,
    ) {
        loginScreen(appState = appState)
        registrationScreen(appState = appState)
        verifyOtpScreen(appState = appState)
        setupAccountScreen(appState = appState)
        setupPaymentMethodScreen(appState = appState)
    }
}