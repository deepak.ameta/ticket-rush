package com.tridhyatech.ticket.auth

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.tridhyatech.ticket.auth.navigation.LoginNavHost
import com.tridhyatech.ticket.auth.navigation.rememberLoginState
import com.tridhyatech.ticket.auth.ui.theme.TicketRushTheme

class LoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TicketRushTheme {
                LoginNavHost(appState = rememberLoginState())
            }
        }
    }
}