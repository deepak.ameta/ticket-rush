package com.tridhyatech.ticket.auth.ui.screens

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.auth.LoginActivity
import com.tridhyatech.ticket.auth.common.BasicScreen
import com.tridhyatech.ticket.auth.navigation.LoginState

const val setupPaymentMethodRoute = "setup_payment_route"

fun NavController.navigateToSetupPaymentMethod(navOptions: NavOptions? = null) {
    this.navigate(setupPaymentMethodRoute, navOptions)
}

fun NavGraphBuilder.setupPaymentMethodScreen(
    appState: LoginState,
) {
    composable(route = setupPaymentMethodRoute) {
        SetupPaymentMethodRoute(appState = appState)
    }
}

@Composable
fun SetupPaymentMethodRoute(
    appState: LoginState
) {
    val activity = LocalContext.current as? LoginActivity

    BasicScreen {
        Text(text = "This is setup payment method screen")
        Spacer(modifier = Modifier.height(24.dp))
        Button(
            onClick = { appState.navigateToHome(activity) },
            content = { Text(text = "Finish Setting Up") },
        )
        Spacer(modifier = Modifier.height(24.dp))
        Button(
            onClick = { appState.navigateToHome(activity)},
            content = { Text(text = "Set up later") },
        )
    }
}