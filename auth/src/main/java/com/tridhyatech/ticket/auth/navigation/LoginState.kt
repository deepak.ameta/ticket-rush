package com.tridhyatech.ticket.auth.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.tridhyatech.ticket.auth.LoginActivity
import com.tridhyatech.ticket.auth.ui.screens.navigateToLogin
import com.tridhyatech.ticket.auth.ui.screens.navigateToRegistration
import com.tridhyatech.ticket.auth.ui.screens.navigateToSetupAccount
import com.tridhyatech.ticket.auth.ui.screens.navigateToSetupPaymentMethod
import com.tridhyatech.ticket.auth.ui.screens.navigateToVerifyOtp

@Composable
fun rememberLoginState(
    navController: NavHostController = rememberNavController(),
): LoginState {
    return remember(navController) {
        LoginState(navController)
    }
}

@Stable
class LoginState(
    val navController: NavController,
) {
//    fun popBackStack() {
//        navController.popBackStack()
//    }

    fun navigateToHome(activity: LoginActivity?) {
        activity?.finish()
    }

    fun navigateToLogin() {
        navController.navigateToLogin()
    }

    fun navigateToRegister() {
        navController.navigateToRegistration()
    }

    fun navigateToVerifyOtp() {
        navController.navigateToVerifyOtp()
    }

    fun navigateToSetupAccount() {
        navController.navigateToSetupAccount()
    }

    fun navigateToSetupPaymentMethod() {
        navController.navigateToSetupPaymentMethod()
    }
}