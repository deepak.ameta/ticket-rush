package com.tridhyatech.ticket.auth.ui.screens

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.auth.common.BasicScreen
import com.tridhyatech.ticket.auth.navigation.LoginState

const val setupAccountRoute = "setup_account_route"

fun NavController.navigateToSetupAccount(navOptions: NavOptions? = null) {
    this.navigate(setupAccountRoute, navOptions)
}

fun NavGraphBuilder.setupAccountScreen(
    appState: LoginState,
) {
    composable(route = setupAccountRoute) {
        SetupAccountRoute(appState = appState)
    }
}

@Composable
fun SetupAccountRoute(
    appState: LoginState
) {
    BasicScreen {
        Text(text = "This is setup account screen")
        Spacer(modifier = Modifier.height(24.dp))
        Button(onClick = appState::navigateToSetupPaymentMethod) {
            Text(text = "Continue")
        }
    }
}