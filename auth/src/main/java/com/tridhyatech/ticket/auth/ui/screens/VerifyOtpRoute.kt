package com.tridhyatech.ticket.auth.ui.screens

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.auth.common.BasicScreen
import com.tridhyatech.ticket.auth.navigation.LoginState

const val verifyOtpRoute = "verify_otp_route"

fun NavController.navigateToVerifyOtp(navOptions: NavOptions? = null) {
    this.navigate(verifyOtpRoute, navOptions)
}

fun NavGraphBuilder.verifyOtpScreen(
    appState: LoginState,
) {
    composable(route = verifyOtpRoute) {
        VerifyOtpRoute(appState = appState)
    }
}

@Composable
fun VerifyOtpRoute(
    appState: LoginState
) {
    BasicScreen {
        Text(text = "This is verify OTP screen")
        Spacer(modifier = Modifier.height(24.dp))
        Button(onClick = appState::navigateToSetupAccount) {
            Text(text = "Verify")
        }
    }
}