package com.tridhyatech.ticketrush

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.tridhyatech.ticketrush.navigation.AppNavHost
import com.tridhyatech.ticketrush.navigation.utils.rememberAppState
import com.tridhyatech.ticketrush.ui.theme.TicketRushTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TicketRushTheme {
                AppNavHost(appState = rememberAppState())
//                FlightBookingRoute()
            }
        }
    }
}