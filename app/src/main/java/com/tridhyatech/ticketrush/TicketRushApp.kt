package com.tridhyatech.ticketrush

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TicketRushApp: Application()