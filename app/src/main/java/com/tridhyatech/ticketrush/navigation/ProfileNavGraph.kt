package com.tridhyatech.ticketrush.navigation

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.navigation
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.profile.inner.deletedata.deleteDataScreen
import com.tridhyatech.ticketrush.ui.screens.profile.inner.edit_info.editInfoScreen
import com.tridhyatech.ticketrush.ui.screens.profile.inner.notification.notificationScreen
import com.tridhyatech.ticketrush.ui.screens.profile.inner.personal_info.personalInfoScreen
import com.tridhyatech.ticketrush.ui.screens.profile.inner.privacy_sharing.privacyScreen
import com.tridhyatech.ticketrush.ui.screens.profile.inner.requestdata.requestScreen
import com.tridhyatech.ticketrush.ui.screens.profile.inner.review.reviewScreen
import com.tridhyatech.ticketrush.ui.screens.profile.profileRoute
import com.tridhyatech.ticketrush.ui.screens.profile.profileScreen

const val profileGraphRoute = "profile_route"

fun NavController.navigateToProfileGraph(navOptions: NavOptions? = null) {
    this.navigate(profileGraphRoute, navOptions)
}

fun NavGraphBuilder.profileGraph(
    appState: AppState,
) {
    navigation(
        route = profileGraphRoute,
        startDestination = profileRoute,
    ) {
        profileScreen(appState = appState)
        personalInfoScreen(appState = appState)
        editInfoScreen(appState = appState)

        privacyScreen(appState = appState)
        requestScreen(appState = appState)
        deleteDataScreen(appState = appState)
        notificationScreen(appState = appState)
        reviewScreen(appState = appState)
    }
}