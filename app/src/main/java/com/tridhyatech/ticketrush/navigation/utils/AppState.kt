package com.tridhyatech.ticketrush.navigation.utils

import android.content.Context
import android.content.Intent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.tridhyatech.ticket.flight.FlightActivity
import com.tridhyatech.ticketrush.ui.screens.navigateToDashboard
import com.tridhyatech.ticketrush.ui.screens.navigateToSplash
import com.tridhyatech.ticketrush.ui.screens.profile.inner.personal_info.navigateToPersonalInfo
import com.tridhyatech.ticketrush.ui.screens.profile.inner.deletedata.navigateToDeleteData
import com.tridhyatech.ticketrush.ui.screens.profile.inner.edit_info.navigateToEditInfo
import com.tridhyatech.ticketrush.ui.screens.profile.inner.notification.navigateToNotification
import com.tridhyatech.ticketrush.ui.screens.profile.inner.privacy_sharing.navigateToPrivacy
import com.tridhyatech.ticketrush.ui.screens.profile.inner.requestdata.navigateToRequest
import com.tridhyatech.ticketrush.ui.screens.profile.inner.review.navigateToReview

@Composable
fun rememberAppState(
    navController: NavHostController = rememberNavController(),
    homeNavController: NavHostController = rememberNavController(),
): AppState {
    return remember(navController) {
        AppState(navController, homeNavController)
    }
}

@Stable
class AppState(
    val navController: NavController,
    val homeNavController: NavController,
) {
    fun popBackStack() {
        navController.popBackStack()
    }

    fun navigateToSplash() {
        navController.navigateToSplash()
    }

    fun navigateToDashboard() {
        navController.navigateToDashboard()
    }

//    fun navigateToAuthRegister(context: Context) {
//        val i = Intent(context, LoginActivity::class.java)
//        context.startActivity(i)
//    }

    fun navigateToFlightBooking(context: Context) {
        val i = Intent(context, FlightActivity::class.java)
        context.startActivity(i)
    }

    /*fun navigateToTrainBooking(context: Context) {
        val i = Intent(context, TrainActivity::class.java)
        context.startActivity(i)
    }

    fun navigateToBusBooking(context: Context) {
        val i = Intent(context, BusActivity::class.java)
        context.startActivity(i)
    }

    fun navigateToHotelBooking(context: Context) {
        val i = Intent(context, HotelActivity::class.java)
        context.startActivity(i)
    }*/

    // Nested navigation
    fun popHomeBackStack() {
        homeNavController.popBackStack()
    }

    fun navigateToPersonalInfo() {
        homeNavController.navigateToPersonalInfo()
    }

    fun navigateToEditInfo() {
        homeNavController.navigateToEditInfo()
    }

    fun navigateToPrivacy() {
        homeNavController.navigateToPrivacy()
    }

    fun navigateToRequestData() {
        homeNavController.navigateToRequest()
    }

    fun navigateToDeleteData() {
        homeNavController.navigateToDeleteData()
    }

    fun navigateToNotification() {
        homeNavController.navigateToNotification()
    }

    fun navigateToReview() {
        homeNavController.navigateToReview()
    }
}