package com.tridhyatech.ticketrush.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.favorite.favoriteScreen
import com.tridhyatech.ticketrush.ui.screens.home.homeRoute
import com.tridhyatech.ticketrush.ui.screens.home.homeScreen
import com.tridhyatech.ticketrush.ui.screens.transaction.transactionScreen

@Composable
fun DashboardNavHost(
    appState: AppState,
    startDestination: String = homeRoute,
) {
    val navController = appState.homeNavController

    NavHost(
        navController = navController as NavHostController,
        startDestination = startDestination,
    ) {
        homeScreen(appState = appState)
        transactionScreen(appState = appState)
        favoriteScreen(appState = appState)
        profileGraph(appState = appState)
    }
}