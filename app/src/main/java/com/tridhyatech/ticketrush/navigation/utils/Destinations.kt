package com.tridhyatech.ticketrush.navigation.utils

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.Person
import androidx.compose.ui.graphics.vector.ImageVector
import com.tridhyatech.ticketrush.ui.screens.favorite.favoriteRoute
import com.tridhyatech.ticketrush.ui.screens.home.homeRoute
import com.tridhyatech.ticketrush.ui.screens.profile.profileRoute
import com.tridhyatech.ticketrush.ui.screens.transaction.transactionRoute

sealed class Destinations(
    val route: String,
    val title: String? = null,
    val icon: ImageVector? = null
) {
    object HomeScreen : Destinations(
        route = homeRoute,
        title = "Home",
        icon = Icons.Outlined.Home
    )

    object Transaction : Destinations(
        route = transactionRoute,
        title = "Transaction",
        icon = Icons.Outlined.Info
    )

    object Favorite : Destinations(
        route = favoriteRoute,
        title = "Favorite",
        icon = Icons.Outlined.Favorite
    )

    object Profile : Destinations(
        route = profileRoute,
        title = "Profile",
        icon = Icons.Outlined.Person
    )
}

val screens = listOf(
    Destinations.HomeScreen,
    Destinations.Transaction,
    Destinations.Favorite,
    Destinations.Profile,
)