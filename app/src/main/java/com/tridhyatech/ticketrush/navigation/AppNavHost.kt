package com.tridhyatech.ticketrush.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.dashboardScreen
import com.tridhyatech.ticketrush.ui.screens.splashRoute
import com.tridhyatech.ticketrush.ui.screens.splashScreen

@Composable
fun AppNavHost(
    appState: AppState,
    startDestination: String = splashRoute,
) {
    val navController = appState.navController

    NavHost(
        navController = navController as NavHostController,
        startDestination = startDestination,
    ) {
        splashScreen(appState = appState)
        dashboardScreen(appState = appState)
    }
}