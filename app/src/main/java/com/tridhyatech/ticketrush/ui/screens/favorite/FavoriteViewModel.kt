package com.tridhyatech.ticketrush.ui.screens.favorite

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.domain.model.hotel.HotelDetail
import com.tridhyatech.ticket.common.utils.dummyHotelDataList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(FavoriteState())
    val uiState: StateFlow<FavoriteState> = _uiState.asStateFlow()

    init {
        _uiState.value = FavoriteState(favHotelsList = dummyHotelDataList)
    }

    fun updateData(action: FavoriteActions) {
        when (action) {
            FavoriteActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            FavoriteActions.OnNext -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            FavoriteActions.OnNotificationIcon -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("No new notifications"))
                }
            }

            is FavoriteActions.OnItemSelection -> {
                // will redirect to hotel module
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("Hotel module is not currently available."))
                }
            }

            is FavoriteActions.UpdateEditText -> {
                _uiState.update { it.copy(favEt = action.text) }
            }
        }
    }
}

data class FavoriteState(
    val favEt: String = "",
    val favHotelsList: ArrayList<HotelDetail> = arrayListOf()
)

sealed interface FavoriteActions {
    object OnNext : FavoriteActions
    object OnBack : FavoriteActions
    object OnNotificationIcon : FavoriteActions
    class OnItemSelection(val item: HotelDetail) : FavoriteActions
    class UpdateEditText(val text: String) : FavoriteActions
}