package com.tridhyatech.ticketrush.ui.screens

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.common.BasicScreen
import com.tridhyatech.ticketrush.navigation.utils.AppState

const val splashRoute = "splash_route"

fun NavController.navigateToSplash(navOptions: NavOptions? = null) {
    this.navigate(splashRoute, navOptions)
}

fun NavGraphBuilder.splashScreen(
    appState: AppState,
) {
    composable(route = splashRoute) {
        SplashRoute(appState = appState)
    }
}

@Composable
fun SplashRoute(
    appState: AppState
) {

    LaunchedEffect(true) {
        appState.navigateToDashboard()
    }

    BasicScreen {
        Text(text = "This is Ticket Rush App")
        Spacer(modifier = Modifier.height(24.dp))
        Button(
            onClick = appState::navigateToDashboard,
            content = { Text(text = "Login") },
        )
    }
}