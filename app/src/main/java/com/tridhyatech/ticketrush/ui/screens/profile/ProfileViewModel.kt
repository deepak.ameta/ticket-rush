package com.tridhyatech.ticketrush.ui.screens.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(ProfileState())
    val uiState: StateFlow<ProfileState> = _uiState.asStateFlow()

    fun updateData(action: ProfileActions) {
        when (action) {
            ProfileActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            ProfileActions.OnEditProfile -> {
//                viewModelScope.launch {
//                    _uiEvent.send(UiEvents.OnError("Will open error dialog"))
//                }
            }

            ProfileActions.OnLogout -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnNext)
                }
            }
        }
    }
}

data class ProfileState(
    val name: String = "Bruce Wayne",
    val email: String = "brucewayne@email.com"
)

sealed interface ProfileActions {
    object OnEditProfile : ProfileActions
    object OnLogout : ProfileActions
    object OnBack : ProfileActions
}