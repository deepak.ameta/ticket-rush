package com.tridhyatech.ticketrush.ui.screens.profile.inner.privacy_sharing

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.profile.profileRoute

const val privacyRoute = "privacy_screen_$profileRoute"

fun NavController.navigateToPrivacy(navOptions: NavOptions? = null) {
    this.navigate(privacyRoute, navOptions)
}

fun NavGraphBuilder.privacyScreen(appState: AppState) {
    composable(route = privacyRoute) {
        PrivacyRoute(appState = appState)
    }
}