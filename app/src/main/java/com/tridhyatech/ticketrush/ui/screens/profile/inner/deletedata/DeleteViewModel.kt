package com.tridhyatech.ticketrush.ui.screens.profile.inner.deletedata

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DeleteViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(DeleteState())
    val uiState: StateFlow<DeleteState> = _uiState.asStateFlow()

    fun updateData(action: DeleteActions) {
        when (action) {
            DeleteActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            DeleteActions.OnNext -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            is DeleteActions.UpdateCounty -> {
                _uiState.update { it.copy(selectedCountry = action.value) }
            }

            is DeleteActions.UpdateReason -> {
                _uiState.update { it.copy(reason = action.value) }
            }
        }
    }
}

data class DeleteState(
    val countryList: ArrayList<String> = arrayListOf("India", "US", "UK"),
    val selectedCountry: String = "India",
    val reason: String = "",
)

sealed interface DeleteActions {
    object OnNext : DeleteActions
    object OnBack : DeleteActions
    data class UpdateCounty(val value: String) : DeleteActions
    data class UpdateReason(val value: String) : DeleteActions
}