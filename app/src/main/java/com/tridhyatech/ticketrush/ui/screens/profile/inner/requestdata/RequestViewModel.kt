package com.tridhyatech.ticketrush.ui.screens.profile.inner.requestdata

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RequestViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(RequestState())
    val uiState: StateFlow<RequestState> = _uiState.asStateFlow()

    fun updateData(action: RequestActions) {
        when (action) {
            RequestActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            RequestActions.OnNext -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            is RequestActions.UpdateCounty -> {
                _uiState.update { it.copy(selectedCountry = action.value) }
            }

            is RequestActions.UpdateReason -> {
                _uiState.update { it.copy(reason = action.value) }
            }
        }
    }
}

data class RequestState(
    val countryList: ArrayList<String> = arrayListOf("India", "US", "UK"),
    val selectedCountry: String = "India",
    val reason: String = "",
)

sealed interface RequestActions {
    object OnNext : RequestActions
    object OnBack : RequestActions
    data class UpdateCounty(val value: String) : RequestActions
    data class UpdateReason(val value: String) : RequestActions
}