package com.tridhyatech.ticketrush.ui.screens.profile

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState

const val profileRoute = "profile"

//fun NavController.navigateToProfile(navOptions: NavOptions? = null) {
//    this.navigate(profileRoute, navOptions)
//}

fun NavGraphBuilder.profileScreen(appState: AppState) {
    composable(route = profileRoute) {
        ProfileRoute(appState = appState)
    }
}