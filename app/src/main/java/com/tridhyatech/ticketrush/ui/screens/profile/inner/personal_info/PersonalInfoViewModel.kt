package com.tridhyatech.ticketrush.ui.screens.profile.inner.personal_info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.profileData
import com.tridhyatech.ticket.common.domain.ProfileInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PersonalInfoViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(ProfileInfo())
    val uiState: StateFlow<ProfileInfo> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val checkEmptyData = session.getValue(profileData, "")
            if (checkEmptyData == "") {
                val data = ProfileInfo(
                    name = "Bruce",
                    gender = "Male",
                    email = "brucewayne@gmail.com",
                    phoneNo = "91 9923-958-877",
                    id = "6448929324039",
                )
                val profileJson = Gson().toJson(data)
                session.setValue(profileData, profileJson)
            } else {
                loadData()
            }
        }
    }

    fun loadData() {
        viewModelScope.launch {
            val profileJson = session.getValue(profileData, "")
            val data = Gson().fromJson(profileJson, ProfileInfo::class.java)
            _uiState.value = data
        }
    }

    fun updateData(action: PersonalInfoActions) {
        when (action) {
            PersonalInfoActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            PersonalInfoActions.OnEditProfile -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnNext)
                }
            }
        }
    }
}

sealed interface PersonalInfoActions {
    object OnEditProfile : PersonalInfoActions
    object OnBack : PersonalInfoActions
}