package com.tridhyatech.ticketrush.ui.screens.profile.inner.personal_info

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.domain.ProfileInfo
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.common.ProfileHeader
import com.tridhyatech.ticketrush.ui.theme.LightGrayTextColor
import com.tridhyatech.ticketrush.ui.utils.composables.CustomButton
import kotlinx.coroutines.flow.collectLatest

@Composable
fun ProfileInfoRoute(
    appState: AppState,
    viewModel: PersonalInfoViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.loadData()
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> {
                    appState.popHomeBackStack()
                }

                UiEvents.OnNext -> {
                    appState.navigateToEditInfo()
                }

                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    PersonalInfoScreen(uiState, viewModel::updateData)
}

@Composable
fun PersonalInfoScreen(
    uiState: ProfileInfo,
    onAction: (PersonalInfoActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp, vertical = 16.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.Top)
    ) {
        ProfileHeader(
            title = "Personal Info",
            onBackClick = { onAction(PersonalInfoActions.OnBack) },
        )
        InfoWithLabel(label = "NAME", text = uiState.name)
        InfoWithLabel(label = "GENDER", text = uiState.gender)
        InfoWithLabel(label = "EMAIL", text = uiState.email)
        InfoWithLabel(label = "PHONE", text = uiState.phoneNo)
        InfoWithLabel(label = "ID NUMBER", text = uiState.id)
        Spacer(Modifier.height(24.dp))
        CustomButton(text = "Edit Profile Info", onClick = {
            onAction(PersonalInfoActions.OnEditProfile)
        })
    }
}

@Composable
fun InfoWithLabel(
    label: String,
    text: String
) {
    Column(
        modifier = Modifier,
        verticalArrangement = Arrangement.Top,
    ) {
        Text(
            text = label,
            style = TextStyle(
                fontSize = 14.sp,
                fontWeight = FontWeight(500),
                color = LightGrayTextColor,
            )
        )
        Text(
            text = text,
            style = TextStyle(
                fontSize = 18.sp,
                fontWeight = FontWeight(500),
                color = Color(0xFF000000),
            )
        )
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewPersonalInfo() {
//    PersonalInfoScreen()
//}