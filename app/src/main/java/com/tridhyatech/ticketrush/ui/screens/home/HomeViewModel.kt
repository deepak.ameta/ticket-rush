package com.tridhyatech.ticketrush.ui.screens.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.allPassengerContactDataStore
import com.tridhyatech.ticket.common.data.session.contactDataStore
import com.tridhyatech.ticket.common.data.session.flightBookingDataStore
import com.tridhyatech.ticket.common.domain.model.flight.BookingDetail
import com.tridhyatech.ticket.common.domain.model.flight.ContactDetail
import com.tridhyatech.ticket.common.domain.model.hotel.HotelDetail
import com.tridhyatech.ticket.common.utils.dummyHotelDataList
import com.tridhyatech.ticket.common.utils.toJson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(HomeState())
    val uiState: StateFlow<HomeState> = _uiState.asStateFlow()

    init {
        _uiState.update {
            it.copy(
                userLocation = "Sleman, Yogyakarta",
                nearByHotelList = dummyHotelDataList,
            )
        }

        viewModelScope.launch {
            session.setValue(contactDataStore, ContactDetail().toJson())
            session.setValue(flightBookingDataStore, BookingDetail().toJson())
            session.setValue(allPassengerContactDataStore, arrayListOf<ContactDetail>().toJson())
        }
    }

    fun updateData(action: HomeActions) {
        when (action) {
            HomeActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            HomeActions.OnNext -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            HomeActions.OnLocationIcon -> {
                // will get user location and update its location
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("This feature is not currently working"))
                }
            }

            HomeActions.OnNotificationIcon -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("No new notifications"))
                }
            }

            is HomeActions.OnNearByHotelItem -> {
                // will redirect to hotel module
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("Hotel module is not currently available."))
                }
            }

            is HomeActions.OnInsightItem -> {
                // will redirect to insight section
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("Insight section is not currently available."))
                }
            }

            HomeActions.OnRecommendedSeeMore -> {
                // will redirect to hotel module
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("Hotel module is not currently available."))
                }
            }

            is HomeActions.OnRecommendedItem -> {
                // will redirect to hotel module
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("Hotel module is not currently available."))
                }
            }

            is HomeActions.OnExploreETUpdate -> {
                _uiState.update { it.copy(exploreEt = action.text) }
            }
        }
    }
}

data class HomeState(
    val exploreEt: String = "",
    val userLocation: String = "",
    val nearByHotelList: ArrayList<HotelDetail> = arrayListOf(),
)

sealed interface HomeActions {
    object OnNext : HomeActions
    object OnBack : HomeActions
    object OnLocationIcon : HomeActions
    object OnNotificationIcon : HomeActions
    object OnRecommendedSeeMore : HomeActions
    class OnNearByHotelItem(val hotelDetail: HotelDetail) : HomeActions
    class OnRecommendedItem(val hotelDetail: HotelDetail) : HomeActions
    class OnInsightItem(val insightItem: String) : HomeActions
    class OnExploreETUpdate(val text: String) : HomeActions
}