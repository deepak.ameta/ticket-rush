package com.tridhyatech.ticketrush.ui.screens.favorite

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState

const val favoriteRoute = "favorite_route"

fun NavGraphBuilder.favoriteScreen(appState: AppState) {
    composable(route = favoriteRoute) { FavoriteRoute(appState = appState) }
}