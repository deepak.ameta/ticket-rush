package com.tridhyatech.ticketrush.ui.screens.profile.inner.personal_info

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.profile.profileRoute

const val personalInfoRoute = "personal_info_screen_$profileRoute"

fun NavController.navigateToPersonalInfo(navOptions: NavOptions? = null) {
    this.navigate(personalInfoRoute, navOptions)
}

fun NavGraphBuilder.personalInfoScreen(appState: AppState) {
    composable(route = personalInfoRoute) {
        ProfileInfoRoute(appState = appState)
    }
}