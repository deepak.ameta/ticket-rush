package com.tridhyatech.ticketrush.ui.screens.profile.inner.deletedata

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.profile.profileRoute

const val deleteRoute = "delete_data_screen_$profileRoute"

fun NavController.navigateToDeleteData(navOptions: NavOptions? = null) {
    this.navigate(deleteRoute, navOptions)
}

fun NavGraphBuilder.deleteDataScreen(appState: AppState) {
    composable(route = deleteRoute) {
        DeleteDataRoute(appState = appState)
    }
}