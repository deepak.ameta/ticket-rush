package com.tridhyatech.ticketrush.ui.screens.profile.inner.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(NotificationState())
    val uiState: StateFlow<NotificationState> = _uiState.asStateFlow()

    fun updateData(action: NotificationActions) {
        when (action) {
            NotificationActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            is NotificationActions.OnToggleTipsEmail -> {
                _uiState.update { it.copy(pushEmailEnabled = action.value) }
            }

            is NotificationActions.OnToggleTipsPush -> {
                _uiState.update { it.copy(pushEnabled = action.value) }
            }

            is NotificationActions.OnToggleActivityEmail -> {
                _uiState.update { it.copy(activityEmailEnabled = action.value) }
            }

            is NotificationActions.OnToggleActivityPush -> {
                _uiState.update { it.copy(activityEnabled = action.value) }
            }

            is NotificationActions.OnToggleRemainderEmail -> {
                _uiState.update { it.copy(remaindersEmailEnabled = action.value) }
            }

            is NotificationActions.OnToggleRemainderPush -> {
                _uiState.update { it.copy(remaindersEnabled = action.value) }
            }
        }
    }
}

data class NotificationState(
    val pushEnabled: Boolean = false,
    val pushEmailEnabled: Boolean = false,
    val activityEnabled: Boolean = false,
    val activityEmailEnabled: Boolean = false,
    val remaindersEnabled: Boolean = false,
    val remaindersEmailEnabled: Boolean = false,
)

sealed interface NotificationActions {
    object OnBack : NotificationActions
    data class OnToggleTipsPush(val value: Boolean) : NotificationActions
    data class OnToggleTipsEmail(val value: Boolean) : NotificationActions
    data class OnToggleActivityPush(val value: Boolean) : NotificationActions
    data class OnToggleActivityEmail(val value: Boolean) : NotificationActions
    data class OnToggleRemainderPush(val value: Boolean) : NotificationActions
    data class OnToggleRemainderEmail(val value: Boolean) : NotificationActions

}