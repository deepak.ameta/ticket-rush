package com.tridhyatech.ticketrush.ui.screens.transaction

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState

const val transactionRoute = "transaction_route"

fun NavGraphBuilder.transactionScreen(appState: AppState) {
    composable(route = transactionRoute) { TransactionRoute(appState = appState) }
}
