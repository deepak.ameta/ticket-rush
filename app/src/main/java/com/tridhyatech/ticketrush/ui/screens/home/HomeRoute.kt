package com.tridhyatech.ticketrush.ui.screens.home

import android.app.Activity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.common.InsightSection
import com.tridhyatech.ticketrush.ui.screens.common.ModuleSelectionSection
import com.tridhyatech.ticketrush.ui.screens.common.RecommendationSection
import com.tridhyatech.ticketrush.ui.screens.common.TopNearBySection
import com.tridhyatech.ticketrush.ui.screens.common.TopSectionHomeScreen
import com.tridhyatech.ticketrush.ui.screens.common.WalletPromoSection
import kotlinx.coroutines.flow.collectLatest

@Composable
fun HomeRoute(
    appState: AppState,
    viewModel: HomeViewModel = hiltViewModel()
) {

    val uiState by viewModel.uiState.collectAsStateWithLifecycle()

    val context = LocalContext.current
    val activity = context as Activity

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> activity.finish()
                UiEvents.OnNext -> {
                    appState.navigateToFlightBooking(context)
                }

                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }
    HomeScreen(
        uiState = uiState,
        onAction = viewModel::updateData,
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(
    uiState: HomeState,
    onAction: (HomeActions) -> Unit,
) {
    Scaffold { padding ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(padding)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {

                TopSectionHomeScreen(uiState, onAction)
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp, vertical = 8.dp),
                    verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
                ) {
                    WalletPromoSection()
                    ModuleSelectionSection(onAction)
                    TopNearBySection(uiState, onAction)
                    RecommendationSection(uiState, onAction)
                    InsightSection(uiState, onAction)
                }
            }
        }
    }
}

/*@Preview(showBackground = true)
@Composable
fun PreviewHomeScreen() {
    HomeScreen()
}*/
