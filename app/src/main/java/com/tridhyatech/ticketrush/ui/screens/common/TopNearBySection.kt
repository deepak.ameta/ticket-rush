package com.tridhyatech.ticketrush.ui.screens.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.common.domain.model.hotel.HotelDetail
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.ui.screens.home.HomeActions
import com.tridhyatech.ticketrush.ui.screens.home.HomeState

@Composable
fun TopNearBySection(
    uiState: HomeState,
    onAction: (HomeActions) -> Unit,
) {
    Column(
        modifier = Modifier.fillMaxWidth(1f)
    ) {
        Row(
            Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.spacedBy(0.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                painter = painterResource(id = R.drawable.icon_near_me),
                contentDescription = "image description",
                contentScale = ContentScale.None,
                modifier = Modifier
                    .padding(1.dp)
                    .size(20.dp),
            )
            Text(
                modifier = Modifier,
                text = "Top Nearby",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(600),
                    color = Color(0xFF1B1446),
                )
            )
            Spacer(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .weight(1f)
            )
            Row(
                modifier = Modifier
                    .clip(RoundedCornerShape(4.dp))
                    .clickable { onAction(HomeActions.OnLocationIcon) }
                    .padding(horizontal = 8.dp),
                horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.End),
            ) {
                Column(
                    modifier = Modifier,
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.End,
                ) {
                    Text(
                        text = "Location",
                        style = TextStyle(
                            fontSize = 10.sp,
                            lineHeight = 14.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFF808080),
                            letterSpacing = 0.8.sp,
                        )
                    )
                    Text(
                        text = uiState.userLocation,
                        style = TextStyle(
                            fontSize = 12.sp,
                            fontWeight = FontWeight(600),
                            color = Color(0xFF1B1446),
                        )
                    )
                }
                Box(
                    modifier = Modifier
                        .width(20.dp)
                        .height(34.dp)
                        .clip(RoundedCornerShape(32.dp))
                        .background(Color(0x1A0768FD))
                        .padding(start = 2.dp, top = 5.dp, end = 2.dp, bottom = 5.dp),
                    contentAlignment = Alignment.Center,
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.icon_location),
                        contentDescription = "image description",
                        contentScale = ContentScale.None,
                        modifier = Modifier
                            .padding(1.dp)
                            .size(16.dp),
                    )
                }
            }
        }
        Spacer(Modifier.height(16.dp))
        repeat(2) {
            TopNearByItem(uiState.nearByHotelList[it], onAction)
        }
    }
}

@Composable
fun TopNearByItem(
    hotelDetail: HotelDetail,
    onAction: (HomeActions) -> Unit,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(1.dp)
            .height(114.dp)
            .border(
                width = 1.dp,
                color = Color(0x0D012276),
                shape = RoundedCornerShape(size = 16.dp)
            )
            .clip(RoundedCornerShape(16.dp))
            .background(Color.White)
            .clickable { onAction(HomeActions.OnNearByHotelItem(hotelDetail)) },
        horizontalArrangement = Arrangement.spacedBy(0.dp, Alignment.CenterHorizontally),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Box(
            modifier = Modifier
                .width(134.dp)
                .height(114.dp)
                .padding(2.dp)
                .border(
                    width = 1.dp,
                    color = Color(0x0D012276),
                    shape = RoundedCornerShape(size = 16.dp),
                )
                .background(
                    color = Color.LightGray,
                    shape = RoundedCornerShape(size = 16.dp),
                )
        )
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 16.dp, vertical = 0.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start,
        ) {
            Text(
                text = hotelDetail.name,
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                    textAlign = TextAlign.Center,
                )
            )
            Row(
                modifier = Modifier,
                horizontalArrangement = Arrangement.spacedBy(2.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    painter = painterResource(id = R.drawable.icon_location_on),
                    contentDescription = "location icon",
                    contentScale = ContentScale.None,
                    modifier = Modifier
                        .padding(1.dp)
                        .width(12.dp)
                        .height(12.dp),
                )
                Text(
                    text = hotelDetail.location,
                    style = TextStyle(
                        fontSize = 10.sp,
                        lineHeight = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                        letterSpacing = 0.8.sp,
                    )
                )
            }

            Text(
                text = "$${hotelDetail.price}",
                style = TextStyle(
                    fontSize = 11.sp,
                    lineHeight = 14.sp,
                    fontWeight = FontWeight(500),
                    fontStyle = FontStyle.Italic,
                    color = Color(0xFFBFBFBF),
                    textDecoration = TextDecoration.LineThrough,
                )
            )
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(2.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        text = "$${hotelDetail.discountedPrice}",
                        style = TextStyle(
                            fontSize = 14.sp,
                            fontWeight = FontWeight(600),
                            color = Color(0xFF398B2B),
                        )
                    )
                    Text(
                        text = "/night",
                        style = TextStyle(
                            fontSize = 12.sp,
                            fontWeight = FontWeight(400),
                            color = Color(0xFF808080),
                        )
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.spacedBy(2.dp, Alignment.End),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.icon_star),
                        contentDescription = "rating icon",
                        contentScale = ContentScale.None,
                        modifier = Modifier
                            .padding(1.dp)
                            .width(16.dp)
                            .height(16.dp),
                    )
                    Text(
                        text = hotelDetail.rating.toString(),
                        style = TextStyle(
                            fontSize = 12.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFFBFBFBF),
                            textAlign = TextAlign.Center,
                        )
                    )
                }
            }
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewTopNearByItem() {
//    TopNearBySection(uiState = HomeState(), onAction = {})
//}