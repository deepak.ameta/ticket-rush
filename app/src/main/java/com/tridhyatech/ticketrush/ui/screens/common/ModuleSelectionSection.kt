package com.tridhyatech.ticketrush.ui.screens.common

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.ui.screens.home.HomeActions

@Composable
fun ModuleSelectionSection(onAction: (HomeActions) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .horizontalScroll(rememberScrollState()),
        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        ModuleItem(name = "Hotel", id = R.drawable.icon_hotel)
        ModuleItem(
            name = "Flight",
            id = R.drawable.icon_hotel,
            onClick = { onAction(HomeActions.OnNext) },
        )
        ModuleItem(name = "Train", id = R.drawable.icon_train)
        ModuleItem(name = "Bus", id = R.drawable.bus_icon)
    }
}

@Composable
fun ModuleItem(
    name: String,
    @DrawableRes id: Int,
    onClick: () -> Unit = {}
) {
    Box(
        modifier = Modifier
            .width(109.dp)
            .height(56.dp)
            .clip(RoundedCornerShape(16.dp))
            .background(color = Color(0xFFF6F8FB))
            .clickable { onClick() },
        contentAlignment = Alignment.Center,
    ) {
        Row(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                painter = painterResource(id = id),
                contentDescription = "",
                contentScale = ContentScale.Fit,
                modifier = Modifier
                    .width(40.dp)
                    .height(40.dp)
                    .padding(start = 8.dp, top = 7.dp, end = 8.dp, bottom = 7.dp),
            )
            Text(
                text = name,
                style = TextStyle(
                    fontSize = 14.sp,
                    lineHeight = 21.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                )
            )
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewModuleSelectionSection() {
//    ModuleSelectionSection()
//}