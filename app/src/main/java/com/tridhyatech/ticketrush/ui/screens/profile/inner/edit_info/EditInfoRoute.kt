package com.tridhyatech.ticketrush.ui.screens.profile.inner.edit_info

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.domain.ProfileInfo
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.common.ProfileHeader
import com.tridhyatech.ticketrush.ui.utils.composables.CustomButton
import kotlinx.coroutines.flow.collectLatest

@Composable
fun EditInfoRoute(
    appState: AppState,
    viewModel: EditInfoViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> {
                    appState.popHomeBackStack()
                }

                UiEvents.OnNext -> {
                    appState.popHomeBackStack()
                }

                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    EditInfoScreen(uiState, viewModel::updateData)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditInfoScreen(
    uiState: ProfileInfo,
    onAction: (EditInfoActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp, vertical = 16.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.Top)
    ) {
        ProfileHeader(
            title = "Edit Personal Info",
            onBackClick = { onAction(EditInfoActions.OnBack) },
        )
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            label = { Text(text = "NAME") },
            value = uiState.name,
            onValueChange = { onAction(EditInfoActions.OnNameUpdate(it)) },
        )
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            label = { Text(text = "GENDER") },
            value = uiState.gender,
            onValueChange = { onAction(EditInfoActions.OnGenderUpdate(it)) },
        )
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            label = { Text(text = "Email") },
            value = uiState.email,
            onValueChange = { onAction(EditInfoActions.OnEmailUpdate(it)) },
        )
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            label = { Text(text = "Phone Number") },
            value = uiState.phoneNo,
            onValueChange = { onAction(EditInfoActions.OnPhoneUpdate(it)) },
        )
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            label = { Text(text = "ID Number") },
            value = uiState.id,
            onValueChange = { onAction(EditInfoActions.OnIdUpdate(it)) },
        )
        Spacer(Modifier.height(24.dp))
        CustomButton(
            text = "Save Changes",
            onClick = { onAction(EditInfoActions.OnNext) },
        )
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewEditInfo() {
//    EditInfoScreen()
//}