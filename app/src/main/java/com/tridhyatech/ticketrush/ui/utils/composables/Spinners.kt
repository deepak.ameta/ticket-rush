package com.tridhyatech.ticketrush.ui.utils.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.KeyboardArrowDown
import androidx.compose.material.icons.outlined.KeyboardArrowUp
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize
import com.tridhyatech.ticketrush.ui.theme.MySpinnerTextColor

@Composable
fun MySpinner(
    label: String,
    list: List<String>,
    selectionEnabled: Boolean = true,
    onSelectionChanged: (String) -> Unit,
    selection: String,
) {
    Column(
        modifier = Modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.Start
    ) {
        Spacer(Modifier.height(5.dp))
        Text(
            text = label,
            style = TextStyle(
                fontSize = 18.sp,
                fontWeight = FontWeight(500),
                color = Color.Black,
            )
        )
        CustomSpinner(
            list = list,
            onSelectionChanged = onSelectionChanged,
            selected = selection,
            selectionEnabled = selectionEnabled,
        )
    }
}

@Composable
fun CustomSpinner(
    list: List<String>,
    marginTop: Dp = 2.dp,
    selectionEnabled: Boolean = true,
    style: TextStyle = TextStyle(
        fontSize = 18.sp,
        fontWeight = FontWeight(500),
        color = Color.Black,
    ),
    onSelectionChanged: (String) -> Unit,
    selected: String,
) {

    var expanded by remember { mutableStateOf(false) }
    var rowSize by remember { mutableStateOf(Size.Zero) }

    val icon = if (expanded) {
        Icons.Outlined.KeyboardArrowUp
    } else {
        Icons.Outlined.KeyboardArrowDown
    }

    Column {

        Spacer(Modifier.height(marginTop))

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .onGloballyPositioned { layoutCoordinates ->
                    rowSize = layoutCoordinates.size.toSize()
                },
        ) {
            Column {
                CustomTextField(
                    forSpinner = true,
                    value = selected,
                    onValueChange = { },
                    trailingIcon = { Icon(icon, "Arrow Spinners") },
                )
                DropdownMenu(
                    modifier = Modifier.width(with(LocalDensity.current) { rowSize.width.toDp() }),
                    expanded = expanded,
                    onDismissRequest = { expanded = false },
                ) {
                    list.forEach {

                        DropdownMenuItem(modifier = Modifier.fillMaxWidth(), onClick = {
                            onSelectionChanged(it)
                            expanded = false
                        }, text = {
                            Text(
                                text = it,
                                modifier = Modifier.wrapContentWidth(),
                                style = if (it != selected) style else {
                                    style.copy(
                                        color = MySpinnerTextColor, fontWeight = FontWeight(500),
                                    )
                                }
                            )
                        })
                    }
                }
            }

            Spacer(
                modifier = Modifier
                    .matchParentSize()
                    .background(Color.Transparent)
                    .padding(2.dp)
                    .clickable {
                        if (selectionEnabled) {
                            expanded = !expanded
                        }
                    },
            )
        }
    }
}