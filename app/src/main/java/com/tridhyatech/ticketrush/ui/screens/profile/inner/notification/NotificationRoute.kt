package com.tridhyatech.ticketrush.ui.screens.profile.inner.notification

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.common.ProfileHeader
import com.tridhyatech.ticketrush.ui.theme.LightGrayTextColor
import kotlinx.coroutines.flow.collectLatest

@Composable
fun NotificationRoute(
    appState: AppState,
    viewModel: NotificationViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> { appState.popHomeBackStack() }
                UiEvents.OnNext -> {}
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    NotificationScreen(uiState, viewModel::updateData)
}

@Composable
fun NotificationScreen(uiState: NotificationState, onAction: (NotificationActions) -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 0.dp, vertical = 16.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(0.dp, alignment = Alignment.Top)
    ) {
        ProfileHeader(title = "Notification", onBackClick = {
            onAction(NotificationActions.OnBack)
        }, addPadding = true)
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, top = 16.dp),
            text = "SPECIAL TIPS AND OFFERS",
            style = TextStyle(
                fontSize = 12.sp,
                fontWeight = FontWeight(500),
                color = LightGrayTextColor,
            )
        )
        NotificationItemRow(
            text = "Push Notification",
            isEnabled = uiState.pushEnabled,
            onToggle = {
                onAction(NotificationActions.OnToggleTipsPush(it))
            })
        NotificationItemRow(text = "Email",
            isEnabled = uiState.pushEmailEnabled,
            onToggle = {
                onAction(NotificationActions.OnToggleTipsEmail(it))
            })

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, top = 16.dp),
            text = "ACTIVITY",
            style = TextStyle(
                fontSize = 12.sp,
                fontWeight = FontWeight(500),
                color = LightGrayTextColor,
            )
        )
        NotificationItemRow(text = "Push Notification",
            isEnabled = uiState.activityEnabled,
            onToggle = {
                onAction(NotificationActions.OnToggleActivityPush(it))
            })
        NotificationItemRow(text = "Email",
            isEnabled = uiState.activityEmailEnabled,
            onToggle = {
                onAction(NotificationActions.OnToggleActivityEmail(it))
            })

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, top = 16.dp),
            text = "REMAINDERS",
            style = TextStyle(
                fontSize = 12.sp,
                fontWeight = FontWeight(500),
                color = LightGrayTextColor,
            )
        )
        NotificationItemRow(text = "Push Notification",
            isEnabled = uiState.remaindersEnabled,
            onToggle = {
                onAction(NotificationActions.OnToggleRemainderPush(it))
            })
        NotificationItemRow(text = "Email",
            isEnabled = uiState.remaindersEmailEnabled,
            onToggle = {
                onAction(NotificationActions.OnToggleRemainderEmail(it))
            })
    }
}

@Composable
fun NotificationItemRow(text: String, isEnabled: Boolean, onToggle: (Boolean) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(horizontal = 16.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = text,
            style = TextStyle(
                fontSize = 18.sp,
                fontWeight = FontWeight(500),
                color = Color(0xFF000000),
            )
        )
        Switch(
            checked = isEnabled,
            onCheckedChange = onToggle
        )
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewNotificationScreen() {
//    NotificationScreen()
//}
