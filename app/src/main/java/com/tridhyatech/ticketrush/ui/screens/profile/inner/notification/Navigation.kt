package com.tridhyatech.ticketrush.ui.screens.profile.inner.notification

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.profile.profileRoute

const val notificationRoute = "notification_screen_$profileRoute"

fun NavController.navigateToNotification(navOptions: NavOptions? = null) {
    this.navigate(notificationRoute, navOptions)
}

fun NavGraphBuilder.notificationScreen(appState: AppState) {
    composable(route = notificationRoute) {
        NotificationRoute(appState = appState)
    }
}