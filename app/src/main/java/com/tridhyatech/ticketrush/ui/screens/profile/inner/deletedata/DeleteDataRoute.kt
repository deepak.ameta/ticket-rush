package com.tridhyatech.ticketrush.ui.screens.profile.inner.deletedata

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.common.ProfileHeader
import com.tridhyatech.ticketrush.ui.theme.LightGrayTextColor
import com.tridhyatech.ticketrush.ui.utils.composables.CustomButton
import com.tridhyatech.ticketrush.ui.utils.composables.MySpinner
import kotlinx.coroutines.flow.collectLatest

@Composable
fun DeleteDataRoute(
    appState: AppState,
    viewModel: DeleteViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> {
                    appState.popHomeBackStack()
                }

                UiEvents.OnNext -> {
                    appState.popHomeBackStack()
                }

                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    DeleteDataScreen(uiState, viewModel::updateData)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DeleteDataScreen(
    uiState: DeleteState,
    onAction: (DeleteActions) -> Unit,
) {
    val initCountry = remember { mutableStateOf("India") }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp, vertical = 16.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.Top)
    ) {
        ProfileHeader(
            title = "Delete Your Account",
            onBackClick = { onAction(DeleteActions.OnBack) })
        Text(
            text = "To confirm you're the true owner of the account we may contact you within 48 hours.",
            style = TextStyle(
                fontSize = 16.sp,
                fontWeight = FontWeight(500),
                color = LightGrayTextColor,
            )
        )

        Box(
            Modifier.fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            MySpinner(
                label = "Where do you leave",
                list = uiState.countryList,
                onSelectionChanged = { onAction(DeleteActions.UpdateCounty(it)) },
                selection = uiState.selectedCountry,
            )
        }

        Column(
            modifier = Modifier.fillMaxWidth(1f)
        ) {
            Text(
                text = "Why are you deleting this account?",
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF000000),
                )
            )
            Spacer(Modifier.height(8.dp))
            OutlinedTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(160.dp),
                value = uiState.reason,
                onValueChange = { onAction(DeleteActions.UpdateReason(it)) },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                maxLines = 3,
            )
        }
        CustomButton(text = "Delete Account", onClick = { onAction(DeleteActions.OnBack) })
    }
}