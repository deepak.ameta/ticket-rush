package com.tridhyatech.ticketrush.ui.screens.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.common.domain.model.hotel.HotelDetail
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.ui.screens.home.HomeActions
import com.tridhyatech.ticketrush.ui.screens.home.HomeState

@Composable
fun RecommendationSection(
    uiState: HomeState,
    onAction: (HomeActions) -> Unit,
) {
    Column(
        modifier = Modifier.fillMaxWidth(1f),
        verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
    ) {
        Row(
            Modifier
                .fillMaxWidth(1f)
                .height(34.dp),
            horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                painter = painterResource(id = R.drawable.icon_recommendation),
                contentDescription = "image description",
                contentScale = ContentScale.None,
                modifier = Modifier
                    .padding(1.dp)
                    .width(20.dp)
                    .height(20.dp),
            )
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    modifier = Modifier,
                    text = "Recommendation",
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                    )
                )
                Box(
                    modifier = Modifier
                        .width(40.dp)
                        .height(24.dp)
                        .clip(RoundedCornerShape(24.dp))
                        .background(color = Color(0x1A0768FD))
                        .clickable { onAction(HomeActions.OnRecommendedSeeMore) }
                        .padding(start = 8.dp, end = 8.dp),
                    contentAlignment = Alignment.Center,
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.icon_right),
                        contentDescription = "recommended section icon",
                        contentScale = ContentScale.None,
                        modifier = Modifier
                            .padding(1.dp)
                            .width(16.dp)
                            .height(16.dp),
                    )
                }
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .horizontalScroll(rememberScrollState()),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            repeat(uiState.nearByHotelList.size) {
                RecommendationItem(
                    hotelDetail = uiState.nearByHotelList[it],
                    onAction = onAction,
                )
            }
        }
    }
}

@Composable
fun RecommendationItem(
    hotelDetail: HotelDetail,
    onAction: (HomeActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .width(161.dp)
            .clip(RoundedCornerShape(16.dp))
            .border(width = 1.dp, color = Color.LightGray, RoundedCornerShape(16.dp))
            .background(Color.White)
            .padding(bottom = 8.dp)
            .clickable { onAction(HomeActions.OnRecommendedItem(hotelDetail)) },
        verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Box(
            modifier = Modifier
                .width(161.dp)
                .height(128.dp)
                .clip(RoundedCornerShape(16.dp))
                .background(Color.LightGray)
        )
        Column(
            modifier = Modifier
                .width(161.dp)
                .padding(start = 8.dp, top = 8.dp, end = 8.dp, bottom = 4.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start,
        ) {
            Text(
                text = hotelDetail.name,
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                )
            )

            Row(
                modifier = Modifier,
                horizontalArrangement = Arrangement.spacedBy(2.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    painter = painterResource(id = R.drawable.icon_location_on),
                    contentDescription = "image description",
                    contentScale = ContentScale.None,
                    modifier = Modifier
                        .padding(1.dp)
                        .width(12.dp)
                        .height(12.dp),
                )
                Text(
                    text = hotelDetail.location,
                    style = TextStyle(
                        fontSize = 10.sp,
                        lineHeight = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                        letterSpacing = 0.8.sp,
                    )
                )
            }

            Text(
                text = "$${hotelDetail.price}",
                style = TextStyle(
                    fontSize = 9.sp,
                    lineHeight = 14.sp,
                    fontWeight = FontWeight(400),
                    fontStyle = FontStyle.Italic,
                    color = Color(0xFFBFBFBF),
                    textDecoration = TextDecoration.LineThrough,
                )
            )

            Row(
                modifier = Modifier.fillMaxWidth(1f),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = "$${hotelDetail.discountedPrice}/night",
                    style = TextStyle(
                        fontSize = 14.sp,
                        lineHeight = 14.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF398B2B),
                    )
                )
                Row(
                    horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.icon_star),
                        contentDescription = "rating icon",
                        contentScale = ContentScale.None,
                        modifier = Modifier
                            .padding(1.dp)
                            .width(16.dp)
                            .height(16.dp),
                    )
                    Text(
                        text = hotelDetail.rating.toString(),
                        style = TextStyle(
                            fontSize = 12.sp,
                            lineHeight = 14.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFFBFBFBF),
                            textAlign = TextAlign.Center,
                        )
                    )
                }
            }
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewRecommendationItem() {
//    RecommendationSection()
//}