package com.tridhyatech.ticketrush.ui.screens.profile.inner.requestdata

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.profile.profileRoute

const val requestRoute = "request_data_screen_$profileRoute"

fun NavController.navigateToRequest(navOptions: NavOptions? = null) {
    this.navigate(requestRoute, navOptions)
}

fun NavGraphBuilder.requestScreen(appState: AppState) {
    composable(route = requestRoute) {
        RequestRoute(appState = appState)
    }
}