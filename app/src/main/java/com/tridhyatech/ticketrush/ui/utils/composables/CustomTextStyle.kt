package com.tridhyatech.ticketrush.ui.utils.composables

import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticketrush.ui.theme.LightGrayTextColor
import com.tridhyatech.ticketrush.ui.theme.MyTextColor

fun transactionBoldStyle() =  TextStyle(
    fontSize = 14.sp,
    fontWeight = FontWeight(800),
    color = MyTextColor,
    textAlign = TextAlign.Center,
)

fun orderIdTextStyle() =  TextStyle(
    fontSize = 12.sp,
    fontWeight = FontWeight(400),
    color = LightGrayTextColor,
    textAlign = TextAlign.Center,
)