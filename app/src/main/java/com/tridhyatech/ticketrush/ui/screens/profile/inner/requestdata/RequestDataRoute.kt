package com.tridhyatech.ticketrush.ui.screens.profile.inner.requestdata

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.common.ProfileHeader
import com.tridhyatech.ticketrush.ui.theme.LightGrayTextColor
import com.tridhyatech.ticketrush.ui.utils.composables.CustomButton
import com.tridhyatech.ticketrush.ui.utils.composables.MySpinner
import kotlinx.coroutines.flow.collectLatest

@Composable
fun RequestRoute(
    appState: AppState,
    viewModel: RequestViewModel = hiltViewModel()
) {

    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> {
                    appState.popHomeBackStack()
                }

                UiEvents.OnNext -> {
                    appState.popHomeBackStack()
                }

                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    RequestScreen(uiState, viewModel::updateData)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RequestScreen(
    uiState: RequestState,
    onAction: (RequestActions) -> Unit,
) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp, vertical = 16.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.Top)
    ) {
        ProfileHeader(title = "Request Personal Data", onBackClick = {
            onAction(RequestActions.OnBack)
        })
        Text(
            text = "Before we make you a copy of your personal data, Please answer these questions.",
            style = TextStyle(
                fontSize = 16.sp,
                fontWeight = FontWeight(500),
                color = LightGrayTextColor,
            )
        )

        Box(
            Modifier.fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            MySpinner(
                label = "Where do you leave",
                list = uiState.countryList,
                onSelectionChanged = { onAction(RequestActions.UpdateCounty(it)) },
                selection = uiState.selectedCountry,
            )
        }

        Column(
            modifier = Modifier.fillMaxWidth(1f)
        ) {
            Text(
                text = "Why are you requesting your data?",
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF000000),
                )
            )
            Spacer(Modifier.height(8.dp))
            OutlinedTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(160.dp),
                value = uiState.reason,
                onValueChange = { onAction(RequestActions.UpdateReason(it)) },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                maxLines = 3,
            )
        }
        CustomButton(text = "Request data", onClick = {
            onAction(RequestActions.OnBack)
        })
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewRequestScreen() {
//    RequestScreen()
//}