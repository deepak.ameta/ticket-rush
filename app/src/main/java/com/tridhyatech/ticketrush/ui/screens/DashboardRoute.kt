package com.tridhyatech.ticketrush.ui.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import com.tridhyatech.ticketrush.navigation.DashboardNavHost
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.navigation.utils.screens

const val dashboardRoute = "dashboard_route"

fun NavController.navigateToDashboard(navOptions: NavOptions? = null) {
    this.navigate(dashboardRoute, navOptions)
}

fun NavGraphBuilder.dashboardScreen(
    appState: AppState,
) {
    composable(route = dashboardRoute) {
        DashboardRoute(appState = appState)
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DashboardRoute(
    appState: AppState
) {
    Scaffold(
        bottomBar = {
            val navHost = appState.homeNavController

            val navBackStackEntry by navHost.currentBackStackEntryAsState()
            val currentRoute = navBackStackEntry?.destination?.route
            var isTopLevelDestination = false

            screens.forEach {
                if (currentRoute == it.route) {
                    isTopLevelDestination = true
                    return@forEach
                }
            }

            TicketRushBottomBar(navController = appState.homeNavController)

//            AnimatedVisibility(
//                visible = isTopLevelDestination,
//                enter = fadeIn(),
//                exit = fadeOut()
//            ) {
//                TicketRushBottomBar(navController = appState.homeNavController)
//            }
        }) { pad ->
        Box(
            modifier = Modifier
                .padding(pad)
                .fillMaxSize()
        ) {
            DashboardNavHost(appState = appState)
        }
    }
}

@Composable
fun TicketRushBottomBar(navController: NavController) {

    val navHost = navController as NavHostController

    NavigationBar(
        modifier = Modifier,
        containerColor = Color.White,
    ) {
        val navBackStackEntry by navHost.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route

        screens.forEach { screen ->

            NavigationBarItem(
                label = { Text(text = screen.title!!) },
                icon = { Icon(imageVector = screen.icon!!, contentDescription = "") },
                selected = currentRoute?.contains(screen.route) ?: false,
                onClick = {
                    navController.navigate(screen.route) {
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        launchSingleTop = true
                        restoreState = true
                    }
                },
                colors = NavigationBarItemDefaults.colors(
                    unselectedTextColor = Color.Gray,
                    unselectedIconColor = Color.Gray,
                    selectedTextColor = Color.Blue,
                    selectedIconColor = Color.Blue,
                ),
            )
        }
    }
}