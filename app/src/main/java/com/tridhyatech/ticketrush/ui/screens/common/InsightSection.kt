package com.tridhyatech.ticketrush.ui.screens.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.ui.screens.home.HomeActions
import com.tridhyatech.ticketrush.ui.screens.home.HomeState

@Composable
fun InsightSection(
    uiState: HomeState,
    onAction: (HomeActions) -> Unit,
) {

    Column(modifier = Modifier.fillMaxWidth(1f)) {
        Row(
            Modifier
                .fillMaxWidth(1f)
                .height(34.dp),
            horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                painter = painterResource(id = R.drawable.icon_insight),
                contentDescription = "Insight section icon",
                contentScale = ContentScale.None,
                modifier = Modifier
                    .padding(1.dp)
                    .width(20.dp)
                    .height(20.dp),
            )
            Text(
                modifier = Modifier.fillMaxWidth(1f),
                text = "Insights",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(600),
                    color = Color(0xFF1B1446),
                )
            )
        }
        Spacer(Modifier.height(16.dp))
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(278.dp)
                .padding(bottom = 16.dp)
                .background(color = Color(0xFFD9D9D9), shape = RoundedCornerShape(size = 16.dp)),
            contentAlignment = Alignment.BottomCenter,
        ) {
            Row(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth()
                    .background(color = Color(0xFFFFFFFF), shape = RoundedCornerShape(size = 8.dp))
                    .padding(horizontal = 16.dp, vertical = 8.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Column(
                    modifier = Modifier.weight(weight = 1f, fill = true),
                    verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Bottom),
                    horizontalAlignment = Alignment.Start,
                ) {
                    Text(
                        modifier = Modifier,
                        text = "10 Tips agar perjalanan tidak membosankan",
                        style = TextStyle(
                            fontSize = 14.sp,
                            fontWeight = FontWeight(600),
                            color = Color(0xFF1B1446),
                        )
                    )
                    Row(
                        modifier = Modifier.width(100.dp),
                        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Row(
                            modifier = Modifier
                                .width(32.dp)
                                .height(12.dp),
                            horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            Image(
                                painter = painterResource(id = R.drawable.icon_view),
                                contentDescription = "image description",
                                contentScale = ContentScale.None,
                                modifier = Modifier
                                    .padding(1.dp)
                                    .width(10.dp)
                                    .height(10.dp),
                            )
                            Text(
                                text = "123",
                                style = TextStyle(
                                    fontSize = 10.sp,
                                    fontWeight = FontWeight(500),
                                    color = Color(0xFF808080),
                                )
                            )
                        }
                        Row(
                            modifier = Modifier
                                .width(32.dp)
                                .height(12.dp),
                            horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            Image(
                                painter = painterResource(id = R.drawable.icon_like),
                                contentDescription = "image description",
                                contentScale = ContentScale.None,
                                modifier = Modifier
                                    .padding(1.dp)
                                    .width(10.dp)
                                    .height(10.dp),
                            )
                            Text(
                                text = "123",
                                style = TextStyle(
                                    fontSize = 10.sp,
                                    fontWeight = FontWeight(500),
                                    color = Color(0xFF808080),
                                )
                            )
                        }
                    }
                }

                Row(
                    modifier = Modifier
                        .height(32.dp)
                        .clip(RoundedCornerShape(8.dp))
                        .background(color = Color(0xFF0768FD))
                        .padding(start = 8.dp, end = 8.dp)
                        .clickable { onAction(HomeActions.OnInsightItem("")) },
                    horizontalArrangement = Arrangement.spacedBy(
                        8.dp,
                        Alignment.CenterHorizontally
                    ),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.icon_read_more),
                        contentDescription = "image description",
                        contentScale = ContentScale.None,
                        modifier = Modifier
                            .padding(1.dp)
                            .size(16.dp),
                    )
                    Text(
                        modifier = Modifier.height(15.dp),
                        text = "Read More",
                        style = TextStyle(
                            fontSize = 12.sp,
                            fontWeight = FontWeight(600),
                            color = Color(0xFFFFFFFF),
                        )
                    )
                }
            }
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewInsightSection() {
//    InsightSection()
//}