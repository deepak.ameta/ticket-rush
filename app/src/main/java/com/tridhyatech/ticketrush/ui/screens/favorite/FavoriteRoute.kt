package com.tridhyatech.ticketrush.ui.screens.favorite

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.domain.model.hotel.HotelDetail
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.utils.composables.CustomTextField
import kotlinx.coroutines.flow.collectLatest

@Composable
fun FavoriteRoute(
    appState: AppState,
    viewModel: FavoriteViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> {}
                UiEvents.OnNext -> {}
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    FavoriteScreen(uiState, viewModel::updateData)
}


@Composable
internal fun FavoriteScreen(
    uiState: FavoriteState,
    onAction: (FavoriteActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color(0xFFF6F8FB)),
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White)
                .padding(horizontal = 16.dp, vertical = 16.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.End),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(
                modifier = Modifier.weight(1f),
                contentAlignment = Alignment.Center
            ) {
                CustomTextField(
                    value = uiState.favEt,
                    placeholderText = "Find your favorites",
                    onValueChange = { onAction(FavoriteActions.UpdateEditText(it)) },
                    leadingIcon = {
                        Image(
                            painter = painterResource(id = R.drawable.icon_search),
                            contentDescription = "image description",
                            contentScale = ContentScale.None,
                            modifier = Modifier
                                .padding(1.dp)
                                .width(20.dp)
                                .height(20.dp),
                        )
                    }
                )
            }
            Box(
                modifier = Modifier
                    .width(56.dp)
                    .height(37.dp)
                    .clip(RoundedCornerShape(32.dp))
                    .border(1.dp, Color(0x336D6B6B), RoundedCornerShape(size = 32.dp))
                    .background(Color.White)
                    .clickable { onAction(FavoriteActions.OnNotificationIcon) }
                    .padding(horizontal = 16.dp, vertical = 5.dp),
                contentAlignment = Alignment.TopCenter
            ) {
                Image(
                    painter = painterResource(R.drawable.icon_mail_black),
                    contentDescription = "",
                    contentScale = ContentScale.Fit,
                    modifier = Modifier
                        .padding(1.dp)
                        .width(24.dp)
                        .height(24.dp),
                )
                Image(
                    painter = painterResource(R.drawable.unread_mail_notifier),
                    contentDescription = "",
                    contentScale = ContentScale.Fit,
                    modifier = Modifier
                        .offset(x = 10.dp, y = 1.dp)
                        .padding(1.dp)
                        .width(7.75.dp)
                        .height(7.75.dp)
                        .background(
                            color = Color(0xFFFFBE41),
                            shape = RoundedCornerShape(12.dp),
                        ),
                )
            }
        }

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 16.dp, vertical = 16.dp),
            content = {
                items(uiState.favHotelsList.size) {
                    FavoriteScreenItem(uiState.favHotelsList[it], onAction)
                }
            }
        )
    }
}

@Composable
fun FavoriteScreenItem(
    hotelDetail: HotelDetail,
    onAction: (FavoriteActions) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(bottom = 12.dp)
            .height(114.dp)
            .clip(RoundedCornerShape(16.dp))
            .border(1.dp, Color(0x0D012276), RoundedCornerShape(16.dp))
            .background(color = Color(0xFFFFFFFF))
            .clickable { onAction(FavoriteActions.OnItemSelection(hotelDetail)) },
        horizontalArrangement = Arrangement.spacedBy(0.dp, Alignment.CenterHorizontally),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Box(
            modifier = Modifier
                .width(134.dp)
                .height(114.dp)
                .padding(3.dp)
                .clip(RoundedCornerShape(16.dp))
                .border(1.dp, Color(0x0D012276), RoundedCornerShape(size = 16.dp))
                .background(Color.LightGray, RoundedCornerShape(size = 16.dp))
        )
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 16.dp, vertical = 0.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start,
        ) {
            Text(
                text = hotelDetail.name,
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                    textAlign = TextAlign.Center,
                )
            )
            Row(
                modifier = Modifier,
                horizontalArrangement = Arrangement.spacedBy(2.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    painter = painterResource(id = R.drawable.icon_location_on),
                    contentDescription = "location icon",
                    contentScale = ContentScale.None,
                    modifier = Modifier
                        .padding(1.dp)
                        .width(12.dp)
                        .height(12.dp),
                )
                Text(
                    text = hotelDetail.location,
                    style = TextStyle(
                        fontSize = 10.sp,
                        lineHeight = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                        letterSpacing = 0.8.sp,
                    )
                )
            }

            Spacer(Modifier.height(8.dp))
            /*Text(
                text = "$225",
                style = TextStyle(
                    fontSize = 11.sp,
                    lineHeight = 14.sp,
                    fontWeight = FontWeight(500),
                    fontStyle = FontStyle.Italic,
                    color = Color(0xFFBFBFBF),
                    textDecoration = TextDecoration.LineThrough,
                )
            )*/
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(2.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        text = "$${hotelDetail.price}",
                        style = TextStyle(
                            fontSize = 14.sp,
                            fontWeight = FontWeight(600),
                            color = Color(0xFF398B2B),
                        )
                    )
                    Text(
                        text = "/night",
                        style = TextStyle(
                            fontSize = 12.sp,
                            fontWeight = FontWeight(400),
                            color = Color(0xFF808080),
                        )
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.spacedBy(2.dp, Alignment.End),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.icon_star),
                        contentDescription = "rating icon",
                        contentScale = ContentScale.None,
                        modifier = Modifier
                            .padding(1.dp)
                            .size(16.dp),
                    )
                    Text(
                        text = hotelDetail.rating.toString(),
                        style = TextStyle(
                            fontSize = 12.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFFBFBFBF),
                            textAlign = TextAlign.Center,
                        )
                    )
                }
            }
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewFavoriteScreen() {
//    FavoriteScreen()
//}