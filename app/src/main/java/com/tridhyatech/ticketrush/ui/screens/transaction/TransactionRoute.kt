package com.tridhyatech.ticketrush.ui.screens.transaction

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.domain.model.PaymentInfo
import com.tridhyatech.ticket.common.utils.PaymentStatus
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.theme.BorderLightColor
import com.tridhyatech.ticketrush.ui.theme.LightGrayTextColor
import com.tridhyatech.ticketrush.ui.utils.composables.CustomTextField
import com.tridhyatech.ticketrush.ui.utils.composables.orderIdTextStyle
import com.tridhyatech.ticketrush.ui.utils.composables.transactionBoldStyle
import kotlinx.coroutines.flow.collectLatest

@Composable
fun TransactionRoute(
    appState: AppState,
    viewModel: TransactionViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> {}
                UiEvents.OnNext -> appState.navigateToFlightBooking(context)
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    TransactionScreen(uiState, viewModel::updateData)
}

@Composable
internal fun TransactionScreen(
    uiState: TransactionState,
    onAction: (TransactionActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color(0xFFF6F8FB)),
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White)
                .padding(horizontal = 16.dp, vertical = 16.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.End),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(
                modifier = Modifier.weight(1f),
                contentAlignment = Alignment.Center
            ) {
                CustomTextField(
                    value = uiState.transactionEt,
                    placeholderText = "Find your transactions",
                    onValueChange = { onAction(TransactionActions.UpdateEditText(it)) },
                    leadingIcon = {
                        Image(
                            modifier = Modifier
                                .padding(1.dp)
                                .size(20.dp),
                            painter = painterResource(id = R.drawable.icon_search),
                            contentDescription = "image description",
                            contentScale = ContentScale.None,
                        )
                    }
                )
            }
            Box(
                modifier = Modifier
                    .width(56.dp)
                    .height(37.dp)
                    .clip(RoundedCornerShape(32.dp))
                    .border(1.dp, Color(0x336D6B6B), RoundedCornerShape(size = 32.dp))
                    .background(Color.White)
                    .clickable { onAction(TransactionActions.OnNotificationIcon) }
                    .padding(horizontal = 16.dp, vertical = 5.dp),
                contentAlignment = Alignment.TopCenter
            ) {
                Image(
                    painter = painterResource(R.drawable.icon_mail_black),
                    contentDescription = "",
                    contentScale = ContentScale.Fit,
                    modifier = Modifier
                        .padding(1.dp)
                        .width(24.dp)
                        .height(24.dp),
                )
                Image(
                    painter = painterResource(R.drawable.unread_mail_notifier),
                    contentDescription = "",
                    contentScale = ContentScale.Fit,
                    modifier = Modifier
                        .offset(x = 10.dp, y = 1.dp)
                        .padding(1.dp)
                        .width(7.75.dp)
                        .height(7.75.dp)
                        .background(
                            color = Color(0xFFFFBE41),
                            shape = RoundedCornerShape(12.dp),
                        ),
                )
            }
        }

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 16.dp, vertical = 16.dp),
            content = {
                items(uiState.paymentDataList.size) {
                    TransactionScreenItem(uiState.paymentDataList[it], onAction)
                }
            }
        )
    }
}

@Composable
fun TransactionScreenItem(
    paymentInfo: PaymentInfo,
    onAction: (TransactionActions) -> Unit,
) {
    val isCompleted = paymentInfo.status == PaymentStatus.COMPLETED

    Box(
        modifier = Modifier
            .fillMaxSize(1f)
            .padding(8.dp),
        contentAlignment = Alignment.TopCenter
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp))
                .border(1.dp, BorderLightColor, RoundedCornerShape(12.dp))
                .padding(1.dp)
                .background(color = Color.White)
                .padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp, Alignment.Top),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(text = "ORDER ID: ${paymentInfo.orderId}", style = orderIdTextStyle())
                Icon(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(20.dp),
                    imageVector = Icons.Default.MoreVert,
                    contentDescription = "Icon more vertical menu",
                    tint = LightGrayTextColor
                )
            }
            Column(
                modifier = Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Row(
                        modifier = Modifier,
                        horizontalArrangement = Arrangement.spacedBy(
                            12.dp, alignment = Alignment.Start
                        ),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.flight_takeoff),
                            contentDescription = "flight icon",
                            contentScale = ContentScale.None,
                            modifier = Modifier
                                .padding(1.dp)
                                .size(20.dp),
                        )
                        Text(
                            text = paymentInfo.routeName,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight(500),
                                color = Color(0xFF000000),
                                textAlign = TextAlign.Center,
                            ),
                        )
                    }
                    Text(
                        text = paymentInfo.date,
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFF000000),
                            textAlign = TextAlign.Center,
                        ),
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Box(
                        modifier = Modifier
                            .background(
                                color = if (isCompleted) Color(0xFFDBF1DB) else Color(0xFFEBE8D1),
                                shape = RoundedCornerShape(4.dp)
                            )
                            .padding(horizontal = 4.dp, vertical = 2.dp),
                        contentAlignment = Alignment.Center,
                    ) {
                        Text(
                            text = if (isCompleted) "Completed" else "Pending",
                            style = TextStyle(
                                fontSize = 12.sp,
                                fontWeight = FontWeight(500),
                                color = if (isCompleted) Color(0xFF3A5E10) else Color(0xFF70681C),
                                textAlign = TextAlign.Center,
                            ),
                        )
                    }
                    Text(text = "INR ${paymentInfo.price}", style = transactionBoldStyle())
                }
            }

            Button(
                modifier = Modifier.fillMaxWidth(),
                onClick = { onAction(TransactionActions.OnTransactionItem(paymentInfo)) },
                shape = RoundedCornerShape(12.dp),
                colors = ButtonDefaults.buttonColors(
                    containerColor = if (isCompleted) Color.White else Color.Blue,
                    contentColor = if (isCompleted) Color(0xFF808080) else Color.White,
                    disabledContainerColor = if (isCompleted) Color.White else Color.Blue,
                    disabledContentColor = if (isCompleted) Color(0xFF808080) else Color.White,
                ),
                border = if (isCompleted) BorderStroke(1.dp, LightGrayTextColor) else null,
                content = { Text(text = if (isCompleted) "See details" else "Complete the payment") }
            )
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewTransactionScreenItem() {
//    TransactionScreenItem()
//}