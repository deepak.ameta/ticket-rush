package com.tridhyatech.ticketrush.ui.screens.profile.inner.edit_info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.profileData
import com.tridhyatech.ticket.common.domain.ProfileInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EditInfoViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(ProfileInfo())
    val uiState: StateFlow<ProfileInfo> = _uiState.asStateFlow()

    init {
        loadData()
    }

    private fun loadData() {
        viewModelScope.launch {
            val profileJson = session.getValue(profileData, "")
            val data = Gson().fromJson(profileJson, ProfileInfo::class.java)
            _uiState.value = data
        }
    }

    fun updateData(action: EditInfoActions) {
        when (action) {
            EditInfoActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            EditInfoActions.OnNext -> {
                viewModelScope.launch {
                    val profileJson = Gson().toJson(_uiState.value)
                    session.setValue(profileData, profileJson)

                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            is EditInfoActions.OnEmailUpdate -> {
                _uiState.update { it.copy(email = action.value) }
            }

            is EditInfoActions.OnGenderUpdate -> {
                _uiState.update { it.copy(gender = action.value) }
            }

            is EditInfoActions.OnIdUpdate -> {
                _uiState.update { it.copy(id = action.value) }
            }

            is EditInfoActions.OnNameUpdate -> {
                _uiState.update { it.copy(name = action.value) }
            }

            is EditInfoActions.OnPhoneUpdate -> {
                _uiState.update { it.copy(phoneNo = action.value) }
            }
        }
    }
}

sealed interface EditInfoActions {
    object OnNext : EditInfoActions
    object OnBack : EditInfoActions
    data class OnNameUpdate(val value: String) : EditInfoActions
    data class OnGenderUpdate(val value: String) : EditInfoActions
    data class OnEmailUpdate(val value: String) : EditInfoActions
    data class OnPhoneUpdate(val value: String) : EditInfoActions
    data class OnIdUpdate(val value: String) : EditInfoActions
}