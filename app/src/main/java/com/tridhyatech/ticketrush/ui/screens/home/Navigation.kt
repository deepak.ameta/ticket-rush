package com.tridhyatech.ticketrush.ui.screens.home

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState

const val homeRoute = "home_route"

fun NavGraphBuilder.homeScreen(appState: AppState) {
    composable(route = homeRoute) { HomeRoute(appState = appState) }
}