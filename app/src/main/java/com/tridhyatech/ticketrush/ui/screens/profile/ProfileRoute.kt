package com.tridhyatech.ticketrush.ui.screens.profile

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.common.ProfileHeader
import com.tridhyatech.ticketrush.ui.theme.LightGrayTextColor
import kotlinx.coroutines.flow.collectLatest

@Composable
fun ProfileRoute(
    appState: AppState,
    viewModel: ProfileViewModel = hiltViewModel()
) {

    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current
    val activity = LocalContext.current as Activity

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> {
                    appState.popHomeBackStack()
                }

                UiEvents.OnNext -> { activity.finish() }
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    ProfileScreen(
        uiState = uiState,
        onAction = viewModel::updateData,
        onPersonalInfoClick = appState::navigateToPersonalInfo,
        onPrivacyClick = appState::navigateToPrivacy,
        onNotificationClick = appState::navigateToNotification,
        onReviewClick = appState::navigateToReview,
    )
}

@Composable
fun ProfileScreen(
    uiState: ProfileState,
    onAction: (ProfileActions) -> Unit,
    onPersonalInfoClick: () -> Unit = {},
    onPrivacyClick: () -> Unit = {},
    onNotificationClick: () -> Unit = {},
    onReviewClick: () -> Unit = {},
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 0.dp, vertical = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(0.dp, alignment = Alignment.Top)
    ) {
        ProfileHeader(title = "Profile", addPadding = true, onBackClick = {
            onAction(ProfileActions.OnBack)
        })
        ProfileImageSection(uiState = uiState, onAction = onAction)
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .background(LightGrayTextColor)
        )
        ProfileSectionRow(title = "Personal Info", label = "ACCOUNT", onClick = onPersonalInfoClick)
        ProfileSectionRow(title = "Privacy & Sharing", onClick = onPrivacyClick)
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .background(LightGrayTextColor)
        )
        ProfileSectionRow(title = "Notification", label = "SETTINGS", onClick = onNotificationClick)
//        ProfileSectionRow(title = "Appearance")
        ProfileSectionRow(title = "Review", onClick = onReviewClick)
        ProfileSectionRow(title = "Logout", onClick = { onAction(ProfileActions.OnLogout) })
    }
}

@Composable
fun ProfileImageSection(
    uiState: ProfileState,
    onAction: (ProfileActions) -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
    ) {
        Box(
            modifier = Modifier.size(100.dp),
            contentAlignment = Alignment.BottomEnd
        ) {
            Box(
                modifier = Modifier
                    .offset((-4).dp, (-4).dp)
                    .size(30.dp)
                    .clip(RoundedCornerShape(50))
                    .background(MyBlueColor)
                    .zIndex(2f)
                    .clickable { onAction(ProfileActions.OnEditProfile) },
                contentAlignment = Alignment.Center
            ) {
                Image(
                    imageVector = Icons.Default.Edit,
                    contentDescription = "Edit profile image",
                    contentScale = ContentScale.Fit,
                    colorFilter = ColorFilter.tint(Color.White),
                    modifier = Modifier
                        .padding(5.dp)
                        .size(20.dp),
                )
            }
            Box(
                modifier = Modifier
                    .height(100.dp)
                    .width(100.dp)
                    .background(LightGrayTextColor, shape = RoundedCornerShape(50)),
                contentAlignment = Alignment.Center
            ) {
                // will add image here after upload photo functionality.
            }
        }
        Text(
            text = uiState.name,
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight(500),
                color = Color(0xFF000000),
            )
        )
        Text(
            text = uiState.email,
            style = TextStyle(
                fontSize = 12.sp,
                fontWeight = FontWeight(500),
                color = LightGrayTextColor,
            )
        )
    }
}

@Composable
fun ProfileSectionRow(
    title: String,
    label: String? = null,
    onClick: () -> Unit = {},
) {
    Column(
        modifier = Modifier.fillMaxWidth(1f)
    ) {
        if (label != null) {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, top = 16.dp),
                text = label,
                style = TextStyle(
                    fontSize = 12.sp,
                    fontWeight = FontWeight(500),
                    color = LightGrayTextColor,
                )
            )
            Spacer(modifier = Modifier.height(8.dp))
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .clickable { onClick() }
                .padding(vertical = 16.dp, horizontal = 16.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = title,
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF000000),
                )
            )
            Image(
                painter = painterResource(id = R.drawable.arrow_back),
                contentDescription = "arrow back",
                contentScale = ContentScale.None,
                modifier = Modifier
                    .padding(1.dp)
                    .size(20.dp)
                    .rotate(180f),
            )
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewProfileScreen() {
//    ProfileScreen(ProfileState(), {})
//}