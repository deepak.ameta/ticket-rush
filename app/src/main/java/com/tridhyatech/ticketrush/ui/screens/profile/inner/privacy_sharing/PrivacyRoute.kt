package com.tridhyatech.ticketrush.ui.screens.profile.inner.privacy_sharing

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.common.ProfileHeader
import com.tridhyatech.ticketrush.ui.theme.LightGrayTextColor

@Composable
fun PrivacyRoute(appState: AppState) {

    PrivacyScreen(
        onBackClick = appState::popHomeBackStack,
        onRequestDataClick = appState::navigateToRequestData,
        onDeleteDataClick = appState::navigateToDeleteData,
    )
}

@Composable
fun PrivacyScreen(
    onBackClick: () -> Unit = {},
    onRequestDataClick: () -> Unit = {},
    onDeleteDataClick: () -> Unit = {},
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp, vertical = 16.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.Top)
    ) {
        ProfileHeader(title = "Privacy & Sharing", onBackClick = onBackClick)
        Spacer(Modifier.height(8.dp))
        Column {
            Text(
                text = "Manage your account data",
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF000000),
                )
            )
            Spacer(Modifier.height(8.dp))
            Text(
                text = "You can make a request to download or delete your personal data from Travely.",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = LightGrayTextColor,
                )
            )
        }

        Spacer(Modifier.height(8.dp))
        RequestDataSection(
            title = "Request your personal data",
            subTitle = "We'll create an file for you to download your personal data.",
            onClick = onRequestDataClick
        )
        RequestDataSection(
            title = "Delete your account",
            subTitle = "After completing this your account and data will be permanently deleted.",
            onClick = onDeleteDataClick
        )
    }
}

@Composable
fun RequestDataSection(
    title: String,
    subTitle: String,
    onClick: () -> Unit = {},
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .clickable { onClick() },
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Column(
            modifier = Modifier.weight(1f)
        ) {
            Text(
                text = title,
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF000000),
                )
            )
            Text(
                text = subTitle,
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = LightGrayTextColor,
                )
            )
        }
        Image(
            painter = painterResource(id = R.drawable.icon_right),
            contentDescription = "arrow back",
            contentScale = ContentScale.None,
            modifier = Modifier
                .padding(1.dp)
                .width(20.dp)
                .height(20.dp),
        )
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewPrivacy() {
//    PrivacyScreen(onDeleteDataClick = {}, onRequestDataClick = {})
//}