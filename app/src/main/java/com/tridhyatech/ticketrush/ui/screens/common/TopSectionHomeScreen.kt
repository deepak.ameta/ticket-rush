package com.tridhyatech.ticketrush.ui.screens.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.ui.screens.home.HomeActions
import com.tridhyatech.ticketrush.ui.screens.home.HomeState

@Composable
fun TopSectionHomeScreen(
    uiState: HomeState,
    onAction: (HomeActions) -> Unit,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(194.dp)
    ) {
        Image(
            painter = painterResource(R.drawable.bg_image_home),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.fillMaxSize(),
        )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(vertical = 16.dp, horizontal = 16.dp),
            verticalArrangement = Arrangement.SpaceBetween,
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                Column(
                    modifier = Modifier.padding(top = 24.dp)
                ) {
                    Text(
                        text = "GOOD DAY",
                        style = TextStyle(
                            fontSize = 10.sp,
                            lineHeight = 14.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFFFFFFFF),
                            letterSpacing = 0.8.sp,
                        )
                    )
                    Text(
                        text = uiState.userLocation,
                        style = TextStyle(
                            fontSize = 20.sp,
                            fontWeight = FontWeight(600),
                            color = Color(0xFFFFFFFF),
                        )
                    )
                }
                Box(
                    modifier = Modifier
                        .width(56.dp)
                        .height(37.dp)
                        .clip(RoundedCornerShape(32.dp))
                        .background(Color(0x33FFFFFF))
                        .clickable { onAction(HomeActions.OnNotificationIcon) }
                        .padding(16.dp, 5.dp),
                    contentAlignment = Alignment.TopCenter
                ) {
                    Image(
                        painter = painterResource(R.drawable.mail),
                        contentDescription = "",
                        contentScale = ContentScale.Fit,
                        modifier = Modifier
                            .padding(1.dp)
                            .width(24.dp)
                            .height(24.dp),
                    )
                    Image(
                        painter = painterResource(R.drawable.unread_mail_notifier),
                        contentDescription = "",
                        contentScale = ContentScale.Fit,
                        modifier = Modifier
                            .offset(x = 10.dp, y = 1.dp)
                            .padding(1.dp)
                            .width(7.75.dp)
                            .height(7.75.dp)
                            .background(
                                color = Color(0xFFFFBE41),
                                shape = RoundedCornerShape(12.dp),
                            ),
                    )
                }
            }

            Row(
                modifier = Modifier
                    .width(343.dp)
                    .height(48.dp)
                    .clip(RoundedCornerShape(16.dp))
                    .border(1.dp, Color(0x0D012276), RoundedCornerShape(size = 16.dp))
                    .background(Color.White)
                    .padding(16.dp, 2.dp),
                horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    painter = painterResource(id = R.drawable.icon_search),
                    contentDescription = "image description",
                    contentScale = ContentScale.None,
                    modifier = Modifier
                        .padding(1.dp)
                        .width(20.dp)
                        .height(20.dp),
                )
                BasicTextField(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .padding(horizontal = 8.dp, vertical = 8.dp),
                    value = uiState.exploreEt,
                    onValueChange = {
                        onAction(HomeActions.OnExploreETUpdate(it))
                    },
                    keyboardOptions = KeyboardOptions(
                        capitalization = KeyboardCapitalization.None,
                        autoCorrect = false,
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Done
                    ),
                    maxLines = 1,
                    singleLine = true,
                    textStyle = TextStyle(
                        fontSize = 14.sp,
                        lineHeight = 21.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                    )
                )
            }
        }
    }
}