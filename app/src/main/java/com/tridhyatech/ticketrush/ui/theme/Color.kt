package com.tridhyatech.ticketrush.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val MyTextColor = Color(0xFF1B1446)
val MySpinnerTextColor = Color(0xFFBFBFBF)
val LightGrayTextColor = Color(0xFFBFBFBF)
val BorderLightColor = Color(0x0D012276)

val profileArrowColor = Color(0xFF0768FD)
val borderProfileArrow = Color(0x0D012276)