package com.tridhyatech.ticketrush.ui.screens.profile.inner.edit_info

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.profile.profileRoute

const val editInfoRoute = "edit_personal_info_screen_$profileRoute"

fun NavController.navigateToEditInfo(navOptions: NavOptions? = null) {
    this.navigate(editInfoRoute, navOptions)
}

fun NavGraphBuilder.editInfoScreen(appState: AppState) {
    composable(route = editInfoRoute) {
        EditInfoRoute(appState = appState)
    }
}