package com.tridhyatech.ticketrush.ui.screens.transaction

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.domain.model.PaymentInfo
import com.tridhyatech.ticket.common.utils.dummyPaymentDataList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(TransactionState())
    val uiState: StateFlow<TransactionState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {

        }
    }

    fun updateData(action: TransactionActions) {
        when (action) {
            TransactionActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            TransactionActions.OnNext -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            TransactionActions.OnNotificationIcon -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("No new notifications"))
                }
            }

            is TransactionActions.OnTransactionItem -> {
                // will open specific module payment page
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnError("Will open specific module payment page"))
                }
            }

            is TransactionActions.UpdateEditText -> {
                _uiState.update { it.copy(transactionEt = action.text) }
            }
        }
    }
}

data class TransactionState(
    val transactionEt: String = "",
    val isAnyUnreadNotification: Boolean = false,
    val paymentDataList: ArrayList<PaymentInfo> = dummyPaymentDataList,
)

sealed interface TransactionActions {
    object OnNext : TransactionActions
    object OnBack : TransactionActions
    object OnNotificationIcon : TransactionActions
    class OnTransactionItem(val paymentInfo: PaymentInfo) : TransactionActions
    class UpdateEditText(val text: String) : TransactionActions
}