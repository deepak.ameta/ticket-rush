package com.tridhyatech.ticketrush.ui.screens.profile.inner.review

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.profile.profileRoute

const val reviewRoute = "review_screen_$profileRoute"

fun NavController.navigateToReview(navOptions: NavOptions? = null) {
    this.navigate(reviewRoute, navOptions)
}

fun NavGraphBuilder.reviewScreen(appState: AppState) {
    composable(route = reviewRoute) {
        ReviewRoute(appState = appState)
    }
}