package com.tridhyatech.ticketrush.ui.screens.profile.inner.review

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.domain.model.ReviewData
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticketrush.R
import com.tridhyatech.ticketrush.navigation.utils.AppState
import com.tridhyatech.ticketrush.ui.screens.common.ProfileHeader
import kotlinx.coroutines.flow.collectLatest

@Composable
fun ReviewRoute(
    appState: AppState,
    viewModel: ReviewViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> {
                    appState.popHomeBackStack()
                }

                UiEvents.OnNext -> {}
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    ReviewScreen(uiState, viewModel::updateData)
}

@Composable
fun ReviewScreen(
    uiState: ReviewState,
    onActions: (ReviewActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = 16.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.Top)
    ) {
        ProfileHeader(title = "Your Review", onBackClick = {
            onActions(ReviewActions.OnBack)
        }, addPadding = true)

        LazyColumn(modifier = Modifier.fillMaxSize(1f)) {
            val list = uiState.reviewList
            items(list.size) {
                ReviewItem(data = list[it])
            }
        }
    }
}

@Composable
fun ReviewItem(data: ReviewData) {
    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(start = 16.dp, end = 16.dp, bottom = 12.dp)
            .border(
                width = 1.dp,
                color = Color(0x0D012276),
                shape = RoundedCornerShape(size = 16.dp)
            )
            .padding(16.dp),
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.Top
        ) {
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.SpaceEvenly,
                horizontalAlignment = Alignment.Start
            ) {
                Row(
                    modifier = Modifier
                ) {
                    repeat(5) { index ->
                        val starColor =
                            if (index < data.rating) MaterialTheme.colorScheme.primary
                            else Color.Gray

                        Icon(
                            modifier = Modifier.size(20.dp),
                            imageVector = Icons.Default.Star,
                            contentDescription = "star icon",
                            tint = starColor
                        )
                    }
                }

                Row(
                    horizontalArrangement = Arrangement.Start,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.icon_like),
                        contentDescription = null,
                        modifier = Modifier.size(22.dp),
                    )
                    Spacer(modifier = Modifier.width(4.dp))
                    Text(
                        text = "${data.usefulCount} PEOPLE THINK THIS IS USEFUL",
                        style = TextStyle(
                            fontSize = 8.sp,
                            lineHeight = 12.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFFBFBFBF),
                            textAlign = TextAlign.Center,
                            letterSpacing = 0.8.sp,
                        ),
                    )
                }

            }
            Text(
                modifier = Modifier,
                text = data.date,
                style = TextStyle(
                    fontSize = 10.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF2E2E2E),
                    textAlign = TextAlign.Center,
                ),
            )
        }
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp)
                .height(2.dp)
                .background(Color(0xFFBFBFBF))
        )
        Text(
            text = data.quote,
            style = TextStyle(
                fontSize = 14.sp,
                fontWeight = FontWeight(500),
                color = Color(0xFFBFBFBF),
                textAlign = TextAlign.Start,
                lineHeight = 20.sp,
            ),
            maxLines = 10
        )
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewReviewScreen() {
//    ReviewScreen()
//}