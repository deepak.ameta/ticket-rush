package com.tridhyatech.ticket.bus

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.Text
import com.tridhyatech.ticket.bus.common.BasicScreen
import com.tridhyatech.ticket.bus.ui.theme.TicketRushTheme

class BusActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TicketRushTheme {
                BasicScreen {
                    Text(text = "Bus Activity")
                }
            }
        }
    }
}