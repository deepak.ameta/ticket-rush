package com.tridhyatech.ticket.flight.ui.screens.payment.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import com.tridhyatech.ticket.flight.ui.screens.payment.CompletePaymentActions
import com.tridhyatech.ticket.flight.ui.screens.payment.CompletePaymentState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor

@Composable
fun PaymentMethodSection(
    uiState: CompletePaymentState,
    onAction: (CompletePaymentActions) -> Unit,
) {
    if (uiState.selectPaymentDialog) {
        DialogWithImage(
            selectedPayment = uiState.selectedPaymentMethod,
            onConfirmation = {
                onAction(CompletePaymentActions.UpdatePaymentMethod(it))
            },
            onDismissRequest = {
                onAction(CompletePaymentActions.TogglePaymentDialog(false))
            },
        )
    }
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.Start,
    ) {
        Text(
            text = "Payment Method",
            style = TextStyle(
                fontSize = 18.sp,
                fontWeight = FontWeight(600),
                color = DarkBlueTextColor,
                textAlign = TextAlign.Center,
            )
        )
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(0.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(
                modifier = Modifier
                    .width(48.dp)
                    .height(32.dp)
                    .clip(RoundedCornerShape(4.dp))
                    .border(1.dp, FlightBorderColor, RoundedCornerShape(5.dp))
                    .background(Color(0xFFDDDDDD))
            )
            Text(
                modifier = Modifier.weight(1f),
                text = uiState.selectedPaymentMethod,
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = DarkBlueTextColor,
                    textAlign = TextAlign.Start,
                )
            )

            Text(
                modifier = Modifier.clickable {
                    onAction(CompletePaymentActions.TogglePaymentDialog(true))
                },
                text = "Change",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color.Red,
                    textAlign = TextAlign.Start,
                )
            )
        }
    }
}

@Composable
fun DialogWithImage(
    selectedPayment: String,
    onDismissRequest: () -> Unit,
    onConfirmation: (String) -> Unit,
) {
    val selectedColor = MyBlueColor
    val unselectedColor = Color(0xFFDDDDDD)

    Dialog(onDismissRequest = { onDismissRequest() }) {
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .background(unselectedColor)
                .padding(24.dp, 16.dp),
            verticalArrangement = Arrangement.spacedBy(2.dp, Alignment.CenterVertically),
            horizontalAlignment = Alignment.Start,
        ) {
            Text(
                text = "Select Payment Method",
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight(600),
                    color = DarkBlueTextColor,
                    textAlign = TextAlign.Center,
                )
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .background(if (selectedPayment == "Paytm") selectedColor else unselectedColor)
                    .clickable { onConfirmation("Paytm") }
                    .padding(4.dp, 4.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Box(
                    modifier = Modifier
                        .width(48.dp)
                        .height(32.dp)
                        .clip(RoundedCornerShape(4.dp))
                        .border(1.dp, FlightBorderColor, RoundedCornerShape(4.dp))
                        .background(LightGrayTextColor)
                )
                Text(
                    modifier = Modifier.weight(1f),
                    text = "Paytm",
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(500),
                        color = if (selectedPayment == "Paytm") Color.White else DarkBlueTextColor,
                        textAlign = TextAlign.Start,
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .background(if (selectedPayment == "PhonePe") selectedColor else unselectedColor)
                    .clickable { onConfirmation("PhonePe") }
                    .padding(4.dp, 4.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Box(
                    modifier = Modifier
                        .width(48.dp)
                        .height(32.dp)
                        .clip(RoundedCornerShape(4.dp))
                        .border(1.dp, FlightBorderColor, RoundedCornerShape(4.dp))
                        .background(LightGrayTextColor)
                )
                Text(
                    modifier = Modifier.weight(1f),
                    text = "PhonePe",
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(500),
                        color = if (selectedPayment == "PhonePe") Color.White else DarkBlueTextColor,
                        textAlign = TextAlign.Start,
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .background(if (selectedPayment == "GooglePay") selectedColor else unselectedColor)
                    .clickable { onConfirmation("GooglePay") }
                    .padding(4.dp, 4.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Box(
                    modifier = Modifier
                        .width(48.dp)
                        .height(32.dp)
                        .clip(RoundedCornerShape(4.dp))
                        .border(1.dp, FlightBorderColor, RoundedCornerShape(4.dp))
                        .background(LightGrayTextColor)
                )
                Text(
                    modifier = Modifier.weight(1f),
                    text = "GooglePay",
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(500),
                        color = if (selectedPayment == "GooglePay") Color.White else DarkBlueTextColor,
                        textAlign = TextAlign.Start,
                    )
                )
            }
        }
    }
}