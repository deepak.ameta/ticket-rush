package com.tridhyatech.ticket.flight.ui.screens.eticket

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.common.FlightCustomButton
import com.tridhyatech.ticket.flight.ui.common.FlightSectionCommonHeader
import com.tridhyatech.ticket.flight.ui.screens.eticket.composables.SeatNumberSection
import com.tridhyatech.ticket.flight.ui.theme.HomeScreenBgColor

@Composable
fun TicketRoute(state: FlightState) {
    val context = LocalContext.current

    TicketScreen(
        onBackClick = state::popBackStack,
        onDownloadClick = { state.finishFlightActivity(context) },
    )
}

@Composable
fun TicketScreen(
    onBackClick: () -> Unit,
    onDownloadClick: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize(1f)
            .background(HomeScreenBgColor),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .background(Color.White)
                .padding(bottom = 16.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            FlightSectionCommonHeader(title = "E-Ticket", onBackClick = onBackClick)
        }
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .weight(1f, true)
                .padding(horizontal = 16.dp)
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            SeatNumberSection()
        }
        FlightCustomButton(
            modifier = Modifier
                .background(Color.White)
                .fillMaxWidth(1f)
                .padding(16.dp, 16.dp, 16.dp, 16.dp),
            text = "Download E-Ticket",
            onClick = onDownloadClick
        )
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewTicketPage() {
//    TicketPage()
//}