package com.tridhyatech.ticket.flight.ui.screens.home

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.navigation.utils.FlightState

const val homeRoute = "home_route"

fun NavGraphBuilder.homeScreen(state: FlightState) {
    composable(route = homeRoute) {
        FlightHomeRoute(state = state)
    }
}