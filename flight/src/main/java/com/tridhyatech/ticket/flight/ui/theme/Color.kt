package com.tridhyatech.ticket.flight.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val HomeScreenBgColor = Color(0xFFF6F8FB) // 0xFFF3F891 // 0xFFF6F8FB
val FlightBorderColor = Color(0x0D012276) // 0x4D012276 // 0x0D012276

val MyBlueColor = Color(0xFF0768FD)
val MyOrangeColor = Color(0xFFFF9141)
val DarkBlueTextColor = Color(0xFF1B1446)
val LightGrayTextColor = Color(0xFF808080)