package com.tridhyatech.ticket.flight.ui.screens.booking.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingActions
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor

@Composable
fun ContactDetailsSection(
    uiState: FlightBookingState,
    onAction: (FlightBookingActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.Start,
    ) {

        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Row(
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = "Contact Details",
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(600),
                        color = DarkBlueTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    text = "(FOR E-TICKET/VOUCHER)",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = LightGrayTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
            }
            AnimatedVisibility(visible = uiState.contactDetail.name.isEmpty()) {
                Icon(
                    modifier = Modifier
                        .size(24.dp)
                        .clickable { onAction(FlightBookingActions.OnAddContactDetail) }
                        .padding(end = 4.dp),
                    imageVector = Icons.Default.Add,
                    contentDescription = "Edit icon",
                    tint = Color.DarkGray,
                )
            }
        }
        AnimatedVisibility(visible = uiState.contactDetail.name.isNotEmpty()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .clip(RoundedCornerShape(16.dp))
                    .border(1.dp, FlightBorderColor, RoundedCornerShape(size = 16.dp))
                    .background(Color.White)
                    .padding(12.dp, 8.dp),
                verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically),
                horizontalAlignment = Alignment.Start,
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Row(
                        modifier = Modifier,
                        horizontalArrangement = Arrangement.spacedBy(12.dp, Alignment.Start),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Box(
                            modifier = Modifier
                                .size(32.dp)
                                .background(FlightBorderColor, CircleShape)
                        )
                        Text(
                            text = uiState.contactDetail.name,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight(600),
                                color = DarkBlueTextColor,
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                    Icon(
                        modifier = Modifier
                            .size(24.dp)
                            .clickable { onAction(FlightBookingActions.OnAddContactDetail) }
                            .padding(end = 4.dp),
                        imageVector = Icons.Default.Edit,
                        contentDescription = "Edit icon",
                        tint = Color.DarkGray,
                    )
                }
                Column(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .clip(RoundedCornerShape(8.dp))
                        .border(0.5.dp, FlightBorderColor, RoundedCornerShape(8.dp))
                        .padding(8.dp),
                    verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
                    horizontalAlignment = Alignment.Start,
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(1f),
                        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Icon(
                            modifier = Modifier.size(20.dp),
                            imageVector = Icons.Default.Email,
                            contentDescription = "Email icon",
                            tint = MyBlueColor,
                        )
                        Text(
                            text = uiState.contactDetail.email,
                            style = TextStyle(
                                fontSize = 14.sp,
                                fontWeight = FontWeight(500),
                                color = LightGrayTextColor,
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                    Row(
                        modifier = Modifier.fillMaxWidth(1f),
                        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Icon(
                            modifier = Modifier.size(20.dp),
                            imageVector = Icons.Default.Phone,
                            contentDescription = "Phone icon",
                            tint = MyBlueColor,
                        )
                        Text(
                            text = uiState.contactDetail.mobileNo.toString(),
                            style = TextStyle(
                                fontSize = 14.sp,
                                fontWeight = FontWeight(500),
                                color = LightGrayTextColor,
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                }
            }
        }
    }
}