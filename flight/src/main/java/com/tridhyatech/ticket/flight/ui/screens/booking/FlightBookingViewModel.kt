package com.tridhyatech.ticket.flight.ui.screens.booking

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.allPassengerContactDataStore
import com.tridhyatech.ticket.common.data.session.contactDataStore
import com.tridhyatech.ticket.common.data.session.flightBookingDataStore
import com.tridhyatech.ticket.common.data.session.flightDataStore
import com.tridhyatech.ticket.common.data.session.passengerData
import com.tridhyatech.ticket.common.domain.model.flight.BookingDetail
import com.tridhyatech.ticket.common.domain.model.flight.ContactDetail
import com.tridhyatech.ticket.common.domain.model.flight.FlightDetail
import com.tridhyatech.ticket.common.domain.model.flight.PassengerDetail
import com.tridhyatech.ticket.common.utils.fromJson
import com.tridhyatech.ticket.common.utils.toJson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FlightBookingViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<BookingEvents>()
    val uiEvent: Flow<BookingEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(FlightBookingState())
    val uiState: StateFlow<FlightBookingState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val flight = session.getValue(flightDataStore, "").fromJson<FlightDetail>()
            val passenger = session.getValue(passengerData, "").fromJson<PassengerDetail>()
            _uiState.update {
                it.copy(
                    flightDetail = flight!!,
                    passengerDetail = passenger!!,
                )
            }
        }
        loadData()
    }

    fun loadData() {
        viewModelScope.launch {
            val contact = session.getValue(contactDataStore, "").fromJson<ContactDetail>()
            val passenger = session.getValue(allPassengerContactDataStore, "")
                .fromJson<List<ContactDetail>>()

            if (passenger?.size!! > 0) _uiState.update {
                it.copy(
                    contactDetail = contact ?: ContactDetail(),
                    passengerInfoDetail = ArrayList(passenger)
                )
            } else {
                _uiState.update { it.copy(contactDetail = contact ?: ContactDetail()) }
            }
        }
    }

    fun updateData(action: FlightBookingActions) {
        when (action) {
            FlightBookingActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(BookingEvents.OnBack)
                }
            }

            FlightBookingActions.OnNext -> {
                val state = uiState.value
                viewModelScope.launch {
                    session.setValue(
                        flightBookingDataStore, BookingDetail(
                            price = state.flightDetail.price,
                            luggagePrice = state.luggagePrice.toDouble(),
                            insuranceAdded = state.insuranceAdded,
                            insurancePrice = if (state.insuranceAdded) 12.0 else 0.0,
                            luggageWeight = if (state.luggagePrice == 510) 10 else if (state.luggagePrice == 210) 5 else 0,
                            finalPrice = state.flightDetail.price + state.luggagePrice + if (state.insuranceAdded) 12.0 else 0.0
                        ).toJson()
                    )
                    _uiEvent.send(BookingEvents.OnNext)
                }
            }

            FlightBookingActions.OnAddContactDetail -> {
                viewModelScope.launch {
                    _uiEvent.send(BookingEvents.OnAddContact)
                }
            }

            is FlightBookingActions.ToggleLuggageSheet -> {
                _uiState.update { it.copy(showLuggageSheet = action.shouldOpen) }
            }

            is FlightBookingActions.AddLuggage -> {
                _uiState.update {
                    it.copy(addOnLuggage = action.selectedLuggage, luggagePrice = action.price)
                }
            }

            is FlightBookingActions.ToggleInsurance -> {
                if (action.added) {
                    _uiState.update { it.copy(insuranceAdded = true) }
                } else {
                    _uiState.update { it.copy(insuranceAdded = false) }
                }

            }

            FlightBookingActions.OnAddPassenger -> {
                viewModelScope.launch {
                    _uiEvent.send(BookingEvents.OnAddPassenger)
                }
            }
        }
    }
}

sealed interface BookingEvents {
    object OnNext : BookingEvents
    object OnBack : BookingEvents
    object OnAddContact : BookingEvents
    object OnAddPassenger : BookingEvents
    data class OnError(val message: String) : BookingEvents
}

data class FlightBookingState(
    val sameAsContactDetails: Boolean = true,
    val showLuggageSheet: Boolean = false,
    val insuranceAdded: Boolean = false,
    val luggagePrice: Int = 0,
    val addOnLuggage: Int = 0,
    val flightDetail: FlightDetail = FlightDetail(),
    val passengerDetail: PassengerDetail = PassengerDetail(),
    val contactDetail: ContactDetail = ContactDetail(),
    val passengerInfoDetail: ArrayList<ContactDetail> = arrayListOf()
)

sealed interface FlightBookingActions {
    object OnNext : FlightBookingActions
    object OnBack : FlightBookingActions
    object OnAddContactDetail : FlightBookingActions
    object OnAddPassenger : FlightBookingActions
    data class ToggleLuggageSheet(val shouldOpen: Boolean) : FlightBookingActions
    data class ToggleInsurance(val added: Boolean) : FlightBookingActions
    data class AddLuggage(val selectedLuggage: Int, val price: Int) : FlightBookingActions
}