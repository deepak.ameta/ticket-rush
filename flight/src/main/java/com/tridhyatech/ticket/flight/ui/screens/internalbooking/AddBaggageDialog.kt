package com.tridhyatech.ticket.flight.ui.screens.internalbooking

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.common.FlightCustomButton
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingActions
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor

@Composable
fun AddBaggageDialog(
    uiState: FlightBookingState,
    onAction: (FlightBookingActions) -> Unit,
    onSave: () -> Unit,
) {
    val firstSelected = uiState.addOnLuggage == 0
    val secondSelected = uiState.addOnLuggage == 1
    val thirdSelected = uiState.addOnLuggage == 2

    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(horizontal = 16.dp, vertical = 8.dp),
        verticalArrangement = Arrangement.spacedBy(12.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start
    ) {
        Text(
            text = "Add Baggage",
            style = TextStyle(
                fontSize = 16.sp,
                fontWeight = FontWeight(600),
                color = Color(0xFF000000),
            )
        )
        Text(
            text = "1. William Johnson",
            style = TextStyle(
                fontSize = 14.sp,
                fontWeight = FontWeight(600),
                color = Color(0xFF000000),
            )
        )

        Row(
            modifier = Modifier
                .fillMaxWidth(1f),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier
                    .clip(RoundedCornerShape(8.dp))
                    .border(1.dp, FlightBorderColor, RoundedCornerShape(8.dp))
                    .background(
                        if (firstSelected) MyBlueColor else Color.White,
                        RoundedCornerShape(8.dp)
                    )
                    .weight(1f)
                    .clickable { onAction(FlightBookingActions.AddLuggage(0, 0)) }
                    .padding(horizontal = 8.dp, vertical = 8.dp),
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(
                    text = "0 KG",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = if (firstSelected) Color.White else DarkBlueTextColor
                    ),
                    textAlign = TextAlign.Center,
                )
                Text(
                    text = "Free",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = if (firstSelected) Color.White else LightGrayTextColor
                    ),
                    textAlign = TextAlign.Center,
                )
            }
            Column(
                modifier = Modifier
                    .clip(RoundedCornerShape(8.dp))
                    .border(1.dp, FlightBorderColor, RoundedCornerShape(8.dp))
                    .background(
                        if (secondSelected) MyBlueColor else Color.White,
                        RoundedCornerShape(8.dp)
                    )
                    .weight(1f)
                    .clickable { onAction(FlightBookingActions.AddLuggage(1, 210)) }
                    .padding(horizontal = 8.dp, vertical = 8.dp),
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(
                    text = "5 KG",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = if (secondSelected) Color.White else DarkBlueTextColor
                    ),
                    textAlign = TextAlign.Center,
                )
                Text(
                    text = "INR 210",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = if (secondSelected) Color.White else LightGrayTextColor
                    ),
                    textAlign = TextAlign.Center,
                )
            }
            Column(
                modifier = Modifier
                    .clip(RoundedCornerShape(8.dp))
                    .border(1.dp, FlightBorderColor, RoundedCornerShape(8.dp))
                    .background(
                        if (thirdSelected) MyBlueColor else Color.White,
                        RoundedCornerShape(8.dp)
                    )
                    .weight(1f)
                    .clickable { onAction(FlightBookingActions.AddLuggage(2, 510)) }
                    .padding(horizontal = 8.dp, vertical = 8.dp),
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(
                    text = "10 KG",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = if (thirdSelected) Color.White else DarkBlueTextColor
                    ),
                    textAlign = TextAlign.Center,
                )
                Text(
                    text = "INR 510",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = if (thirdSelected) Color.White else LightGrayTextColor
                    ),
                    textAlign = TextAlign.Center,
                )
            }
        }

        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier,
            ) {
                Text(
                    text = "Total",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = LightGrayTextColor,
                    )
                )
                Text(
                    text = "INR ${uiState.luggagePrice}",
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight(500),
                        color = LightGrayTextColor,
                    )
                )
            }
            Box(
                modifier = Modifier,
                contentAlignment = Alignment.Center,
            ) {
                FlightCustomButton(
                    text = "Add Baggage",
                    onClick = onSave
                )
            }
        }

    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewBaggageDialog() {
//    AddBaggageDialog()
//}