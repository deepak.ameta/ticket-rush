package com.tridhyatech.ticket.flight.ui.screens.payment.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.common.FlightCustomEditText
import com.tridhyatech.ticket.flight.ui.screens.payment.CompletePaymentActions
import com.tridhyatech.ticket.flight.ui.screens.payment.CompletePaymentState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor

@Composable
fun AddDiscountSection(
    uiState: CompletePaymentState,
    onAction: (CompletePaymentActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.Start,
    ) {
        Text(
            text = "Add Discount",
            style = TextStyle(
                fontSize = 16.sp,
                fontWeight = FontWeight(600),
                color = DarkBlueTextColor,
                textAlign = TextAlign.Center,
            )
        )
        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            FlightCustomEditText(
                modifier = Modifier.weight(1f),
                value = uiState.couponValue,
                placeholder = "Add Coupon",
                onValueChange = { onAction(CompletePaymentActions.CouponUpdate(it)) }
            )
            Box(
                modifier = Modifier
                    .clip(RoundedCornerShape(8.dp))
                    .border(1.dp, FlightBorderColor)
                    .clickable { },
                contentAlignment = Alignment.Center
            ) {
                Row(
                    modifier = Modifier
                        .background(MyBlueColor)
                        .padding(8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        text = "Apply Coupon",
                        style = TextStyle(
                            fontSize = 14.sp,
                            fontWeight = FontWeight(600),
                            color = Color.White,
                            textAlign = TextAlign.Center,
                        )
                    )
                    Icon(
                        imageVector = Icons.Default.KeyboardArrowRight,
                        contentDescription = "apply coupon icon",
                        tint = Color.White
                    )
                }
            }
        }
    }
}