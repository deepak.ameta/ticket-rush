package com.tridhyatech.ticket.flight.ui.screens.internalbooking.passenger_info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.allPassengerContactDataStore
import com.tridhyatech.ticket.common.data.session.passengerData
import com.tridhyatech.ticket.common.domain.model.flight.ContactDetail
import com.tridhyatech.ticket.common.domain.model.flight.PassengerDetail
import com.tridhyatech.ticket.common.utils.fromJson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PassengerViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(PassengerState())
    val uiState: StateFlow<PassengerState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val passenger = session.getValue(passengerData, "")
                .fromJson<PassengerDetail>()

            val list = session.getValue(allPassengerContactDataStore, "")
                .fromJson<List<ContactDetail>>()

            if (passenger == null) return@launch
            if (list?.size!! > 0) {
                _uiState.update {
                    it.copy(
                        passengerCount = passenger.passengerCount,
                        passengerInfoList = ArrayList(list)
                    )
                }
            } else {
                val myList = arrayListOf<ContactDetail>()
                for (i in 0 until passenger.passengerCount) {
                    myList.add(ContactDetail())
                }
                _uiState.update {
                    it.copy(
                        passengerCount = passenger.passengerCount,
                        passengerInfoList = myList
                    )
                }
            }
        }
    }

    fun updateData(action: PassengerActions) {
        when (action) {
            PassengerActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            PassengerActions.OnSave -> {
                viewModelScope.launch {
                    session.setValue(
                        allPassengerContactDataStore,
                        Gson().toJson(uiState.value.passengerInfoList)
                    )
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            is PassengerActions.OnIdUpdate -> {
                val contact = uiState.value.passengerInfoList[action.index]
                val updatedList = uiState.value.passengerInfoList.toMutableList().apply {
                    set(action.index, contact.copy(id = action.text))
                }
                _uiState.value = _uiState.value.copy(passengerInfoList = ArrayList(updatedList))
            }

            is PassengerActions.OnNameUpdate -> {
                val contact = uiState.value.passengerInfoList[action.index]
                val updatedList = uiState.value.passengerInfoList.toMutableList().apply {
                    set(action.index, contact.copy(name = action.text))
                }
                _uiState.value = _uiState.value.copy(passengerInfoList = ArrayList(updatedList))
            }
        }
    }
}

data class PassengerState(
    val passengerCount: Int = 0,
    val passengerInfoList: ArrayList<ContactDetail> = arrayListOf()
)

sealed interface PassengerActions {
    object OnSave : PassengerActions
    object OnBack : PassengerActions
    data class OnNameUpdate(val text: String, val index: Int) : PassengerActions
    data class OnIdUpdate(val text: String, val index: Int) : PassengerActions
}