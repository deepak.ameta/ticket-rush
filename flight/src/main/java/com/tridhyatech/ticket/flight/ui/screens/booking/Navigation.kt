package com.tridhyatech.ticket.flight.ui.screens.booking

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.navigation.utils.FlightState

const val completeYourBookingRoute = "complete_your_booking_route"

fun NavController.navigateToCompleteYourBooking(navOptions: NavOptions? = null) {
    this.navigate(completeYourBookingRoute, navOptions)
}

fun NavGraphBuilder.completeYourBookingScreen(state: FlightState) {
    composable(route = completeYourBookingRoute) {
        FlightBookingRoute(state = state)
    }
}