package com.tridhyatech.ticket.flight.ui.screens.eticket.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyOrangeColor

@Composable
fun SeatNumberSection() {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .border(
                width = 1.dp,
                color = FlightBorderColor,
                shape = RoundedCornerShape(size = 16.dp)
            )
            .background(color = Color(0xFFFFFFFF), shape = RoundedCornerShape(size = 16.dp))
            .padding(start = 16.dp, top = 12.dp, end = 16.dp, bottom = 12.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.Start,
    ) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(
                modifier = Modifier
                    .border(
                        width = 1.dp,
                        color = FlightBorderColor,
                        shape = RoundedCornerShape(size = 5.dp)
                    )
                    .width(34.dp)
                    .height(17.dp)
                    .background(
                        color = Color(0xFFDDDDDD),
                        shape = RoundedCornerShape(size = 4.95833.dp)
                    )
            )
            Text(
                text = "Southwest Air",
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight(600),
                    color = DarkBlueTextColor,
                    textAlign = TextAlign.Center,
                )
            )
        }
        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = "WED, 26 OCT",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                )
            )
            Row(
                horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(12.dp),
                    painter = painterResource(id = R.drawable.icon_luggage),
                    contentDescription = "luggage icon",
                    contentScale = ContentScale.None
                )
                Text(
                    text = "20 Kg",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
                Spacer(modifier = Modifier.width(4.dp))
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(12.dp),
                    painter = painterResource(id = R.drawable.icon_food),
                    contentDescription = "food icon",
                    contentScale = ContentScale.None
                )
                Text(
                    text = "FOOD",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
                Spacer(modifier = Modifier.width(4.dp))
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(12.dp),
                    painter = painterResource(id = R.drawable.icon_wifi),
                    contentDescription = "wifi icon",
                    contentScale = ContentScale.None
                )
                Text(
                    text = "WIFI",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .border(
                    width = 1.dp,
                    color = FlightBorderColor,
                    shape = RoundedCornerShape(size = 16.dp)
                )
                .padding(8.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = "GTC",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    modifier = Modifier,
                    text = "14.00",
                    style = TextStyle(
                        fontSize = 18.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .weight(1f),
                horizontalArrangement = Arrangement.spacedBy(0.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .weight(1f)
                        .height(1.dp),
                    painter = painterResource(id = R.drawable.icon_dashline_destination),
                    contentDescription = "dashed line source to destination",
                    contentScale = ContentScale.None
                )
                Row(
                    modifier = Modifier
                        .border(
                            width = 1.dp,
                            color = FlightBorderColor,
                            shape = RoundedCornerShape(size = 28.dp)
                        )
                        .padding(start = 8.dp, top = 4.dp, end = 16.dp, bottom = 4.dp),
                    horizontalArrangement = Arrangement.spacedBy(
                        8.dp,
                        Alignment.CenterHorizontally
                    ),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        modifier = Modifier
                            .padding(1.dp)
                            .width(16.dp)
                            .height(16.dp),
                        painter = painterResource(id = R.drawable.icon_plane_search_screen),
                        contentDescription = "plane icon",
                        contentScale = ContentScale.None
                    )
                    Column(
                        verticalArrangement = Arrangement.spacedBy(2.dp, Alignment.Top),
                        horizontalAlignment = Alignment.Start,
                    ) {
                        Text(
                            text = "TRANSIT",
                            style = TextStyle(
                                fontSize = 10.sp,
                                fontWeight = FontWeight(500),
                                color = Color(0xFF808080),
                                textAlign = TextAlign.Center,
                            )
                        )
                        Text(
                            text = "1h 34m",
                            style = TextStyle(
                                fontSize = 12.sp,
                                fontWeight = FontWeight(600),
                                color = Color(0xFF1B1446),
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                }
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .weight(1f)
                        .height(1.dp),
                    painter = painterResource(id = R.drawable.icon_dashline_destination),
                    contentDescription = "dashed line source to destination",
                    contentScale = ContentScale.None
                )
            }
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.CenterVertically),
                horizontalAlignment = Alignment.End,
            ) {
                Text(
                    text = "KDC",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    text = "07.15",
                    style = TextStyle(
                        fontSize = 18.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(vertical = 8.dp),
            horizontalArrangement = Arrangement.spacedBy(12.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(
                modifier = Modifier
                    .size(32.dp)
                    .background(FlightBorderColor, CircleShape)
            )
            Column {
                Text(
                    text = "Bruce Wayne",
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight(500),
                        color = DarkBlueTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    text = "ID: 43********4356",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(400),
                        color = LightGrayTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
        Spacer(modifier = Modifier.height(2.dp))
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .border(
                    width = 1.dp,
                    color = FlightBorderColor,
                    shape = RoundedCornerShape(size = 16.dp)
                )
                .padding(12.dp),
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = "Gate",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = LightGrayTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    modifier = Modifier,
                    text = "B7",
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = "Flight Number",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = LightGrayTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    modifier = Modifier,
                    text = "ID-1234",
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = "Class",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = LightGrayTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    modifier = Modifier,
                    text = "Premium",
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = "Seat",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = LightGrayTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    modifier = Modifier,
                    text = "2A",
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
        Spacer(modifier = Modifier.height(2.dp))
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(vertical = 8.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = "ORDER NO.",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = LightGrayTextColor,
                    textAlign = TextAlign.Center,
                )
            )
            Row(
                modifier = Modifier,
                horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = "ABC983259450845",
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(500),
                        color = DarkBlueTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Icon(
                    modifier = Modifier.size(24.dp),
                    imageVector = Icons.Default.Check,
                    contentDescription = "copy order number icon",
                    tint = MyOrangeColor,
                )
            }
        }
        Spacer(modifier = Modifier.height(2.dp))
        Image(
            modifier = Modifier
                .fillMaxWidth(1f)
                .height(64.dp)
                .padding(horizontal = 16.dp),
            painter = painterResource(id = R.drawable.barcode),
            contentDescription = "",
            contentScale = ContentScale.FillWidth
        )
        Spacer(modifier = Modifier.height(2.dp))
    }
}