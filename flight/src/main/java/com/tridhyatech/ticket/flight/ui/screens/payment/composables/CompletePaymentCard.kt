package com.tridhyatech.ticket.flight.ui.screens.payment.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Notifications
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyOrangeColor

@Composable
fun CompletePaymentCard(
    source: String,
    destination: String,
    date: String,
    departureTime: String,
    totalTime: String,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .border(1.dp, FlightBorderColor, RoundedCornerShape(16.dp))
            .padding(12.dp, 8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Box(
            modifier = Modifier
                .width(60.dp)
                .height(50.dp)
                .clip(RoundedCornerShape(4.dp))
                .border(1.dp, FlightBorderColor, RoundedCornerShape(4.dp))
                .background(Color(0xFFDDDDDD))
        )
        Spacer(
            modifier = Modifier
                .height(50.dp)
                .width(1.dp)
                .background(LightGrayTextColor)
        )
        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(1f),
            verticalArrangement = Arrangement.spacedBy(2.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start,
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(0.dp),
                horizontalArrangement = Arrangement.spacedBy(2.dp, Alignment.Start),
                verticalAlignment = Alignment.Top,
            ) {
                Text(
                    text = source,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(600),
                        color = DarkBlueTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Image(
                    modifier = Modifier,
                    painter = painterResource(id = R.drawable.chevron_right_blue),
                    contentDescription = "forward arrow icon",
                )
                Text(
                    text = destination,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(600),
                        color = DarkBlueTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Row(
                horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Icon(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(16.dp),
                    imageVector = Icons.Default.DateRange,
                    contentDescription = "date icon",
                    tint = MyOrangeColor
                )
                Text(
                    text = date,
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = DarkBlueTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Spacer(modifier = Modifier.width(4.dp))
                Icon(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(16.dp),
                    imageVector = Icons.Default.CheckCircle,
                    contentDescription = "time icon",
                    tint = MyOrangeColor
                )
                Text(
                    text = departureTime,
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Row(
                horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Icon(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(16.dp),
                    imageVector = Icons.Default.Notifications,
                    contentDescription = "time icon",
                    tint = MyOrangeColor
                )
                Text(
                    text = totalTime,
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
    }
}