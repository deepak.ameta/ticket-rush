package com.tridhyatech.ticket.flight.ui.screens.transactiondetail

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.navigation.utils.FlightState

const val transactionDetailRoute = "transaction_detail_route"

fun NavController.navigateToTransactionDetail(navOptions: NavOptions? = null) {
    this.navigate(transactionDetailRoute, navOptions)
}

fun NavGraphBuilder.transactionDetailScreen(state: FlightState) {
    composable(route = transactionDetailRoute) {
        TransactionDetailRoute(state = state)
    }
}