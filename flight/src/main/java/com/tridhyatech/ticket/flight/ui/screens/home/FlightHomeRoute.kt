package com.tridhyatech.ticket.flight.ui.screens.home

import android.app.Activity
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.common.HeaderSection
import com.tridhyatech.ticket.flight.ui.screens.home.composables.FlightParamSection
import com.tridhyatech.ticket.flight.ui.screens.home.composables.RecentActivitySection
import com.tridhyatech.ticket.flight.ui.screens.home.composables.SearchFlightSection
import com.tridhyatech.ticket.flight.ui.theme.HomeScreenBgColor
import kotlinx.coroutines.flow.collectLatest

@Composable
fun FlightHomeRoute(
    state: FlightState,
    viewModel: FlightHomeViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()

    val context = LocalContext.current
    val activity = context as Activity

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> activity.finish()
                UiEvents.OnNext -> state.navigateToSearchResult()
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }


    FlightHomeScreen(
        uiState = uiState,
        onAction = viewModel::updateData,
    )
}

@Composable
fun FlightHomeScreen(
    uiState: FlightHomeState,
    onAction: (FlightHomeActions) -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxSize(1f)
            .background(color = HomeScreenBgColor)
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(185.dp)
                .background(color = Color(0xFFD9D9D9))
        )
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start,
        ) {
            HeaderSection(title = "Flights", onBackClick = { onAction(FlightHomeActions.OnBack) })
            Column(
                modifier = Modifier
                    .padding(horizontal = 8.dp)
                    .fillMaxWidth(1f)
                    .verticalScroll(rememberScrollState()),
                verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .border(
                            width = 1.dp,
                            color = Color(0x0D012276),
                            shape = RoundedCornerShape(size = 24.dp),
                        )
                        .background(
                            color = Color.White,
                            shape = RoundedCornerShape(size = 24.dp)
                        )
                        .padding(top = 0.dp, bottom = 24.dp),
                    verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
                    horizontalAlignment = Alignment.Start,
                ) {
                    SearchFlightSection(uiState = uiState, onAction = onAction)
                    FlightParamSection(uiState = uiState, onAction = onAction)
                }
                RecentActivitySection(uiState = uiState, onAction = onAction)
            }
        }
    }
}