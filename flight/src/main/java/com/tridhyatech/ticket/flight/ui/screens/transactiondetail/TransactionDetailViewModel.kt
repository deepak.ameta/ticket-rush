package com.tridhyatech.ticket.flight.ui.screens.transactiondetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.contactDataStore
import com.tridhyatech.ticket.common.data.session.flightBookingDataStore
import com.tridhyatech.ticket.common.data.session.flightDataStore
import com.tridhyatech.ticket.common.data.session.passengerData
import com.tridhyatech.ticket.common.data.session.transactionDataStore
import com.tridhyatech.ticket.common.domain.model.flight.BookingDetail
import com.tridhyatech.ticket.common.domain.model.flight.ContactDetail
import com.tridhyatech.ticket.common.domain.model.flight.FlightDetail
import com.tridhyatech.ticket.common.domain.model.flight.PassengerDetail
import com.tridhyatech.ticket.common.domain.model.flight.TransactionDetail
import com.tridhyatech.ticket.common.utils.fromJson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionDetailViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(TransactionDetailState())
    val uiState: StateFlow<TransactionDetailState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val flight = session.getValue(flightDataStore, "").fromJson<FlightDetail>()
            val passenger = session.getValue(passengerData, "").fromJson<PassengerDetail>()
            val booking = session.getValue(flightBookingDataStore, "").fromJson<BookingDetail>()
            val transaction = session.getValue(transactionDataStore, "").fromJson<TransactionDetail>()
            val contact = session.getValue(contactDataStore, "").fromJson<ContactDetail>()

            _uiState.update {
                it.copy(
                    transaction = transaction ?: TransactionDetail(),
                    flight = flight ?: FlightDetail(),
                    passenger = passenger ?: PassengerDetail(),
                    contact = contact ?: ContactDetail(),
                    booking = booking ?: BookingDetail(),
                )
            }
        }
    }

    fun updateData(action: TransactionDetailActions) {
        when (action) {
            TransactionDetailActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            TransactionDetailActions.OnNext -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            is TransactionDetailActions.ToggleRefundOrReschedule -> {
                _uiState.update { it.copy(showRefundDialog = action.show) }
            }
        }
    }
}

data class TransactionDetailState(
    val transaction: TransactionDetail = TransactionDetail(),
    val flight: FlightDetail = FlightDetail(),
    val passenger: PassengerDetail = PassengerDetail(),
    val booking: BookingDetail = BookingDetail(),
    val contact: ContactDetail = ContactDetail(),
    val showRefundDialog: Boolean = false,
)

sealed interface TransactionDetailActions {
    object OnNext : TransactionDetailActions
    object OnBack : TransactionDetailActions
    data class ToggleRefundOrReschedule(val show: Boolean) : TransactionDetailActions
}