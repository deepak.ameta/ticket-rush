package com.tridhyatech.ticket.flight.ui.screens.internalbooking.contact

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.common.FlightCustomButton
import com.tridhyatech.ticket.flight.ui.common.FlightCustomEditText
import com.tridhyatech.ticket.flight.ui.common.FlightSectionCommonHeader
import com.tridhyatech.ticket.flight.ui.common.GenderSelection
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import kotlinx.coroutines.flow.collectLatest

@Composable
fun ContactDetailRoute(
    state: FlightState,
    viewModel: ContactViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> state.popBackStack()
                UiEvents.OnNext -> state.popBackStack()
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    ContactDetailScreen(uiState, viewModel::updateData)
}

@Composable
fun ContactDetailScreen(
    uiState: ContactState,
    onAction: (ContactActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize(1f)
            .background(Color.White),
        verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start,
    ) {
        FlightSectionCommonHeader(
            title = "Contact Details",
            onBackClick = { onAction(ContactActions.OnBack) })
        Column(
            modifier = Modifier
                .weight(1f, true)
                .fillMaxWidth(1f)
                .background(Color.White)
                .padding(bottom = 16.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {

            FlightCustomEditText(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(horizontal = 16.dp),
                label = "Name",
                value = uiState.name,
                onValueChange = { onAction(ContactActions.OnNameUpdate(it)) }
            )
            GenderSelection(uiState, onAction)
            FlightCustomEditText(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(horizontal = 16.dp),
                label = "Email",
                value = uiState.email,
                onValueChange = { onAction(ContactActions.OnEmailUpdate(it)) }
            )
            FlightCustomEditText(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(horizontal = 16.dp),
                label = "Phone No",
                value = uiState.phone.toString(),
                onValueChange = { onAction(ContactActions.OnPhoneUpdate(it)) },
                keyBoardOptions = KeyboardOptions(
                    capitalization = KeyboardCapitalization.Words,
                    autoCorrect = false,
                    keyboardType = KeyboardType.Decimal,
                    imeAction = ImeAction.Next
                )
            )
            FlightCustomEditText(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(horizontal = 16.dp),
                label = "ID Number",
                value = uiState.id,
                onValueChange = { onAction(ContactActions.OnIdUpdate(it)) },
                keyBoardOptions = KeyboardOptions(
                    capitalization = KeyboardCapitalization.Words,
                    autoCorrect = false,
                    keyboardType = KeyboardType.Decimal,
                    imeAction = ImeAction.Done
                )
            )
            Text(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(horizontal = 16.dp),
                text = "This contact is for e-Ticket and Refund/Reschedule.",
                style = TextStyle(
                    fontSize = 12.sp,
                    fontWeight = FontWeight(400),
                    color = LightGrayTextColor,
                    textAlign = TextAlign.Start,
                )
            )
        }
        FlightCustomButton(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 16.dp, vertical = 16.dp),
            text = "Save Changes",
        ) { onAction(ContactActions.OnSave) }
    }
}


//@Preview
//@Composable
//fun PreviewContactDetailPage() {
//    ContactDetailPage()
//}