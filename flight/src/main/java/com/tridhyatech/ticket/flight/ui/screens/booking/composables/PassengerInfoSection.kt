package com.tridhyatech.ticket.flight.ui.screens.booking.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingActions
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor

@Composable
fun PassengerInfoSection(
    uiState: FlightBookingState,
    onAction: (FlightBookingActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.Start,
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = "Passenger Info",
                style = TextStyle(
                    fontSize = 16.sp,
                    fontWeight = FontWeight(600),
                    color = DarkBlueTextColor,
                    textAlign = TextAlign.Center,
                )
            )
            Icon(
                modifier = Modifier
                    .size(24.dp)
                    .clickable { onAction(FlightBookingActions.OnAddPassenger) }
                    .padding(end = 4.dp),
                imageVector = if (
                    uiState.passengerInfoDetail.size != uiState.passengerDetail.passengerCount) {
                    Icons.Default.Add
                } else {
                    Icons.Default.Edit
                },
                contentDescription = "Add/Edit icon",
                tint = Color.DarkGray,
            )
        }

        AnimatedVisibility(visible = uiState.passengerInfoDetail.size > 0) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(16.dp))
                .padding(bottom = 4.dp)
                .border(1.dp, FlightBorderColor, RoundedCornerShape(16.dp))
                .background(Color.White)
                .padding(16.dp, 12.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
            horizontalAlignment = Alignment.Start,
        ) {
                repeat(uiState.passengerInfoDetail.size) {
                    Text(
                        modifier = Modifier.fillMaxWidth(1f),
                        text = uiState.passengerInfoDetail[it].name,
                        style = TextStyle(
                            fontSize = 14.sp,
                            fontWeight = FontWeight(500),
                            color = DarkBlueTextColor,
                            textAlign = TextAlign.Start,
                        )
                    )
                }
            }
        }
    }
}