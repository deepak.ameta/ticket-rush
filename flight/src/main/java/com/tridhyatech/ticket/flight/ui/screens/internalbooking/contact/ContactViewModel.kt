package com.tridhyatech.ticket.flight.ui.screens.internalbooking.contact

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.contactDataStore
import com.tridhyatech.ticket.common.domain.model.flight.ContactDetail
import com.tridhyatech.ticket.common.utils.isDigit
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ContactViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(ContactState())
    val uiState: StateFlow<ContactState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val jsonContactData = session.getValue(contactDataStore, "")
            val contact = Gson().fromJson(jsonContactData, ContactDetail::class.java)
            _uiState.value = contact.toContactState()
        }
    }

    fun updateData(action: ContactActions) {
        when (action) {
            ContactActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            ContactActions.OnSave -> {
                viewModelScope.launch {
                    session.setValue(
                        contactDataStore,
                        Gson().toJson(uiState.value.toContactDetail())
                    )
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            is ContactActions.OnPhoneUpdate -> {
                if (action.text.isDigit() && action.text != "") _uiState.update { it.copy(phone = action.text.toInt()) }
                else {
                    viewModelScope.launch {
                        _uiEvent.send(UiEvents.OnError("Only Digits are allowed"))
                    }
                }
            }

            is ContactActions.OnEmailUpdate -> {
                _uiState.update { it.copy(email = action.text) }
            }

            is ContactActions.OnIdUpdate -> {
                _uiState.update { it.copy(id = action.text) }
            }

            is ContactActions.OnNameUpdate -> {
                _uiState.update { it.copy(name = action.text) }
            }

            is ContactActions.OnGenderUpdate -> {
                _uiState.update { it.copy(gender = action.text) }
            }
        }
    }
}

data class ContactState(
    val name: String = "",
    val email: String = "",
    val gender: String = "",
    val phone: Int = 0,
    val id: String = "",
)

fun ContactDetail.toContactState(): ContactState {
    return ContactState(
        name = this.name,
        email = this.email,
        gender = this.gender,
        phone = this.mobileNo,
        id = this.id
    )
}

fun ContactState.toContactDetail(): ContactDetail {
    return ContactDetail(
        name = this.name,
        mobileNo = this.phone,
        email = this.email,
        id = this.id,
        gender = this.gender
    )
}


sealed interface ContactActions {
    object OnSave : ContactActions
    object OnBack : ContactActions
    data class OnNameUpdate(val text: String) : ContactActions
    data class OnEmailUpdate(val text: String) : ContactActions
    data class OnPhoneUpdate(val text: String) : ContactActions
    data class OnIdUpdate(val text: String) : ContactActions
    data class OnGenderUpdate(val text: String) : ContactActions
}