package com.tridhyatech.ticket.flight.ui.screens.bookingsuccessful

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.navigation.utils.FlightState

const val paymentSuccessfulRoute = "payment_successful_route"

fun NavController.navigateToPaymentSuccessful(navOptions: NavOptions? = null) {
    this.navigate(paymentSuccessfulRoute, navOptions)
}

fun NavGraphBuilder.paymentSuccessfulScreen(state: FlightState) {
    composable(route = paymentSuccessfulRoute) {
        BookingSuccessfulRoute(state = state)
    }
}