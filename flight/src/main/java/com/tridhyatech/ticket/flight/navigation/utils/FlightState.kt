package com.tridhyatech.ticket.flight.navigation.utils

import android.app.Activity
import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.tridhyatech.ticket.flight.ui.screens.booking.navigateToCompleteYourBooking
import com.tridhyatech.ticket.flight.ui.screens.bookingsuccessful.navigateToPaymentSuccessful
import com.tridhyatech.ticket.flight.ui.screens.eticket.navigateToETicket
import com.tridhyatech.ticket.flight.ui.screens.internalbooking.contact.navigateToContactDetail
import com.tridhyatech.ticket.flight.ui.screens.internalbooking.passenger_info.navigateToPassengerScreen
import com.tridhyatech.ticket.flight.ui.screens.payment.navigateToCompletePayment
import com.tridhyatech.ticket.flight.ui.screens.search.navigateToSearchResult
import com.tridhyatech.ticket.flight.ui.screens.transactiondetail.navigateToTransactionDetail

@Composable
fun rememberFlightState(flightNavController: NavHostController = rememberNavController()): FlightState {
    return remember(flightNavController) {
        FlightState(flightNavController)
    }
}

@Stable
class FlightState(val flightNavController: NavController) {

    fun popBackStack() {
        flightNavController.popBackStack()
    }

    fun finishFlightActivity(context: Context) {
        (context as Activity).finish()
    }

    fun navigateToSearchResult() {
        flightNavController.navigateToSearchResult()
    }

    fun navigateToCompleteBooking() {
        flightNavController.navigateToCompleteYourBooking()
    }

    fun navigateToCompletePayment() {
        flightNavController.navigateToCompletePayment()
    }

    fun navigateToPaymentSuccessful() {
        flightNavController.navigateToPaymentSuccessful()
    }

    fun navigateToTransactionDetail() {
        flightNavController.navigateToTransactionDetail()
    }

    fun navigateToETicket() {
        flightNavController.navigateToETicket()
    }

    /**
     * These screens are internal booking screens
     */
    fun navigateToAddEditContactDetails() {
        flightNavController.navigateToContactDetail()
    }

    fun navigateToAddPassengerInfo() {
        flightNavController.navigateToPassengerScreen()
    }
}