package com.tridhyatech.ticket.flight.ui.screens.payment.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.common.FlightCustomButton
import com.tridhyatech.ticket.flight.ui.screens.payment.CompletePaymentActions
import com.tridhyatech.ticket.flight.ui.screens.payment.CompletePaymentState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor

@Composable
fun PaymentBottomSection(
    uiState: CompletePaymentState,
    onAction: (CompletePaymentActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .background(Color.White)
            .padding(8.dp, 8.dp, 8.dp, 16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start,
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start
            ) {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(0.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        text = "SUBTOTAL",
                        style = TextStyle(
                            fontSize = 8.sp,
                            fontWeight = FontWeight(600),
                            color = LightGrayTextColor,
                            textAlign = TextAlign.Center,
                        )
                    )
                    Icon(
                        modifier = Modifier.size(20.dp),
                        imageVector = Icons.Default.KeyboardArrowDown,
                        contentDescription = "check icon",
                        tint = MyBlueColor
                    )
                }
                Text(
                    text = uiState.bookingDetail.finalPrice.toString(),
                    style = TextStyle(
                        fontSize = 18.sp,
                        fontWeight = FontWeight(600),
                        color = DarkBlueTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Text(
                text = "You will get 1250 coin",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(600),
                    color = DarkBlueTextColor,
                    textAlign = TextAlign.Start,
                ),
            )
        }
        FlightCustomButton(
            modifier = Modifier.fillMaxWidth(1f),
            text = "Pay Now",
            onClick = { onAction(CompletePaymentActions.OnNext) },
        )
    }
}