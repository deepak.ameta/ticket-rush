package com.tridhyatech.ticket.flight.ui.screens.booking.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor

@Composable
fun FlightDetailCard(uiState: FlightBookingState) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .clip(RoundedCornerShape(16.dp))
            .border(1.dp, FlightBorderColor, RoundedCornerShape(16.dp))
            .background(Color.White)
            .padding(horizontal = 16.dp, vertical = 12.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.Start,
    ) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .weight(1f, true),
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Box(
                    modifier = Modifier
                        .border(
                            width = 1.dp,
                            color = FlightBorderColor,
                            shape = RoundedCornerShape(size = 5.dp)
                        )
                        .width(34.dp)
                        .height(17.dp)
                        .background(
                            color = Color(0xFFDDDDDD),
                            shape = RoundedCornerShape(size = 4.95833.dp)
                        )
                )
                Text(
                    text = uiState.flightDetail.companyName,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(600),
                        color = DarkBlueTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Box(
                modifier = Modifier
                    .background(Color(0xFFF7EED2), RoundedCornerShape(4.dp))
                    .padding(4.dp, 2.dp, 4.dp, 2.dp),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = "Departure",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFFECA02F),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = uiState.passengerDetail.date,
                style = TextStyle(
                    fontSize = 12.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                )
            )
            Row(
                horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(12.dp),
                    painter = painterResource(id = R.drawable.icon_luggage),
                    contentDescription = "luggage icon",
                    contentScale = ContentScale.None
                )
                Text(
                    text = "20 Kg",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
                Spacer(modifier = Modifier.width(4.dp))
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(12.dp),
                    painter = painterResource(id = R.drawable.icon_food),
                    contentDescription = "food icon",
                    contentScale = ContentScale.None
                )
                Text(
                    text = "Food",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
                Spacer(modifier = Modifier.width(4.dp))
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(12.dp),
                    painter = painterResource(id = R.drawable.icon_wifi),
                    contentDescription = "wifi icon",
                    contentScale = ContentScale.None
                )
                Text(
                    text = "Wifi",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .border(
                    width = 1.dp,
                    color = FlightBorderColor,
                    shape = RoundedCornerShape(size = 16.dp)
                )
                .padding(8.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = uiState.flightDetail.source,
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    modifier = Modifier,
                    text = uiState.flightDetail.departureTime,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .weight(1f),
                horizontalArrangement = Arrangement.spacedBy(0.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .weight(1f)
                        .height(1.dp),
                    painter = painterResource(id = R.drawable.icon_dashline_destination),
                    contentDescription = "dashed line source to destination",
                    contentScale = ContentScale.None
                )
                Row(
                    modifier = Modifier
                        .border(
                            width = 1.dp,
                            color = FlightBorderColor,
                            shape = RoundedCornerShape(size = 28.dp)
                        )
                        .padding(start = 8.dp, top = 4.dp, end = 16.dp, bottom = 4.dp),
                    horizontalArrangement = Arrangement.spacedBy(
                        8.dp,
                        Alignment.CenterHorizontally
                    ),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        modifier = Modifier
                            .padding(1.dp)
                            .width(16.dp)
                            .height(16.dp),
                        painter = painterResource(id = R.drawable.icon_plane_search_screen),
                        contentDescription = "plane icon",
                        contentScale = ContentScale.None
                    )
                    Column(
                        verticalArrangement = Arrangement.spacedBy(2.dp, Alignment.Top),
                        horizontalAlignment = Alignment.Start,
                    ) {
                        Text(
                            text = "TRANSIT",
                            style = TextStyle(
                                fontSize = 10.sp,
                                fontWeight = FontWeight(500),
                                color = Color(0xFF808080),
                                textAlign = TextAlign.Center,
                            )
                        )
                        Text(
                            text = uiState.flightDetail.totalTime,
                            style = TextStyle(
                                fontSize = 12.sp,
                                fontWeight = FontWeight(600),
                                color = Color(0xFF1B1446),
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                }
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .weight(1f)
                        .height(1.dp),
                    painter = painterResource(id = R.drawable.icon_dashline_destination),
                    contentDescription = "dashed line source to destination",
                    contentScale = ContentScale.None
                )
            }
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.CenterVertically),
                horizontalAlignment = Alignment.End,
            ) {
                Text(
                    text = uiState.flightDetail.destination,
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    text = uiState.flightDetail.arrivalTime,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .size(12.dp),
                painter = painterResource(id = R.drawable.icon_food),
                contentDescription = "food icon",
                contentScale = ContentScale.None
            )
            Text(
                text = "Refundable",
                style = TextStyle(
                    fontSize = 10.sp,
                    fontWeight = FontWeight(600),
                    color = Color(0xFF1B1446),
                    textAlign = TextAlign.Center,
                )
            )
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .size(12.dp),
                painter = painterResource(id = R.drawable.icon_food),
                contentDescription = "food icon",
                contentScale = ContentScale.None
            )
            Text(
                text = "Reschedule",
                style = TextStyle(
                    fontSize = 10.sp,
                    fontWeight = FontWeight(600),
                    color = Color(0xFF1B1446),
                    textAlign = TextAlign.Center,
                )
            )
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewFlightDetailCard() {
//    Column(Modifier.padding(8.dp)) {
//        FlightDetailCard()
//    }
//}