package com.tridhyatech.ticket.flight.ui.common

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.selectable
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.screens.internalbooking.contact.ContactActions
import com.tridhyatech.ticket.flight.ui.screens.internalbooking.contact.ContactState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor

@Composable
fun GenderSelection(
    uiState: ContactState,
    onAction: (ContactActions) -> Unit,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(horizontal = 16.dp),
        horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = "Gender",
            style = TextStyle(
                fontSize = 14.sp,
                fontWeight = FontWeight(400),
                color = DarkBlueTextColor,
                textAlign = TextAlign.Start,
            ),
        )
        Spacer(modifier = Modifier.weight(1f))
        RadioButton(
            selected = uiState.gender == "Male",
            onClick = { onAction(ContactActions.OnGenderUpdate("Male")) },
            modifier = Modifier.selectable(
                selected = uiState.gender == "Male",
                onClick = { onAction(ContactActions.OnGenderUpdate("Male")) }
            ),
        )
        Text("Male")

        RadioButton(
            selected = uiState.gender == "Female",
            onClick = { onAction(ContactActions.OnGenderUpdate("Female")) },
            modifier = Modifier.selectable(
                selected = uiState.gender == "Female",
                onClick = { onAction(ContactActions.OnGenderUpdate("Female")) }
            ),
        )
        Text("Female")
    }
}