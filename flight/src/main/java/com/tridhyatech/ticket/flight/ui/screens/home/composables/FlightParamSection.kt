package com.tridhyatech.ticket.flight.ui.screens.home.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.TabRowDefaults
import androidx.compose.material3.TabRowDefaults.tabIndicatorOffset
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize
import com.tridhyatech.ticket.common.data.session.passengerCount
import com.tridhyatech.ticket.common.utils.rememberPreference
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.ui.screens.home.FlightHomeActions
import com.tridhyatech.ticket.flight.ui.screens.home.FlightHomeState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.HomeScreenBgColor
import com.tridhyatech.ticket.flight.utils.OpenDatePicker
import kotlinx.coroutines.launch

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun FlightParamSection(
    uiState: FlightHomeState,
    onAction: (FlightHomeActions) -> Unit
) {
    val pagerState = rememberPagerState { 2 }
    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(horizontal = 16.dp)
    ) {
        Tabs(pagerState = pagerState)
        TabsContent(
            pagerState = pagerState, uiState = uiState,
            onAction = onAction
        )
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun Tabs(pagerState: PagerState) {
    val list = listOf("One Way", "Round Trip")
    val scope = rememberCoroutineScope()

    TabRow(
        modifier = Modifier.height(65.dp),
        selectedTabIndex = pagerState.currentPage,
        containerColor = Color.White,
        contentColor = Color(0xFFFFFFFF),
        indicator = {
            TabRowDefaults.Indicator(
                modifier = Modifier.tabIndicatorOffset(it[pagerState.currentPage]),
                height = 2.dp,
                color = Color(0xFF0768FD)
            )
        }
    ) {
        list.forEachIndexed { index, _ ->
            Tab(
                text = {
                    Text(
                        list[index],
                        style = TextStyle(
                            fontSize = 14.sp,
                            fontWeight = FontWeight(600),
                            color = if (pagerState.currentPage == index) Color(0xFF1B1446) else Color.Black,
                            textAlign = TextAlign.Center,
                        )
                    )
                },
                selected = pagerState.currentPage == index,
                onClick = { scope.launch { pagerState.animateScrollToPage(index) } }
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TabsContent(
    pagerState: PagerState,
    uiState: FlightHomeState,
    onAction: (FlightHomeActions) -> Unit
) {
    HorizontalPager(state = pagerState) { page ->
        when (page) {
            0 -> OneWaySection(
                uiState = uiState,
                onAction = onAction
            )

            1 -> OneWaySection(
                uiState = uiState,
                onAction = onAction
            )
        }
    }
}

@Composable
fun OneWaySection(
    uiState: FlightHomeState,
    onAction: (FlightHomeActions) -> Unit
) {
    var totalPassengers by rememberPreference(passengerCount, 0)
    var rowSize by remember { mutableStateOf(Size.Zero) }
    var expanded by remember { mutableStateOf(false) }

    val context = LocalContext.current
    val picker = OpenDatePicker(context) {
        onAction(FlightHomeActions.UpdateJourneyDate(it))
    }

    Column(
        modifier = Modifier.fillMaxWidth(1f),
        verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top)
    ) {
        Row(
            modifier = Modifier
                .padding(top = 16.dp)
                .fillMaxWidth(1f)
                .background(color = HomeScreenBgColor, shape = RoundedCornerShape(size = 16.dp))
                .padding(start = 12.dp, top = 8.dp, end = 16.dp, bottom = 8.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .size(24.dp),
                painter = painterResource(id = R.drawable.date_range),
                contentDescription = "date range icon",
                contentScale = ContentScale.None
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .weight(1f),
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = "Journey Date",
                    style = TextStyle(
                        fontSize = 10.sp,
                        lineHeight = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        letterSpacing = 0.8.sp,
                    )
                )
                Text(
                    text = uiState.journeyDate,
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                    )
                )
            }

            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .size(24.dp)
                    .clip(CircleShape)
                    .clickable {
                        picker.showDatePickerDialog()
                    },
                painter = painterResource(id = R.drawable.edit_calendar),
                contentDescription = "edit calendar icon",
                contentScale = ContentScale.None
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .background(color = HomeScreenBgColor, shape = RoundedCornerShape(size = 16.dp))
                .padding(start = 12.dp, top = 8.dp, end = 16.dp, bottom = 8.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .size(24.dp),
                painter = painterResource(id = R.drawable.person_icon),
                contentDescription = "passenger icon",
                contentScale = ContentScale.None
            )
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth(1f),
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = "Passenger",
                    style = TextStyle(
                        fontSize = 10.sp,
                        lineHeight = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        letterSpacing = 0.8.sp,
                    )
                )
                Text(
                    text = totalPassengers.toString(),
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                    )
                )
            }
            Row(
                horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.CenterHorizontally),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(20.dp)
                        .clip(CircleShape)
                        .clickable { if (totalPassengers > 0) totalPassengers -= 1 },
                    painter = painterResource(id = R.drawable.remove_passenger_icon),
                    contentDescription = "remove passenger icon",
                    contentScale = ContentScale.None
                )
                Text(
                    modifier = Modifier,
                    text = totalPassengers.toString(),
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(20.dp)
                        .clip(CircleShape)
                        .clickable { totalPassengers += 1 },
                    painter = painterResource(id = R.drawable.add_passenger_icon),
                    contentDescription = "add passenger icon",
                    contentScale = ContentScale.None
                )
            }
        }

        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .clip(RoundedCornerShape(16.dp))
                .background(HomeScreenBgColor)
                .onGloballyPositioned {
                    rowSize = it.size.toSize()
                },
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .clickable { expanded = !expanded }
                    .padding(horizontal = 16.dp, vertical = 8.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .size(24.dp),
                    painter = painterResource(id = R.drawable.airplane_ticket_type_icon),
                    contentDescription = "flight class type icon",
                    contentScale = ContentScale.None
                )
                Column(
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxWidth(1f),
                    verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                    horizontalAlignment = Alignment.Start,
                ) {
                    Text(
                        text = "Type",
                        style = TextStyle(
                            fontSize = 10.sp,
                            lineHeight = 14.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFF808080),
                            letterSpacing = 0.8.sp,
                        )
                    )
                    Text(
                        text = uiState.seatType,
                        style = TextStyle(
                            fontSize = 14.sp,
                            fontWeight = FontWeight(500),
                            color = DarkBlueTextColor,
                        )
                    )
                }
                Image(
                    modifier = Modifier
                        .width(20.dp)
                        .height(20.dp),
                    painter = painterResource(id = R.drawable.arrow_drop_down_type_icon),
                    contentDescription = "arrow down icon",
                    contentScale = ContentScale.None
                )
            }

            AnimatedVisibility(visible = expanded) {
                val list = listOf("Economy", "Premium Economy", "First Class", "Business Class")
                Card(
                    modifier = Modifier
                        .padding(horizontal = 4.dp, vertical = 4.dp)
                        .width(rowSize.width.dp),
                    shape = RoundedCornerShape(16.dp)
                ) {
                    repeat(list.size) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .clickable {
                                    onAction(FlightHomeActions.UpdateFlightClass(list[it]))
                                    expanded = false
                                }
                                .padding(horizontal = 16.dp, vertical = 12.dp)
                        ) {
                            Text(
                                text = list[it],
                                style = TextStyle(
                                    fontSize = 14.sp,
                                    fontWeight = FontWeight(500),
                                    color = DarkBlueTextColor,
                                ),
                            )
                        }
                    }
                }
            }
        }
        FindFlightCustomButton(onFindClick = { onAction(FlightHomeActions.OnNext) })
    }
}