package com.tridhyatech.ticket.flight.ui.screens.payment

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.common.FlightSectionCommonHeader
import com.tridhyatech.ticket.flight.ui.screens.booking.composables.ProcessStepSection
import com.tridhyatech.ticket.flight.ui.screens.payment.composables.AddDiscountSection
import com.tridhyatech.ticket.flight.ui.screens.payment.composables.CompletePaymentCard
import com.tridhyatech.ticket.flight.ui.screens.payment.composables.PaymentBottomSection
import com.tridhyatech.ticket.flight.ui.screens.payment.composables.PaymentMethodSection
import kotlinx.coroutines.flow.collectLatest

@Composable
fun CompletePaymentRoute(
    state: FlightState,
    viewModel: CompletePaymentViewModel = hiltViewModel()
) {

    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> state.popBackStack()
                UiEvents.OnNext -> state.navigateToPaymentSuccessful()
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    CompletePaymentPage(uiState, viewModel::updateData)
}

@Composable
fun CompletePaymentPage(
    uiState: CompletePaymentState,
    onAction: (CompletePaymentActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize(1f)
            .background(Color.White),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .background(Color.White)
                .padding(bottom = 16.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            FlightSectionCommonHeader(title = "Complete Your Payment", onBackClick = {
                onAction(CompletePaymentActions.OnBack)
            })
            ProcessStepSection(onBookingPage = true, onPaymentPage = true)
        }
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .weight(1f, true)
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            CompletePaymentCard(
                uiState.flightDetail.source,
                uiState.flightDetail.destination,
                uiState.passengerDetail.date,
                "${uiState.flightDetail.departureTime} UTC",
                uiState.flightDetail.totalTime
            )
            PaymentMethodSection(uiState, onAction)
            AddDiscountSection(uiState, onAction)
        }
        PaymentBottomSection(uiState, onAction)
    }
}