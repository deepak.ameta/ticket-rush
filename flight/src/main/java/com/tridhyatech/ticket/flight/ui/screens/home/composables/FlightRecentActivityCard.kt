package com.tridhyatech.ticket.flight.ui.screens.home.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.common.domain.model.flight.PassengerDetail
import com.tridhyatech.ticket.flight.R
import java.util.Locale

@Composable
fun FlightRecentActivityCard(
    flightData: PassengerDetail,
    onItemClick: (PassengerDetail) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .clip(RoundedCornerShape(16.dp))
            .border(
                width = 1.dp,
                color = Color(0x0D012276),
                shape = RoundedCornerShape(size = 16.dp)
            )
            .background(Color.White)
            .clickable { onItemClick(flightData) }
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start,
    ) {
        Text(
            text = "WEDNESDAY ${flightData.date}",
            style = TextStyle(
                fontSize = 10.sp,
                lineHeight = 14.sp,
                fontWeight = FontWeight(500),
                color = Color(0xFF808080),
                letterSpacing = 0.8.sp,
            )
        )
        Spacer(
            modifier = Modifier
                .fillMaxWidth(1f)
                .height(2.dp)
                .padding(horizontal = 16.dp)
                .background(Color(0x0D012276))
        )
        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier.weight(1f),
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.CenterVertically),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = flightData.from,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                    )
                )
                Text(
                    text = "(USA)",
                    style = TextStyle(
                        fontSize = 10.sp,
                        lineHeight = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        letterSpacing = 0.8.sp,
                    )
                )
            }
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .width(24.dp)
                    .height(24.dp),
                painter = painterResource(id = R.drawable.chevron_right_blue),
                contentDescription = "forward arrow icon",
                contentScale = ContentScale.None
            )
            Column(
                modifier = Modifier.weight(1f),
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.CenterVertically),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = flightData.to,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF1B1446),
                    )
                )
                Text(
                    text = "(USA)",
                    style = TextStyle(
                        fontSize = 10.sp,
                        lineHeight = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        letterSpacing = 0.8.sp,
                    )
                )
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .height(16.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .width(16.dp)
                    .height(16.dp),
                painter = painterResource(id = R.drawable.person_small_icon),
                contentDescription = "recent person small icon",
                contentScale = ContentScale.None
            )
            Text(
                text = "${flightData.passengerCount} PERSON",
                style = TextStyle(
                    fontSize = 10.sp,
                    lineHeight = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                    letterSpacing = 0.8.sp,
                )
            )
            Image(
                modifier = Modifier
                    .padding(1.dp),
                painter = painterResource(id = R.drawable.rating_small_icon),
                contentDescription = "recent rating small icon",
                contentScale = ContentScale.None
            )
            Text(
                text = flightData.seatType.uppercase(Locale.US),
                style = TextStyle(
                    fontSize = 10.sp,
                    lineHeight = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                    letterSpacing = 0.8.sp,
                ),
            )
        }
    }
}