package com.tridhyatech.ticket.flight.ui.screens.internalbooking.passenger_info

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.common.FlightCustomButton
import com.tridhyatech.ticket.flight.ui.common.FlightCustomEditText
import com.tridhyatech.ticket.flight.ui.common.FlightSectionCommonHeader
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import kotlinx.coroutines.flow.collectLatest

@Composable
fun PassengerRoute(
    state: FlightState,
    viewModel: PassengerViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> state.popBackStack()
                UiEvents.OnNext -> state.popBackStack()
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    PassengerInfoScreen(uiState, viewModel::updateData)
}

@Composable
fun PassengerInfoScreen(
    uiState: PassengerState,
    onAction: (PassengerActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize(1f)
            .background(Color.White),
        verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start,
    ) {
        FlightSectionCommonHeader(title = "Passenger Info", onBackClick = {
            onAction(PassengerActions.OnBack)
        })

        LazyColumn(
            modifier = Modifier
                .weight(1f, true)
                .fillMaxWidth(1f)
                .background(Color.White)
                .padding(bottom = 16.dp),
            verticalArrangement = Arrangement.spacedBy(18.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            items(uiState.passengerInfoList.size) {
                val data = uiState.passengerInfoList[it]
                Column(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .clip(RoundedCornerShape(16.dp))
                        .border(1.dp, FlightBorderColor, RoundedCornerShape(16.dp))
                        .padding(8.dp),
                ) {
                    FlightCustomEditText(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(horizontal = 16.dp),
                        label = "Passenger ${it + 1} Name",
                        value = data.name,
                        onValueChange = { name ->
                            onAction(PassengerActions.OnNameUpdate(name, it))
                        }
                    )
                    FlightCustomEditText(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(horizontal = 16.dp),
                        label = "Passenger ${it + 1} ID",
                        value = data.id,
                        onValueChange = { id -> onAction(PassengerActions.OnIdUpdate(id, it)) },
                        keyBoardOptions = KeyboardOptions(
                            capitalization = KeyboardCapitalization.Words,
                            autoCorrect = false,
                            keyboardType = KeyboardType.Decimal,
                            imeAction = ImeAction.Done
                        )
                    )
                }
            }
        }
        FlightCustomButton(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 16.dp, vertical = 16.dp),
            text = "Save Changes",
        ) { onAction(PassengerActions.OnSave) }
    }
}

//@Preview
//@Composable
//fun PreviewPassengerInfoPage() {
//    PassengerInfoPage()
//}