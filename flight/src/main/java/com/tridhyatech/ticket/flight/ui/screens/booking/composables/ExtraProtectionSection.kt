package com.tridhyatech.ticket.flight.ui.screens.booking.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingActions
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor
import com.tridhyatech.ticket.flight.ui.theme.MyOrangeColor

@Composable
fun ExtraProtectionSection(
    uiState: FlightBookingState,
    onAction: (FlightBookingActions) -> Unit,
) {
    val added = uiState.insuranceAdded

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.Start,
    ) {
        Text(
            text = "Extra Protection",
            style = TextStyle(
                fontSize = 16.sp,
                fontWeight = FontWeight(600),
                color = DarkBlueTextColor,
                textAlign = TextAlign.Center,
            )
        )
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .border(
                    width = 1.dp,
                    color = FlightBorderColor,
                    shape = RoundedCornerShape(size = 16.dp)
                )
                .background(color = Color(0xFFFFFFFF), shape = RoundedCornerShape(size = 16.dp))
                .padding(horizontal = 16.dp, vertical = 12.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically),
            horizontalAlignment = Alignment.Start,
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(1f),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Row(
                    modifier = Modifier,
                    horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        modifier = Modifier.size(24.dp),
                        painter = painterResource(id = R.drawable.person_small_icon),
                        contentDescription = "baggage icon"
                    )
                    Text(
                        text = "Travel Insurance",
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(600),
                            color = DarkBlueTextColor,
                            textAlign = TextAlign.Center,
                        )
                    )
                }
                Text(
                    text = " INR 12",
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight(600),
                        color = DarkBlueTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Spacer(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .height(1.dp)
                    .padding(horizontal = 16.dp)
                    .background(FlightBorderColor)
            )
            Column(
                modifier = Modifier.fillMaxWidth(1f),
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start
            ) {
                Row(
                    modifier = Modifier,
                    horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(
                        modifier = Modifier.size(20.dp),
                        imageVector = Icons.Default.Check,
                        contentDescription = "check icon",
                        tint = MyOrangeColor
                    )
                    Text(
                        text = "Coverage for Accidents up to $10000",
                        style = TextStyle(
                            fontSize = 12.sp,
                            fontWeight = FontWeight(600),
                            color = LightGrayTextColor,
                            textAlign = TextAlign.Start,
                        )
                    )
                }
                Row(
                    modifier = Modifier,
                    horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(
                        modifier = Modifier.size(20.dp),
                        imageVector = Icons.Default.Check,
                        contentDescription = "check icon",
                        tint = MyOrangeColor
                    )
                    Text(
                        text = "Coverage for trip cancellation by passengers up to $1250",
                        style = TextStyle(
                            fontSize = 12.sp,
                            fontWeight = FontWeight(600),
                            color = LightGrayTextColor,
                            textAlign = TextAlign.Start,
                        )
                    )
                }
            }
            Box(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .clip(RoundedCornerShape(12.dp))
                    .border(1.dp, FlightBorderColor, RoundedCornerShape(12.dp))
                    .background(if (added) MyBlueColor else Color.White)
                    .clickable { onAction(FlightBookingActions.ToggleInsurance(!added)) }
                    .padding(8.dp),
                contentAlignment = Alignment.Center
            ) {
                Row(
                    modifier = Modifier,
                    horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(
                        modifier = Modifier.size(20.dp),
                        imageVector = if (added) Icons.Default.Check else Icons.Default.Add,
                        contentDescription = "check icon",
                        tint = if (added) Color.White else MyBlueColor,
                    )
                    Text(
                        text = if (added) "Insurance Added" else "Add Insurance",
                        style = TextStyle(
                            fontSize = 12.sp,
                            fontWeight = FontWeight(600),
                            color = if (added) Color.White else MyBlueColor,
                            textAlign = TextAlign.Start,
                        )
                    )
                }
            }
        }
    }
}