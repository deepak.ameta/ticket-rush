package com.tridhyatech.ticket.flight.ui.screens.search.composables

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.common.utils.stationMap
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.ui.screens.search.FlightSearchState

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SearchResultTopSection(uiState: FlightSearchState, onChangeClick: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(horizontal = 16.dp)
            .background(Color.White),
        verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .weight(1f),
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = "FLIGHT TO",
                    style = TextStyle(
                        fontSize = 10.sp,
                        lineHeight = 14.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        letterSpacing = 0.8.sp,
                    )
                )
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                    verticalAlignment = Alignment.Top,
                ) {
                    Text(
                        modifier = Modifier.basicMarquee(),
                        text = stationMap[uiState.passengerDetail.from]!!,
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(600),
                            color = Color(0xFF1B1446),
                        )
                    )
                    Image(
                        modifier = Modifier
                            .padding(1.dp)
                            .width(21.dp)
                            .height(24.dp),
                        painter = painterResource(id = R.drawable.chevron_right_blue),
                        contentDescription = "image description",
                        contentScale = ContentScale.None
                    )
                    Text(
                        modifier = Modifier.weight(1f).basicMarquee(),
                        text = stationMap[uiState.passengerDetail.to]!!,
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(600),
                            color = Color(0xFF1B1446),
                        )
                    )
                }
            }
            Text(
                modifier = Modifier
                    .clip(RoundedCornerShape(12.dp))
                    .padding(horizontal = 8.dp, vertical = 4.dp)
                    .clickable { onChangeClick() },
                text = "Change",
                style = TextStyle(
                    fontSize = 12.sp,
                    fontWeight = FontWeight(600),
                    color = Color(0xFF0768FD),
                )
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
                .border(1.dp, Color(0x4D012276), RoundedCornerShape(12.dp))
                .padding(8.dp),
            horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.CenterHorizontally),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .size(16.dp),
                painter = painterResource(id = R.drawable.date_range_orange),
                contentDescription = "date range icon",
                contentScale = ContentScale.FillWidth
            )
            Text(
                text = uiState.passengerDetail.date,
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                )
            )
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .size(16.dp),
                painter = painterResource(id = R.drawable.person_small_icon),
                contentDescription = "person icon",
                contentScale = ContentScale.None
            )
            Text(
                text = "${uiState.passengerDetail.passengerCount} Person",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                )
            )
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .size(16.dp),
                painter = painterResource(id = R.drawable.rating_small_icon),
                contentDescription = "rating icon",
                contentScale = ContentScale.None
            )
            Text(
                modifier = Modifier.basicMarquee(),
                text = uiState.passengerDetail.seatType,
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = Color(0xFF1B1446),
                )
            )
        }
    }
}