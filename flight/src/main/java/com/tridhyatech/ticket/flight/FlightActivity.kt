package com.tridhyatech.ticket.flight

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.tridhyatech.ticket.flight.navigation.FlightNavHost
import com.tridhyatech.ticket.flight.navigation.utils.rememberFlightState
import com.tridhyatech.ticket.flight.ui.theme.TicketRushTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FlightActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TicketRushTheme {
                FlightNavHost(flightState = rememberFlightState())
            }
        }
    }
}