package com.tridhyatech.ticket.flight.ui.screens.payment.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor

//@Composable
//fun CompletePaymentSection() {
//    Column(
//        modifier = Modifier
//            .fillMaxWidth()
//            .padding(horizontal = 16.dp)
//            .border(
//                width = 1.dp,
//                color = FlightBorderColor,
//                shape = RoundedCornerShape(size = 16.dp)
//            )
//            .background(color = Color(0xFFFFFFFF), shape = RoundedCornerShape(size = 16.dp))
//            .padding(start = 16.dp, top = 12.dp, end = 16.dp, bottom = 12.dp),
//        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
//        horizontalAlignment = Alignment.Start,
//    ) {
//        Text(
//            text = "Complete your payment",
//            style = TextStyle(
//                fontSize = 14.sp,
//                fontWeight = FontWeight(600),
//                color = DarkBlueTextColor,
//                textAlign = TextAlign.Center,
//            )
//        )
//        CompletePaymentCard()
//    }
//}