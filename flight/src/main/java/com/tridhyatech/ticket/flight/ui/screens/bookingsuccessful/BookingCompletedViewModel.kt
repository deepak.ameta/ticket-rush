package com.tridhyatech.ticket.flight.ui.screens.bookingsuccessful

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.flightDataStore
import com.tridhyatech.ticket.common.data.session.passengerData
import com.tridhyatech.ticket.common.domain.model.flight.FlightDetail
import com.tridhyatech.ticket.common.domain.model.flight.PassengerDetail
import com.tridhyatech.ticket.common.utils.fromJson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookingCompletedViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(BookingCompletedState())
    val uiState: StateFlow<BookingCompletedState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val flight = session.getValue(flightDataStore, "").fromJson<FlightDetail>()
            val passenger = session.getValue(passengerData, "").fromJson<PassengerDetail>()

            _uiState.update {
                it.copy(
                    flightDetail = flight!!,
                    passengerDetail = passenger!!,
                )
            }
        }
    }

    fun updateData(action: BookingCompletedActions) {
        when (action) {
            BookingCompletedActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            BookingCompletedActions.OnNext -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnNext)
                }
            }
        }
    }
}

data class BookingCompletedState(
    val flightDetail: FlightDetail = FlightDetail(),
    val passengerDetail: PassengerDetail = PassengerDetail(),
)

sealed interface BookingCompletedActions {
    object OnNext : BookingCompletedActions
    object OnBack : BookingCompletedActions
}