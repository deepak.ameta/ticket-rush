package com.tridhyatech.ticket.flight.ui.screens.payment

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.navigation.utils.FlightState

const val completePaymentRoute = "complete_payment_route"

fun NavController.navigateToCompletePayment(navOptions: NavOptions? = null) {
    this.navigate(completePaymentRoute, navOptions)
}

fun NavGraphBuilder.completePaymentScreen(state: FlightState) {
    composable(route = completePaymentRoute) {
        CompletePaymentRoute(state = state)
    }
}