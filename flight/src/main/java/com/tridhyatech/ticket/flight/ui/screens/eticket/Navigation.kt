package com.tridhyatech.ticket.flight.ui.screens.eticket

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.navigation.utils.FlightState

const val ticketRoute = "e_ticket_route"

fun NavController.navigateToETicket(navOptions: NavOptions? = null) {
    this.navigate(ticketRoute, navOptions)
}

fun NavGraphBuilder.eTicketScreen(state: FlightState) {
    composable(route = ticketRoute) {
        TicketRoute(state = state)
    }
}