package com.tridhyatech.ticket.flight.ui.screens.home.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize
import com.tridhyatech.ticket.common.utils.dummyAirportList
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.ui.common.ItemsCategory
import com.tridhyatech.ticket.flight.ui.screens.home.FlightHomeActions
import com.tridhyatech.ticket.flight.ui.screens.home.FlightHomeState
import com.tridhyatech.ticket.flight.ui.theme.HomeScreenBgColor

@Composable
fun SearchFlightSection(
    uiState: FlightHomeState,
    onAction: (FlightHomeActions) -> Unit,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(start = 16.dp, top = 16.dp, end = 16.dp, bottom = 0.dp)
            .background(HomeScreenBgColor),
        contentAlignment = Alignment.CenterEnd,
    ) {
        Image(
            modifier = Modifier
                .padding(0.dp)
                .width(32.dp)
                .height(49.dp)
                .clickable { onAction(FlightHomeActions.ToggleFromTo) },
            painter = painterResource(id = R.drawable.toggle_flight),
            contentDescription = "toggle flight destination switch",
            contentScale = ContentScale.None
        )
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(start = 16.dp, top = 16.dp, end = 16.dp, bottom = 16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically),
            horizontalAlignment = Alignment.Start,
        ) {
            Row(
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .width(24.dp)
                        .height(24.dp),
                    painter = painterResource(id = R.drawable.flight_takeoff),
                    contentDescription = "take off flight icon",
                    contentScale = ContentScale.None
                )
                Column(
                    modifier = Modifier.fillMaxWidth(1f),
                    verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                    horizontalAlignment = Alignment.Start,
                ) {
                    Text(
                        modifier = Modifier,
                        text = "TAKE OFF",
                        style = TextStyle(
                            fontSize = 10.sp,
                            lineHeight = 14.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFF808080),
                            letterSpacing = 0.8.sp,
                        )
                    )
                    TransparentTextViewWithSuggestion(
                        stationName = uiState.selectedFrom,
                        onSelectionChange = {
                            onAction(FlightHomeActions.UpdateFrom(it))
                        }
                    )
                }
            }
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(3.dp)
                    .padding(horizontal = 16.dp)
                    .background(Color(0x0D012276))
            )
            Row(
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .width(24.dp)
                        .height(24.dp),
                    painter = painterResource(id = R.drawable.flight_land),
                    contentDescription = "flight landing icon",
                    contentScale = ContentScale.None
                )
                Column(
                    modifier = Modifier.fillMaxWidth(1f),
                    verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
                    horizontalAlignment = Alignment.Start,
                ) {
                    Text(
                        text = "LANDING",
                        style = TextStyle(
                            fontSize = 10.sp,
                            lineHeight = 14.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFF808080),
                            letterSpacing = 0.8.sp,
                        )
                    )
                    TransparentTextViewWithSuggestion(
                        stationName = uiState.selectedTo,
                        onSelectionChange = {
                            onAction(FlightHomeActions.UpdateTo(it))
                        }
                    )
                }
            }
        }
    }
}

@Composable
fun TransparentTextViewWithSuggestion(
    stationName: String,
    onSelectionChange: (String) -> Unit,
) {
    val focusManager = LocalFocusManager.current
    val airportList = dummyAirportList

    var rowSize by remember { mutableStateOf(Size.Zero) }
    var expanded by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .onGloballyPositioned {
                rowSize = it.size.toSize()
            },
        verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top)
    ) {
        BasicTextField(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 8.dp, vertical = 8.dp),
            value = stationName,
            onValueChange = {
                onSelectionChange(it)
                expanded = true
            },
            keyboardOptions = KeyboardOptions(
                capitalization = KeyboardCapitalization.Words,
                autoCorrect = false,
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done
            ),
            maxLines = 1,
            singleLine = true,
            textStyle = TextStyle(
                fontSize = 14.sp,
                fontWeight = FontWeight(500),
                color = Color(0xFF1B1446),
            ),
            interactionSource = remember { MutableInteractionSource() }
                .also { interactionSource ->
                    LaunchedEffect(interactionSource) {
                        interactionSource.interactions.collect {
                            if (it is PressInteraction.Release) {
//                                expanded = true
                            }
                        }
                    }
                }
        )
        AnimatedVisibility(visible = expanded) {

            val sortedList = if (stationName.isNotEmpty()) {
                airportList.filter { name ->
                    name.lowercase().contains(stationName.lowercase())
                }.sorted()
            } else {
                airportList.sorted()
            }

            Card(
                modifier = Modifier
                    .padding(horizontal = 4.dp, vertical = 4.dp)
                    .width(rowSize.width.dp),
                shape = RoundedCornerShape(8.dp)
            ) {
                LazyColumn(
                    modifier = Modifier.heightIn(max = 80.dp),
                ) {
                    items(sortedList.size) {
                        ItemsCategory(title = sortedList[it]) { title ->
                            onSelectionChange(title)
                            focusManager.clearFocus()
                            expanded = false
                        }
                    }
                }
            }
        }
    }
}