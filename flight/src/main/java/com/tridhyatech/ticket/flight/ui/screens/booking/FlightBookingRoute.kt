package com.tridhyatech.ticket.flight.ui.screens.booking

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.common.FlightSectionCommonHeader
import com.tridhyatech.ticket.flight.ui.screens.booking.composables.BookingBottomSection
import com.tridhyatech.ticket.flight.ui.screens.booking.composables.ContactDetailsSection
import com.tridhyatech.ticket.flight.ui.screens.booking.composables.ExtraProtectionSection
import com.tridhyatech.ticket.flight.ui.screens.booking.composables.FacilitySection
import com.tridhyatech.ticket.flight.ui.screens.booking.composables.FlightDetailCard
import com.tridhyatech.ticket.flight.ui.screens.booking.composables.PassengerInfoSection
import com.tridhyatech.ticket.flight.ui.screens.booking.composables.ProcessStepSection
import com.tridhyatech.ticket.flight.ui.screens.internalbooking.AddBaggageDialog
import com.tridhyatech.ticket.flight.ui.theme.HomeScreenBgColor
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@Composable
fun FlightBookingRoute(
    state: FlightState,
    viewModel: FlightBookingViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.loadData()
        viewModel.uiEvent.collectLatest {
            when (it) {
                BookingEvents.OnBack -> state.popBackStack()
                BookingEvents.OnNext -> state.navigateToCompletePayment()
                BookingEvents.OnAddContact -> state.navigateToAddEditContactDetails()
                BookingEvents.OnAddPassenger -> state.navigateToAddPassengerInfo()
                is BookingEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    CompleteYourBookingScreen(uiState, viewModel::updateData)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CompleteYourBookingScreen(
    uiState: FlightBookingState,
    onAction: (FlightBookingActions) -> Unit,
) {
    val sheetState = rememberModalBottomSheetState()
    val scope = rememberCoroutineScope()
    var showBottomSheet by remember { mutableStateOf(false) }

    if (uiState.showLuggageSheet) {
        ModalBottomSheet(
            onDismissRequest = {
                onAction(FlightBookingActions.ToggleLuggageSheet(false))
                showBottomSheet = false
            },
            sheetState = sheetState,
        ) {
            AddBaggageDialog(uiState, onAction) {
                scope.launch { sheetState.hide() }.invokeOnCompletion {
                    if (!sheetState.isVisible) {
                        onAction(FlightBookingActions.ToggleLuggageSheet(false))
                        showBottomSheet = false
                    }
                }
            }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize(1f)
            .background(HomeScreenBgColor),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .background(Color.White)
                .padding(bottom = 16.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            FlightSectionCommonHeader(title = "Complete Your Booking", onBackClick = {
                onAction(FlightBookingActions.OnBack)
            })
            ProcessStepSection(onBookingPage = true)
        }
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .weight(1f, true)
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            FlightDetailCard(uiState)
            ContactDetailsSection(uiState, onAction)
            PassengerInfoSection(uiState, onAction)
            FacilitySection(uiState, onAction)
            ExtraProtectionSection(uiState, onAction)
        }
        BookingBottomSection(uiState, onAction)
    }
}