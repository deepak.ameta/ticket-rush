package com.tridhyatech.ticket.flight.ui.screens.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.passengerCount
import com.tridhyatech.ticket.common.data.session.passengerData
import com.tridhyatech.ticket.common.domain.model.flight.PassengerDetail
import com.tridhyatech.ticket.common.utils.dummyRecentSearchList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FlightHomeViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private var _uiEvent = Channel<UiEvents>()
    val uiEvent = _uiEvent.receiveAsFlow()

    private var _uiState = MutableStateFlow(FlightHomeState())
    val uiState = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            session.setValue(passengerCount, 0)
            _uiState.update { it.copy(recentSearchList = dummyRecentSearchList) }
        }
    }

    fun updateData(action: FlightHomeActions) {
        when (action) {
            FlightHomeActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            FlightHomeActions.OnNext -> {
                viewModelScope.launch {
                    session.setValue(
                        passengerData,
                        Gson().toJson(uiState.value.toPassengerDetail())
                    )
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            FlightHomeActions.ToggleFromTo -> {
                val state = uiState.value
                _uiState.update {
                    it.copy(
                        selectedFrom = state.selectedTo,
                        selectedTo = state.selectedFrom
                    )
                }
            }

            is FlightHomeActions.UpdateFlightClass -> {
                viewModelScope.launch {
                    _uiState.update {
                        it.copy(seatType = action.type)
                    }
                }
            }

            is FlightHomeActions.UpdateJourneyDate -> {
                viewModelScope.launch {
                    _uiState.update {
                        it.copy(journeyDate = action.date)
                    }
                }
            }

            is FlightHomeActions.UpdateFrom -> {
                _uiState.update { it.copy(selectedFrom = action.station) }
            }

            is FlightHomeActions.UpdateTo -> {
                _uiState.update { it.copy(selectedTo = action.station) }
            }

            is FlightHomeActions.RecentItemClick -> {
                val detail = action.data
                viewModelScope.launch {
                    _uiState.update {
                        it.copy(
                            selectedFrom = detail.from,
                            selectedTo = detail.to,
                            passengerCount = detail.passengerCount,
                            journeyDate = detail.date,
                            seatType = detail.seatType
                        )
                    }
                }
                viewModelScope.launch {
                    session.setValue(passengerCount, detail.passengerCount)
                }
            }
        }
    }
}

data class FlightHomeState(
    val passengerCount: Int = 0,
    val seatType: String = "Economy",
    val journeyDate: String = "",
    val selectedFrom: String = "",
    val selectedTo: String = "",
    val recentSearchList: ArrayList<PassengerDetail> = arrayListOf(),
)

sealed interface FlightHomeActions {
    object OnNext : FlightHomeActions
    object OnBack : FlightHomeActions
    object ToggleFromTo : FlightHomeActions
    class UpdateFlightClass(val type: String) : FlightHomeActions
    class UpdateJourneyDate(val date: String) : FlightHomeActions
    class UpdateFrom(val station: String) : FlightHomeActions
    class UpdateTo(val station: String) : FlightHomeActions
    class RecentItemClick(val data: PassengerDetail) : FlightHomeActions
}

fun FlightHomeState.toPassengerDetail() = PassengerDetail(
    from = this.selectedFrom,
    to = this.selectedTo,
    passengerCount = this.passengerCount,
    date = this.journeyDate,
    seatType = this.seatType
)