package com.tridhyatech.ticket.flight.utils

import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class OpenDatePicker(private val mContext: Context, private val onDateSelection: (String) -> Unit) {

    private val mCalendar = Calendar.getInstance()

    fun showDatePickerDialog() {

        val mYear = mCalendar.get(Calendar.YEAR)
        val mMonth = mCalendar.get(Calendar.MONTH)
        val mDay = mCalendar.get(Calendar.DAY_OF_MONTH)

        val mDatePickerDialog = DatePickerDialog(
            mContext,
            { _: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int ->

                // Here we'll convert date in required format (01 January 2024).
                mCalendar.set(year, monthOfYear, dayOfMonth)
                val sdf = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
                val formattedDate: String = sdf.format(mCalendar.time)
                onDateSelection(formattedDate)

            }, mYear, mMonth, mDay
        )

        mDatePickerDialog.show()
    }
}