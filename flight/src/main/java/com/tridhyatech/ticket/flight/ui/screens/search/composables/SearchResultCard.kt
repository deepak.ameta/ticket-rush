package com.tridhyatech.ticket.flight.ui.screens.search.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.common.domain.model.flight.FlightDetail
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor

@Composable
fun SearchResultCard(
    flightDetail: FlightDetail,
    onFlightSelection: (FlightDetail) -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .clip(RoundedCornerShape(16.dp))
            .clickable { onFlightSelection(flightDetail) }
            .border(
                width = 1.dp,
                color = FlightBorderColor,
                shape = RoundedCornerShape(size = 16.dp)
            )
            .background(Color.White)
            .padding(start = 16.dp, top = 12.dp, end = 16.dp, bottom = 12.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.Start,
    ) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(
                modifier = Modifier
                    .border(
                        width = 1.dp,
                        color = FlightBorderColor,
                        shape = RoundedCornerShape(size = 5.dp)
                    )
                    .width(34.dp)
                    .height(17.dp)
                    .background(
                        color = Color(0xFFDDDDDD),
                        shape = RoundedCornerShape(size = 4.95833.dp)
                    )
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = flightDetail.companyName,
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    text = flightDetail.flightType,
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(400),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Image(
                modifier = Modifier
                    .padding(1.dp)
                    .size(24.dp),
                painter = painterResource(id = R.drawable.icon_down_arrow_small_gray),
                contentDescription = "down arrow icon",
                contentScale = ContentScale.None
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .border(
                    width = 1.dp,
                    color = FlightBorderColor,
                    shape = RoundedCornerShape(size = 16.dp)
                )
                .padding(8.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = flightDetail.source,
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    modifier = Modifier,
                    text = flightDetail.departureTime,
                    style = TextStyle(
                        fontSize = 18.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .weight(1f),
                horizontalArrangement = Arrangement.spacedBy(0.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .weight(1f)
                        .height(1.dp),
                    painter = painterResource(id = R.drawable.icon_dashline_destination),
                    contentDescription = "dashed line source to destination",
                    contentScale = ContentScale.None
                )
                Row(
                    modifier = Modifier
                        .border(
                            width = 1.dp,
                            color = FlightBorderColor,
                            shape = RoundedCornerShape(size = 28.dp)
                        )
                        .padding(start = 8.dp, top = 4.dp, end = 16.dp, bottom = 4.dp),
                    horizontalArrangement = Arrangement.spacedBy(
                        8.dp,
                        Alignment.CenterHorizontally
                    ),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        modifier = Modifier
                            .padding(1.dp)
                            .width(16.dp)
                            .height(16.dp),
                        painter = painterResource(id = R.drawable.icon_plane_search_screen),
                        contentDescription = "plane icon",
                        contentScale = ContentScale.None
                    )
                    Column(
                        verticalArrangement = Arrangement.spacedBy(2.dp, Alignment.Top),
                        horizontalAlignment = Alignment.Start,
                    ) {
                        Text(
                            text = "Transit",
                            style = TextStyle(
                                fontSize = 10.sp,
                                fontWeight = FontWeight(500),
                                color = Color(0xFF808080),
                                textAlign = TextAlign.Center,
                            )
                        )
                        Text(
                            text = flightDetail.totalTime,
                            style = TextStyle(
                                fontSize = 12.sp,
                                fontWeight = FontWeight(600),
                                color = Color(0xFF1B1446),
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                }
                Image(
                    modifier = Modifier
                        .padding(1.dp)
                        .weight(1f)
                        .height(1.dp),
                    painter = painterResource(id = R.drawable.icon_dashline_destination),
                    contentDescription = "dashed line source to destination",
                    contentScale = ContentScale.None
                )
            }
            Column(
                modifier = Modifier,
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.CenterVertically),
                horizontalAlignment = Alignment.End,
            ) {
                Text(
                    text = flightDetail.destination,
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF808080),
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    text = flightDetail.arrivalTime,
                    style = TextStyle(
                        fontSize = 18.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
        Row(
            modifier = Modifier.fillMaxWidth(1f),
            horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.End),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .weight(1f),
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
                verticalAlignment = Alignment.Top,
            ) {
                Column(
                    verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                    horizontalAlignment = Alignment.Start,
                ) {
                    Row(
                        horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Image(
                            modifier = Modifier
                                .padding(1.dp)
                                .size(12.dp),
                            painter = painterResource(id = R.drawable.icon_luggage),
                            contentDescription = "luggage icon",
                            contentScale = ContentScale.None
                        )
                        Text(
                            text = "20 Kg",
                            style = TextStyle(
                                fontSize = 10.sp,
                                fontWeight = FontWeight(500),
                                color = Color(0xFF1B1446),
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                    Row(
                        horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Image(
                            modifier = Modifier
                                .padding(1.dp)
                                .size(12.dp),
                            painter = painterResource(id = R.drawable.icon_wifi),
                            contentDescription = "wifi icon",
                            contentScale = ContentScale.None
                        )
                        Text(
                            text = "Wifi",
                            style = TextStyle(
                                fontSize = 10.sp,
                                fontWeight = FontWeight(500),
                                color = Color(0xFF1B1446),
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                }
                Row(
                    horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        modifier = Modifier
                            .padding(1.dp)
                            .size(12.dp),
                        painter = painterResource(id = R.drawable.icon_food),
                        contentDescription = "food icon",
                        contentScale = ContentScale.None
                    )
                    Text(
                        text = "Food",
                        style = TextStyle(
                            fontSize = 10.sp,
                            fontWeight = FontWeight(500),
                            color = Color(0xFF1B1446),
                            textAlign = TextAlign.Center,
                        )
                    )
                }
            }
            Column(
                verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.CenterVertically),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = "INR ${flightDetail.price}",
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontWeight = FontWeight(600),
                        color = Color(0xFF1B1446),
                        textAlign = TextAlign.Center,
                    )
                )
                Text(
                    text = "/Person",
                    style = TextStyle(
                        fontSize = 10.sp,
                        fontWeight = FontWeight(500),
                        color = Color(0xFF0768FD),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
    }
}