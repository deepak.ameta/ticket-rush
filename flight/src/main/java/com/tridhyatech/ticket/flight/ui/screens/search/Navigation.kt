package com.tridhyatech.ticket.flight.ui.screens.search

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.navigation.utils.FlightState

const val searchResultRoute = "searchResult_route"

fun NavController.navigateToSearchResult(navOptions: NavOptions? = null) {
    this.navigate(searchResultRoute, navOptions)
}

fun NavGraphBuilder.searchResultScreen(state: FlightState) {
    composable(route = searchResultRoute) {
        FlightSearchRoute(state = state)
    }
}