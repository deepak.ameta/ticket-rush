package com.tridhyatech.ticket.flight.ui.screens.booking.composables

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor

@Composable
fun ProcessStepSection(
    onBookingPage: Boolean = false,
    onPaymentPage: Boolean = false,
    onCompletePage: Boolean = false
) {

    val selectedColor = MyBlueColor
    val unSelectedColor = LightGrayTextColor
    val lineColor = Color.LightGray

    val selectedTextColor = Color.Black
    val unSelectedText = LightGrayTextColor

    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(horizontal = 24.dp),
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 12.dp),
            horizontalArrangement = Arrangement.SpaceAround,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(
                Modifier
                    .size(24.dp)
                    .border(
                        6.dp,
                        if (onBookingPage) selectedColor else unSelectedColor,
                        CircleShape
                    )
            )
            Spacer(
                Modifier
                    .weight(1f)
                    .height(4.dp)
                    .border(2.dp, if (onPaymentPage) selectedColor else lineColor, RectangleShape)
            )
            Box(
                Modifier
                    .size(24.dp)
                    .border(
                        6.dp,
                        if (onPaymentPage) selectedColor else unSelectedColor,
                        CircleShape
                    )
            )
            Spacer(
                Modifier
                    .weight(1f)
                    .height(4.dp)
                    .border(2.dp, if (onCompletePage) selectedColor else lineColor, RectangleShape)
            )
            Box(
                Modifier
                    .size(24.dp)
                    .border(
                        6.dp,
                        if (onCompletePage) selectedColor else unSelectedColor,
                        CircleShape
                    )
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(1f),
            horizontalArrangement = Arrangement.Absolute.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = "Booking",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = if (onBookingPage) selectedTextColor else unSelectedText,
                    textAlign = TextAlign.Start,
                )
            )
            Text(
                text = "Payment",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = if (onPaymentPage) selectedTextColor else unSelectedText,
                    textAlign = TextAlign.Center,
                )
            )
            Text(
                modifier = Modifier.offset(4.dp),
                text = "Complete",
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight(500),
                    color = if (onCompletePage) selectedTextColor else unSelectedText,
                    textAlign = TextAlign.Center,
                )
            )
        }
    }
}