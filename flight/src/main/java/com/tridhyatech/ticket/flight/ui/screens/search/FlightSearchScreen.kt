package com.tridhyatech.ticket.flight.ui.screens.search

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.common.FlightSectionCommonHeader
import com.tridhyatech.ticket.flight.ui.screens.search.composables.SearchResultCard
import com.tridhyatech.ticket.flight.ui.screens.search.composables.SearchResultTopSection
import com.tridhyatech.ticket.flight.ui.theme.HomeScreenBgColor
import kotlinx.coroutines.flow.collectLatest

@Composable
fun FlightSearchRoute(
    state: FlightState,
    viewModel: FlightSearchViewModel = hiltViewModel()
) {

    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> state.popBackStack()
                UiEvents.OnNext -> state.navigateToCompleteBooking()
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    FlightSearchScreen(uiState, viewModel::updateData)
}

@Composable
fun FlightSearchScreen(
    uiState: FlightSearchState,
    onAction: (FlightSearchActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(HomeScreenBgColor),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .background(Color.White),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            FlightSectionCommonHeader(onBackClick = { onAction(FlightSearchActions.OnBack) })
            SearchResultTopSection(uiState) { onAction(FlightSearchActions.OnBack) }
        }
        LazyColumn(
            modifier = Modifier.fillMaxWidth(1f),
            verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            items(uiState.flightList.size) {
                SearchResultCard(
                    flightDetail = uiState.flightList[it],
                    onFlightSelection = { flightDetail ->
                        onAction(FlightSearchActions.OnFlightSelection(flightDetail))
                    },
                )
            }
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewFlightSearchScreen() {
//    FlightSearchScreen()
//}