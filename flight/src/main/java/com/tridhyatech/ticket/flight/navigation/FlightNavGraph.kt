package com.tridhyatech.ticket.flight.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.screens.booking.completeYourBookingScreen
import com.tridhyatech.ticket.flight.ui.screens.bookingsuccessful.paymentSuccessfulScreen
import com.tridhyatech.ticket.flight.ui.screens.eticket.eTicketScreen
import com.tridhyatech.ticket.flight.ui.screens.home.homeRoute
import com.tridhyatech.ticket.flight.ui.screens.home.homeScreen
import com.tridhyatech.ticket.flight.ui.screens.internalbooking.contact.contactDetailScreen
import com.tridhyatech.ticket.flight.ui.screens.internalbooking.passenger_info.passengerScreen
import com.tridhyatech.ticket.flight.ui.screens.payment.completePaymentScreen
import com.tridhyatech.ticket.flight.ui.screens.search.searchResultScreen
import com.tridhyatech.ticket.flight.ui.screens.transactiondetail.transactionDetailScreen

@Composable
fun FlightNavHost(
    flightState: FlightState,
    startDestination: String = homeRoute,
) {
    val navController = flightState.flightNavController

    NavHost(
        navController = navController as NavHostController,
        startDestination = startDestination,
    ) {
        homeScreen(state = flightState)
        searchResultScreen(state = flightState)
        completeYourBookingScreen(state = flightState)
        completePaymentScreen(state = flightState)
        paymentSuccessfulScreen(state = flightState)
        transactionDetailScreen(state = flightState)
        eTicketScreen(state = flightState)

        contactDetailScreen(state = flightState)
        passengerScreen(state = flightState)
    }
}