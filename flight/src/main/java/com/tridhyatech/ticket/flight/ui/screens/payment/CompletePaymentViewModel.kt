package com.tridhyatech.ticket.flight.ui.screens.payment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.flightBookingDataStore
import com.tridhyatech.ticket.common.data.session.flightDataStore
import com.tridhyatech.ticket.common.data.session.passengerData
import com.tridhyatech.ticket.common.data.session.transactionDataStore
import com.tridhyatech.ticket.common.domain.model.flight.BookingDetail
import com.tridhyatech.ticket.common.domain.model.flight.FlightDetail
import com.tridhyatech.ticket.common.domain.model.flight.PassengerDetail
import com.tridhyatech.ticket.common.domain.model.flight.TransactionDetail
import com.tridhyatech.ticket.common.utils.fromJson
import com.tridhyatech.ticket.common.utils.toJson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CompletePaymentViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(CompletePaymentState())
    val uiState: StateFlow<CompletePaymentState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val flight = session.getValue(flightDataStore, "").fromJson<FlightDetail>()
            val passenger = session.getValue(passengerData, "").fromJson<PassengerDetail>()
            val booking = session.getValue(flightBookingDataStore, "").fromJson<BookingDetail>()

            _uiState.update {
                it.copy(
                    flightDetail = flight!!,
                    passengerDetail = passenger!!,
                    bookingDetail = booking ?: BookingDetail()
                )
            }
        }
    }

    fun updateData(action: CompletePaymentActions) {
        when (action) {
            CompletePaymentActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            CompletePaymentActions.OnNext -> {
                viewModelScope.launch {
                    session.setValue(
                        transactionDataStore,
                        TransactionDetail(
                            status = "Success",
                            invoice = "U345458430ABE",
                            transactionDate = "24-Feb-2024",
                            paymentMethod = uiState.value.selectedPaymentMethod,
                            amountPaid = uiState.value.bookingDetail.finalPrice,
                        ).toJson()
                    )
                    _uiEvent.send(UiEvents.OnNext)
                }
            }

            is CompletePaymentActions.TogglePaymentDialog -> {
                _uiState.update { it.copy(selectPaymentDialog = action.show) }
            }

            is CompletePaymentActions.UpdatePaymentMethod -> {
                _uiState.update {
                    it.copy(
                        selectedPaymentMethod = action.name,
                        selectPaymentDialog = false
                    )
                }
            }

            is CompletePaymentActions.CouponUpdate -> {
                _uiState.update { it.copy(couponValue = action.coupon) }
            }
        }
    }
}

data class CompletePaymentState(
    val flightDetail: FlightDetail = FlightDetail(),
    val passengerDetail: PassengerDetail = PassengerDetail(),
    val selectPaymentDialog: Boolean = false,
    val selectedPaymentMethod: String = "Paytm",
    val couponValue: String = "",
    val bookingDetail: BookingDetail = BookingDetail()
)

sealed interface CompletePaymentActions {
    object OnNext : CompletePaymentActions
    object OnBack : CompletePaymentActions
    data class TogglePaymentDialog(val show: Boolean) : CompletePaymentActions
    data class UpdatePaymentMethod(val name: String) : CompletePaymentActions
    data class CouponUpdate(val coupon: String) : CompletePaymentActions
}