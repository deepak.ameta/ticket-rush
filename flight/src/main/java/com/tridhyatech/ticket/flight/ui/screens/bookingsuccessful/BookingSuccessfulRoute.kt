package com.tridhyatech.ticket.flight.ui.screens.bookingsuccessful

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.common.FlightCustomButton
import com.tridhyatech.ticket.flight.ui.screens.booking.composables.ProcessStepSection
import com.tridhyatech.ticket.flight.ui.screens.payment.composables.CompletePaymentCard
import kotlinx.coroutines.flow.collectLatest

@Composable
fun BookingSuccessfulRoute(
    state: FlightState,
    viewModel: BookingCompletedViewModel = hiltViewModel(),
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> state.popBackStack()
                UiEvents.OnNext -> state.navigateToTransactionDetail()
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }

    BookingCompletedPage(uiState, viewModel::updateData)
}

@Composable
fun BookingCompletedPage(
    uiState: BookingCompletedState,
    onAction: (BookingCompletedActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize(1f)
            .background(Color.White),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start
    ) {
        Column(
            modifier = Modifier
                .weight(1f)
                .background(Color.White),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                modifier = Modifier
                    .size(164.dp)
                    .clip(CircleShape),
                painter = painterResource(id = R.drawable.payment_complete),
                contentDescription = "payment complete image",
            )
            Text(
                text = "Payment Complete",
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight(600),
                    color = Color(0xFF000000),
                )
            )
            ProcessStepSection(onBookingPage = true, onPaymentPage = true, onCompletePage = true)
            CompletePaymentCard(
                uiState.flightDetail.source,
                uiState.flightDetail.destination,
                uiState.passengerDetail.date,
                "${uiState.flightDetail.departureTime} UTC",
                uiState.flightDetail.totalTime
            )
        }
        FlightCustomButton(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 16.dp, vertical = 16.dp),
            text = "See Details",
            onClick = { onAction(BookingCompletedActions.OnNext) }
        )

    }
}