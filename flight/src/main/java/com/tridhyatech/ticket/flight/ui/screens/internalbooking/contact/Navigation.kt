package com.tridhyatech.ticket.flight.ui.screens.internalbooking.contact

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.navigation.utils.FlightState

const val contactDetailRoute = "contact_details_route"

fun NavController.navigateToContactDetail(navOptions: NavOptions? = null) {
    this.navigate(contactDetailRoute, navOptions)
}

fun NavGraphBuilder.contactDetailScreen(state: FlightState) {
    composable(route = contactDetailRoute) {
        ContactDetailRoute(state = state)
    }
}