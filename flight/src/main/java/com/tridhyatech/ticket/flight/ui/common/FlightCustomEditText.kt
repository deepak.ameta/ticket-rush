package com.tridhyatech.ticket.flight.ui.common

import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor

@Composable
fun FlightCustomEditText(
    modifier: Modifier = Modifier,
    label: String? = null,
    placeholder: String? = null,
    value: String,
    onValueChange: (String) -> Unit,
    keyBoardOptions: KeyboardOptions = KeyboardOptions(
        capitalization = KeyboardCapitalization.Words,
        autoCorrect = false,
        keyboardType = KeyboardType.Text,
        imeAction = ImeAction.Next
    ),
) {
    OutlinedTextField(
        modifier = modifier,
        label = { Text(text = label ?: "") },
        value = value,
        onValueChange = onValueChange,
        textStyle = TextStyle(
            fontSize = 16.sp,
            fontWeight = FontWeight(500),
            color = DarkBlueTextColor,
            textAlign = TextAlign.Start,
        ),
        placeholder = {
            Text(
                text = placeholder ?: "",
                style = TextStyle(
                    fontSize = 16.sp,
                    fontWeight = FontWeight(500),
                    color = DarkBlueTextColor,
                    textAlign = TextAlign.Start,
                ),
            )
        },
        keyboardOptions = keyBoardOptions
    )
}