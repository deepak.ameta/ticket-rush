package com.tridhyatech.ticket.flight.ui.screens.home.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.R

@Composable
fun FindFlightCustomButton(onFindClick: () -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .height(48.dp)
            .clip(RoundedCornerShape(16.dp))
            .background(color = Color(0xFF0768FD))
            .clickable { onFindClick() }
            .padding(start = 16.dp, end = 16.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterHorizontally),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Image(
            modifier = Modifier
                .padding(1.dp)
                .size(24.dp),
            painter = painterResource(id = R.drawable.search),
            contentDescription = "find flight button icon",
            contentScale = ContentScale.None
        )
        Text(
            modifier = Modifier
                .height(17.dp),
            text = "FIND YOUR PLANE",
            style = TextStyle(
                fontSize = 14.sp,
                fontWeight = FontWeight(600),
                color = Color(0xFFFFFFFF),
            )
        )
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewCustomButton() {
    FindFlightCustomButton(onFindClick = {})
}