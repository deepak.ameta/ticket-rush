package com.tridhyatech.ticket.flight.ui.screens.booking.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingActions
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor

@Composable
fun FacilitySection(
    uiState: FlightBookingState,
    onAction: (FlightBookingActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.Start,
    ) {
        Text(
            text = "Facility",
            style = TextStyle(
                fontSize = 16.sp,
                fontWeight = FontWeight(600),
                color = DarkBlueTextColor,
                textAlign = TextAlign.Center,
            )
        )
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .border(
                    width = 1.dp,
                    color = FlightBorderColor,
                    shape = RoundedCornerShape(size = 16.dp)
                )
                .background(color = Color(0xFFFFFFFF), shape = RoundedCornerShape(size = 16.dp))
                .padding(horizontal = 16.dp, vertical = 12.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically),
            horizontalAlignment = Alignment.Start,
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(1f),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Row(
                    modifier = Modifier,
                    horizontalArrangement = Arrangement.spacedBy(12.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        modifier = Modifier.size(32.dp),
                        painter = painterResource(id = R.drawable.icon_luggage),
                        contentDescription = "baggage icon"
                    )
                    Column(
                        modifier = Modifier,
                        verticalArrangement = Arrangement.spacedBy(
                            2.dp,
                            Alignment.CenterVertically
                        ),
                        horizontalAlignment = Alignment.Start,
                    ) {
                        Text(
                            text = "Baggage",
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight(600),
                                color = DarkBlueTextColor,
                                textAlign = TextAlign.Center,
                            )
                        )
                        Text(
                            text = when (uiState.luggagePrice) {
                                210 -> "5 Kg Luggage Added"
                                510 -> "10 Kg Luggage Added"
                                else -> "Add Extra Baggage"
                            },
                            style = TextStyle(
                                fontSize = 10.sp,
                                fontWeight = FontWeight(500),
                                color = LightGrayTextColor,
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                }
                Icon(
                    modifier = Modifier
                        .size(24.dp)
                        .clickable {
                            onAction(FlightBookingActions.ToggleLuggageSheet(true))
                        },
                    imageVector = Icons.Default.Add,
                    contentDescription = "add baggage icon",
                    tint = MyBlueColor
                )
            }
        }
    }
}