package com.tridhyatech.ticket.flight.ui.screens.transactiondetail

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.utils.showToast
import com.tridhyatech.ticket.flight.R
import com.tridhyatech.ticket.flight.navigation.utils.FlightState
import com.tridhyatech.ticket.flight.ui.common.FlightCustomButton
import com.tridhyatech.ticket.flight.ui.common.FlightSectionCommonHeader
import com.tridhyatech.ticket.flight.ui.screens.transactiondetail.composables.TicketDetailCard
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.FlightBorderColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import kotlinx.coroutines.flow.collectLatest

@Composable
fun TransactionDetailRoute(
    state: FlightState,
    viewModel: TransactionDetailViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(true) {
        viewModel.uiEvent.collectLatest {
            when (it) {
                UiEvents.OnBack -> state.popBackStack()
                UiEvents.OnNext -> state.navigateToETicket()
                is UiEvents.OnError -> context.showToast(it.message)
            }
        }
    }
    TransactionDetailScreen(uiState, viewModel::updateData)
}

@Composable
fun TransactionDetailScreen(
    uiState: TransactionDetailState,
    onAction: (TransactionDetailActions) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize(1f)
            .background(Color.White),
        verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
        horizontalAlignment = Alignment.Start
    ) {
        FlightSectionCommonHeader(title = "Transaction Details", onBackClick = {
            onAction(TransactionDetailActions.OnBack)
        })
        Column(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 16.dp)
                .weight(1f)
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.Top),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                modifier = Modifier
                    .clip(RoundedCornerShape(12.dp))
                    .size(84.dp),
                painter = painterResource(id = R.drawable.transactiondone),
                contentDescription = "transaction detail page image",
                contentScale = ContentScale.None,
            )
            TicketDetailCard(uiState)
            Column(
                modifier = Modifier.fillMaxWidth(1f),
                verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        text = "Status",
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(500),
                            color = LightGrayTextColor,
                        )
                    )
                    Text(
                        text = uiState.transaction.status,
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(600),
                            color = Color(0xFF136617),
                        )
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        text = "Invoice",
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(500),
                            color = LightGrayTextColor,
                        )
                    )
                    Text(
                        text = uiState.transaction.invoice,
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(600),
                            color = DarkBlueTextColor,
                        )
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        text = "Transaction Date",
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(500),
                            color = LightGrayTextColor,
                        )
                    )
                    Text(
                        text = uiState.transaction.transactionDate,
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(600),
                            color = DarkBlueTextColor,
                        )
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        text = "Payment Method",
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(500),
                            color = LightGrayTextColor,
                        )
                    )
                    Text(
                        text = uiState.transaction.paymentMethod,
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(600),
                            color = DarkBlueTextColor,
                        )
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        text = "Amount Paid",
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(500),
                            color = LightGrayTextColor,
                        )
                    )
                    Text(
                        text = uiState.transaction.amountPaid.toString(),
                        style = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight(600),
                            color = DarkBlueTextColor,
                        )
                    )
                }
            }
            Box(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .clip(RoundedCornerShape(8.dp))
                    .border(1.dp, FlightBorderColor, RoundedCornerShape(8.dp))
                    .clickable { onAction(TransactionDetailActions.ToggleRefundOrReschedule(true)) }
                    .padding(8.dp),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = "Refund or Reschedule Ticket",
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontWeight = FontWeight(500),
                        color = Color.Red,
                    ),
                )
            }
        }
        FlightCustomButton(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 16.dp, vertical = 16.dp),
            text = "See E-Ticket",
            onClick = { onAction(TransactionDetailActions.OnBack) },
        )
    }
}