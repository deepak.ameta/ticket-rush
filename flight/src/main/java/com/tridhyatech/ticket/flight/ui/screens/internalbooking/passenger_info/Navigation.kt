package com.tridhyatech.ticket.flight.ui.screens.internalbooking.passenger_info

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.tridhyatech.ticket.flight.navigation.utils.FlightState

const val passengerRoute = "passenger_route"

fun NavController.navigateToPassengerScreen(navOptions: NavOptions? = null) {
    this.navigate(passengerRoute, navOptions)
}

fun NavGraphBuilder.passengerScreen(state: FlightState) {
    composable(route = passengerRoute) {
        PassengerRoute(state = state)
    }
}