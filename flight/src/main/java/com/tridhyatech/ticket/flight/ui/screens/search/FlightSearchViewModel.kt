package com.tridhyatech.ticket.flight.ui.screens.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.tridhyatech.ticket.common.base.UiEvents
import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.flightDataStore
import com.tridhyatech.ticket.common.data.session.passengerData
import com.tridhyatech.ticket.common.domain.model.flight.FlightDetail
import com.tridhyatech.ticket.common.domain.model.flight.PassengerDetail
import com.tridhyatech.ticket.common.utils.dummySearchFlightResultList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FlightSearchViewModel @Inject constructor(
    private val session: Session
) : ViewModel() {

    private val _uiEvent = Channel<UiEvents>()
    val uiEvent: Flow<UiEvents> = _uiEvent.receiveAsFlow()

    private val _uiState = MutableStateFlow(FlightSearchState())
    val uiState: StateFlow<FlightSearchState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val jsonData = session.getValue(passengerData, "")
            val gsonData = Gson().fromJson(jsonData, PassengerDetail::class.java)
            _uiState.update {
                it.copy(
                    passengerDetail = gsonData,
                    flightList = dummySearchFlightResultList
                )
            }
        }
    }

    fun updateData(action: FlightSearchActions) {
        when (action) {
            FlightSearchActions.OnBack -> {
                viewModelScope.launch {
                    _uiEvent.send(UiEvents.OnBack)
                }
            }

            is FlightSearchActions.OnFlightSelection -> {
                viewModelScope.launch {
                    session.setValue(flightDataStore, Gson().toJson(action.flightDetail))
                    _uiEvent.send(UiEvents.OnNext)
                }
            }
        }
    }
}

data class FlightSearchState(
    val passengerDetail: PassengerDetail = PassengerDetail(),
    val flightList: ArrayList<FlightDetail> = arrayListOf()
)

sealed interface FlightSearchActions {
    object OnBack : FlightSearchActions
    data class OnFlightSelection(val flightDetail: FlightDetail) : FlightSearchActions
}