package com.tridhyatech.ticket.flight.ui.screens.booking.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tridhyatech.ticket.flight.ui.common.FlightCustomButton
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingActions
import com.tridhyatech.ticket.flight.ui.screens.booking.FlightBookingState
import com.tridhyatech.ticket.flight.ui.theme.DarkBlueTextColor
import com.tridhyatech.ticket.flight.ui.theme.LightGrayTextColor
import com.tridhyatech.ticket.flight.ui.theme.MyBlueColor

@Composable
fun BookingBottomSection(
    uiState: FlightBookingState,
    onAction: (FlightBookingActions) -> Unit,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .background(Color.White)
            .padding(8.dp, 8.dp, 8.dp, 16.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp, Alignment.Start),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Column(
            modifier = Modifier.padding(horizontal = 16.dp),
            verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Top),
            horizontalAlignment = Alignment.Start
        ) {
            Row(
                horizontalArrangement = Arrangement.spacedBy(0.dp, Alignment.Start),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = "SUBTOTAL",
                    style = TextStyle(
                        fontSize = 8.sp,
                        fontWeight = FontWeight(600),
                        color = LightGrayTextColor,
                        textAlign = TextAlign.Center,
                    )
                )
                Icon(
                    modifier = Modifier.size(20.dp),
                    imageVector = Icons.Default.KeyboardArrowDown,
                    contentDescription = "check icon",
                    tint = MyBlueColor
                )
            }
            Text(
                text = "INR ${
                    uiState.flightDetail.price +
                            uiState.luggagePrice +
                            if (uiState.insuranceAdded) 12 else 0
                }",

                style = TextStyle(
                    fontSize = 16.sp,
                    fontWeight = FontWeight(600),
                    color = DarkBlueTextColor,
                    textAlign = TextAlign.Center,
                )
            )
        }
        FlightCustomButton(
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .fillMaxWidth(1f),
            text = "Next",
            onClick = { onAction(FlightBookingActions.OnNext) }
        )
    }
}

