package com.tridhyatech.ticket.common.domain.model.flight

data class ContactDetail(
    val name: String = "",
    val mobileNo: Int = 0,
    val email: String = "",
    val id: String = "",
    val gender: String = "Male",
)
