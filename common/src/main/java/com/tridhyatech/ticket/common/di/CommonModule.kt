package com.tridhyatech.ticket.common.di

import com.tridhyatech.ticket.common.data.session.Session
import com.tridhyatech.ticket.common.data.session.SessionImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface CommonModule {

    @Binds
    fun provideSession(session: SessionImpl): Session
}