package com.tridhyatech.ticket.common.data.session

import android.content.Context
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import javax.inject.Inject

interface Session {
    suspend fun <T> getValue(key: Preferences.Key<T>, defaultValue: T): T
    suspend fun <T : Any> setValue(key: Preferences.Key<T>, value: T)
}

class SessionImpl @Inject constructor(@ApplicationContext private val context: Context) : Session {

    override suspend fun <T> getValue(
        key: Preferences.Key<T>,
        defaultValue: T
    ): T {
        val value = context.dataStore.data.map {
            it[key] ?: defaultValue
        }.first()
        return value
    }

    override suspend fun <T : Any> setValue(
        key: Preferences.Key<T>,
        value: T
    ) {
        context.dataStore.edit {
            it[key] = value
        }
    }
}