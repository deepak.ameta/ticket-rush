package com.tridhyatech.ticket.common.data.session

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "flight_preferences123")

/**
 * this is used for passenger count in flight module
 * this is the best approach when we wanted to show notification
 * or anything which can update its state from any page
 */
val passengerCount = intPreferencesKey("passenger_count")

/**
 * this key is used to store user's profile details
 */
val profileData = stringPreferencesKey("profile_data")

/**
 * this key is used to store passenger data for searching flights
 */
val passengerData = stringPreferencesKey("passenger_data")

/**
 * this key is used to store flight details for selected flights
 */
val flightDataStore = stringPreferencesKey("flight_data_store")

/**
 * this key is used to store flight details for selected flights
 */
val contactDataStore = stringPreferencesKey("contact_detail_data_store")
val allPassengerContactDataStore = stringPreferencesKey("all_passenger_contact_data_store")
val flightBookingDataStore = stringPreferencesKey("flight_booking_data_store")
val transactionDataStore = stringPreferencesKey("transaction_data_store")