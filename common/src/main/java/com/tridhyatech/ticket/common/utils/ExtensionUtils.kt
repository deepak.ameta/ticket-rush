package com.tridhyatech.ticket.common.utils

import android.content.Context
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Extension function to show toast
 */
fun Context.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

/**
 * Extension function to check if string is number only
 */
fun String.isDigit(): Boolean {
    return this.all { it.isDigit() }
}

/**
 * Extension function to convert an object to JSON string
 */
inline fun <reified T> T.toJson(): String {
    return Gson().toJson(this)
}

/**
 * Extension function to convert JSON string to an object
 */
inline fun <reified T> String.fromJson(): T? {
    return try {
        Gson().fromJson(this, object : TypeToken<T>() {}.type)
    } catch (e: Exception) {
        null
    }
}
