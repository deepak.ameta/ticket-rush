package com.tridhyatech.ticket.common.domain.model.flight

data class PassengerDetail(
    val from: String = "MTS",
    val to: String = "MTS",
    val date: String = "",
    val passengerCount: Int = 0,
    val seatType: String = "",
)