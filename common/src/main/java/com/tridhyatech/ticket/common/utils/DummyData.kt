package com.tridhyatech.ticket.common.utils

import com.tridhyatech.ticket.common.domain.model.PaymentInfo
import com.tridhyatech.ticket.common.domain.model.ReviewData
import com.tridhyatech.ticket.common.domain.model.flight.FlightDetail
import com.tridhyatech.ticket.common.domain.model.flight.PassengerDetail
import com.tridhyatech.ticket.common.domain.model.hotel.HotelDetail

val dummyHotelDataList = arrayListOf(
    HotelDetail("Keraton Yogyakarta", "YOGYAKARTA, DIY", 225, 125, 4.5, ""),
    HotelDetail("Hotel Alana", "SLEMAN, DIY", 220, 130, 4.2, ""),
    HotelDetail("Keraton Yogyakarta", "YOGYAKARTA, DIY", 225, 125, 4.5, ""),
)

val dummyPaymentDataList = arrayListOf(
    PaymentInfo(
        "Jakarta - Solo",
        "235803954",
        TicketType.FLIGHT,
        PaymentStatus.PENDING,
        "02 Nov 2024",
        1200
    ),
    PaymentInfo(
        "India - Solo",
        "465475668",
        TicketType.FLIGHT,
        PaymentStatus.COMPLETED,
        "23 Feb 2024",
        450
    ),
    PaymentInfo(
        "New York - Solo",
        "967745635",
        TicketType.FLIGHT,
        PaymentStatus.COMPLETED,
        "15 Mar 2024",
        1800
    ),
)

val dummyReviewList = arrayListOf(
    ReviewData(
        rating = 4.5,
        date = "04 Jan 2024",
        usefulCount = 25,
        quote = "Hyat residency has always been a favorite place to stay with friends and have fun."
    ),
    ReviewData(
        rating = 3.2,
        date = "04 Jan 2024",
        usefulCount = 10,
        quote = "Decent product, but could use some improvements.",
    ),
    ReviewData(
        rating = 5.0,
        date = "04 Jan 2024",
        usefulCount = 50,
        quote = "Hyat residency has always been a favorite place to stay with friends and have fun."
    )
)

val dummyAirportList = arrayListOf("MTS", "NJY", "NYC", "GTM")

val dummyRecentSearchList = arrayListOf(
    PassengerDetail(
        from = "MTS",
        to = "NJY",
        date = "26 Oct 2024",
        passengerCount = 3,
        seatType = "Premium Economy",
    ),
    PassengerDetail(
        from = "NYC",
        to = "GTM",
        date = "16 Oct 2024",
        passengerCount = 5,
        seatType = "Economy",
    ),
    PassengerDetail(
        from = "GTM",
        to = "NYC",
        date = "06 Oct 2024",
        passengerCount = 2,
        seatType = "Premium Economy",
    ),
)

val stationMap = mutableMapOf(
    "MTS" to "Metropolis",
    "NJY" to "New Jersey",
    "NYC" to "New York",
    "GTM" to "Gotham",
)

val dummySearchFlightResultList = arrayListOf(
    FlightDetail(
        companyName = "Southwest Airlines",
        flightType = "Executive",
        source = "GTC",
        departureTime = "14:00",
        destination = "KDC",
        arrivalTime = "07:05",
        totalTime = "1h 34m",
        price = 350.00,
    ),
    FlightDetail(
        companyName = "Delta Airlines",
        flightType = "Business",
        source = "JFK",
        departureTime = "12:30",
        destination = "LAX",
        arrivalTime = "18:45",
        totalTime = "5h 15m",
        price = 500.00,
    ),
    FlightDetail(
        companyName = "United Airlines",
        flightType = "Economy",
        source = "ORD",
        departureTime = "08:45",
        destination = "SFO",
        arrivalTime = "11:30",
        totalTime = "3h 45m",
        price = 250.00,
    ),
    FlightDetail(
        companyName = "American Airlines",
        flightType = "First Class",
        source = "DFW",
        departureTime = "10:15",
        destination = "MIA",
        arrivalTime = "14:20",
        totalTime = "4h 5m",
        price = 600.00,
    ),
    FlightDetail(
        companyName = "JetBlue Airways",
        flightType = "Premium",
        source = "BOS",
        departureTime = "16:30",
        destination = "SEA",
        arrivalTime = "21:10",
        totalTime = "4h 40m",
        price = 450.00,
    ),
    FlightDetail(
        companyName = "Alaska Airlines",
        flightType = "Standard",
        source = "ANC",
        departureTime = "09:00",
        destination = "MCO",
        arrivalTime = "16:20",
        totalTime = "7h 20m",
        price = 300.00,
    )
)