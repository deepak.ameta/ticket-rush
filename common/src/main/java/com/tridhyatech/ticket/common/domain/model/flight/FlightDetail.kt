package com.tridhyatech.ticket.common.domain.model.flight

data class FlightDetail(
    val companyName: String = "",
    val flightType: String = "",
    val source: String = "",
    val departureTime: String = "",
    val destination: String = "",
    val arrivalTime: String = "",
    val totalTime: String = "",
    val price: Double = 0.00,
)