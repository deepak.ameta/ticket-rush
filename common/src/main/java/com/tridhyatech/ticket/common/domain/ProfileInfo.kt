package com.tridhyatech.ticket.common.domain

data class ProfileInfo(
    val name: String = "",
    val gender: String = "",
    val email: String = "",
    val phoneNo: String = "",
    val id: String = "",
)
