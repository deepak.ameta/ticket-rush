package com.tridhyatech.ticket.common.domain.model

import com.tridhyatech.ticket.common.utils.PaymentStatus
import com.tridhyatech.ticket.common.utils.TicketType

data class PaymentInfo(
    val routeName: String,
    val orderId: String,
    val ticketType: TicketType,
    val status: PaymentStatus,
    val date: String,
    val price: Int,
)