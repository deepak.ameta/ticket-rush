package com.tridhyatech.ticket.common.domain.model.hotel

data class HotelDetail(
    val name: String,
    val location: String,
    val price: Int,
    val discountedPrice: Int,
    val rating: Double,
    val imageUrl: String,
)
