package com.tridhyatech.ticket.common.domain.model.flight

data class BookingDetail(
    val price: Double = 0.0,
    val luggageWeight: Int = 0,
    val luggagePrice: Double = 0.0,
    val insuranceAdded: Boolean = false,
    val insurancePrice: Double = 0.0,
    val finalPrice: Double = 0.0,
)