package com.tridhyatech.ticket.common.domain.model.flight

data class TransactionDetail(
    val status: String = "",
    val invoice: String = "",
    val transactionDate: String = "",
    val paymentMethod: String = "",
    val amountPaid: Double = 0.0,
)
