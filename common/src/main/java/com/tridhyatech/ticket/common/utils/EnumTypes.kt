package com.tridhyatech.ticket.common.utils

enum class PaymentStatus {
    COMPLETED, PENDING
}

enum class TicketType {
    BUS, FLIGHT, HOTEL, TRAIN
}