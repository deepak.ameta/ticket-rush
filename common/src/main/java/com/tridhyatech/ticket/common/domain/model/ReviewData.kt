package com.tridhyatech.ticket.common.domain.model

data class ReviewData(
    val rating: Double,
    val date: String,
    val usefulCount: Int,
    val quote: String,
)