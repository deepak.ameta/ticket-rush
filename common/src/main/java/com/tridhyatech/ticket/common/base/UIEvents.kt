package com.tridhyatech.ticket.common.base

sealed interface UiEvents {
    object OnNext : UiEvents
    object OnBack : UiEvents
    data class OnError(val message: String) : UiEvents
}